<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Boina de l&atilde; dupla face agressiva | Acess&oacute;rios de Pintura</title>
	<meta name="Description" content="Boina de l&atilde; dupla face agressiva: S�o boinas produzidas a partir de fibras naturais e sint�ticas para polimento de pinturas automotivas." />
	<meta name="Keywords" content="boina l� 3m casa pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Boina de L� Dupla Face Agressiva" alt="Boina de L� Dupla Face Agressiva" src="../slices/pinceis-e-rolos/img-boina-la-duplaface.jpg" />
						</div>
						<h2>Boina de L� Dupla Face Agressiva</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>S�o boinas produzidas a partir de fibras naturais e sint�ticas para polimento de pinturas automotivas.</p>
							<p>Proporcionando uma alta produtividade com excelente acabamento.</p>
							<p>S�o especialmente indicadas para serem utilizadas com o Sistema Perfect-it de polimento.</p>
							<p>N�o necessitamde suporte. S�o conectadas diretamente na politriz atrav�s de um adaptador.</p>
							<p>Possuem furo central que evita o ac�mulo de material.</p>
							<p>S�o altamente flex�veis, permitindo polimento em �reas curvas.</p>
							<p>CA 4115</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>