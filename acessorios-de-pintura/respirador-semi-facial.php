<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>PROTETOR AURICULAR POMP PLUS 3M</title>
    <meta name="Description" content="RESPIRADOR SEMI FACIAL 6200: Respirador com manuten��o, tamanho m�dio, composto por elast�meros sint�ticos." />
    <meta name="Keywords" content="acess�rios de pintura tintas casa pintura" />
    <meta name="Author" content="Wender S. Souza" />
    <meta name="Robots" content="index, follow" />
    <meta name="revisit-after" content="1 day" />
    <? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
    <div id="Pagina">
        <div id="Linha1">
            <div id="ConteudoLinha1">
                <? include "../componentes/topo.php"; ?>
            </div>
        </div>
        <div id="Linha2">
            <div id="ConteudoLinha2">
                <div id="ConteudoProdutos">
                    <div id="Informacoes">
                        <a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
                        <div id="ImagemProduto">
                            <img title="Respirador Semi Facial 6200" alt="Respirador Semi Facial 6200" src="../slices/pinceis-e-rolos/img-respirador-semi-facil.jpg" />
                        </div>
                        <h2>Respirador Semi Facial 6200</h2>
                        <div id="InformacoesProduto">
                            <span class="Titulo">Descri��o do produto</span>
                            <p>Respirador com manuten��o, tamanho m�dio, composto por  elast�meros sint�ticos. </p>
                            <p>Possui pe�as de reposi��o. Combina filtros e  cartuchos para poeiras, fumos, n�voas, gases e vapores.</p>
                            <p>CA 4115</p>
                        </div>
                        <div id="InformacoesAdicionais"></div>
                    </div>
                    <? include "../componentes/solicitar-orcamento.php"; ?>
                    <? include "../componentes/outros-produtos.php"; ?>
                </div>
            </div>
        </div>
        <div id="Linha3">
            <? include "../componentes/rodape-tintas.php"; ?>
        </div>
    </div>
    <div id="mask"></div>
</body>
</html>