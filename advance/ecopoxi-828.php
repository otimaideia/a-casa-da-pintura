<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Ecopoxi 828 | Advance Tintas</title>
	<meta name="Description" content="Ecopoxi 828 Advance Tintas: Tinta de acabamento de excelente aspecto decorativo e protetivo." />
	<meta name="Keywords" content="Tintas advance tintas ecopoxi A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Tinta Advance Ecomar" alt="Tinta Advance Ecomar" src="../slices/tintas-advance/img-ecopoxi.jpg" />
						</div>
						<h2>Tinta Advance Ecopoxi 828</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Tinta de acabamento de excelente aspecto decorativo e protetivo.</p>
							<p><b>Locais para Aplica��o:</b> Tanques de Concreto.</p>
							<p><a target="_blank" title="Dados t�cnicos Ecopoxi 828" href="/2014/pdf/ecopoxi-828.pdf">Dados T�cnicos (PDF)</a></p>
							<span class="Titulo">Mais informa��es sobre a tinta</span>
							<p>� uma tinta ep�xi modificada, hidrossol�vel, de dois ../componentes. Tinta de acabamento de excelente aspecto decorativo e protetivo, de f�cil aplica��o a rolo, trincha ou por pulveriza��o. Quando aplicado em espessuras de 60 a 90 micrometros resiste as repetidas opera��es de limpeza e desinfec��o com produtos qu�micos de uso dom�stico, industrial e hospitalar. Por ser uma tinta hidrossol�vel, pode ser aplicada em locais fechados sem necessidade de interdi��o. Atende as Normas Hospitalares: ASTM 3273, ABNT 15301, NBR 15079 Premium, JIS 2801:2006. Recomendado na pintura de superf�cies de alvenaria , blocos , concreto ou cimento amianto. Na pintura de paredes, em f�bricas de produtos aliment�cios, laborat�rios farmac�uticos, latic�nios, cervejarias, f�bricas de refrigerantes, hospitais (centro cir�rgico) e outras onde revestimentos � base de solventes podem contaminar produtos e processos. Tamb�m recomendado para a prote��o de a�o carbono, quando aplicado sobre primer recomendado.</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>