<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>M�scara 3M 8822 PFF2 com v�lvula | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas</title>
	<meta name="Description" content="M�scara 3M: � um respirador semifacial filtrante que protege contra poeiras t�xicas, odores e vapores org�nicos em baixas concentra��es" />
	<meta name="Keywords" content="mascara 3m" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="M�scara 3M 8713B PFF1" alt="M�scara 3M 8713B PFF1" src="../slices/pinceis-e-rolos/img-mascara-3m.jpg" />
						</div>
						<h2>M�scara 3M 8713B PFF1</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>� um respirador semifacial filtrante que protege contra poeiras t�xicas, odores e vapores org�nicos em baixas concentra��es. Extremamente leve, proporcionando conforto ao usu�rio. Ideal para ser usado na repintura automotiva. Possui grampo de ajuste nasal com tira de espuma e duas bandas de espuma e duas bandas de el�stico, amoldando-se perfeitamente � face do usu�rio. Respirador com Certificado de Aprova��o do Minist�rio do Trabalho.</p>
							<p><b>C�digo: CA 448</b></p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<ul>
									<li><b>Refer�ncia:</b> PC</li>
									<li>Marca: A Casa da Pintura</li>
									<li>Unidade: PC</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>