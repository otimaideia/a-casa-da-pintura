<?php
#### START AUTOCODE
/**
 * Classe generada para a tabela "tb_trabalhe_conosco"
 * in 2014-02-03
 * @author Hugo Ferreira da Silva
 * @link http://www.hufersil.com.br/lumine
 * @package entidades
 *
 */

class TbTrabalheConosco extends Lumine_Base {

    
    public $id;
    public $nmCandidato;
    public $dsEmail;
    public $cdTelefone;
    public $nmArquivo;
    
    
    
    /**
     * Inicia os valores da classe
     * @author Hugo Ferreira da Silva
     * @return void
     */
    protected function _initialize()
    {
        $this->metadata()->setTablename('tb_trabalhe_conosco');
        $this->metadata()->setPackage('entidades');
        
        # nome_do_membro, nome_da_coluna, tipo, comprimento, opcoes
        
        $this->metadata()->addField('id', 'id', 'int', 11, array('primary' => true, 'notnull' => true, 'autoincrement' => true));
        $this->metadata()->addField('nmCandidato', 'nm_candidato', 'varchar', 100, array());
        $this->metadata()->addField('dsEmail', 'ds_email', 'varchar', 120, array());
        $this->metadata()->addField('cdTelefone', 'cd_telefone', 'varchar', 45, array());
        $this->metadata()->addField('nmArquivo', 'nm_arquivo', 'varchar', 255, array());

        
    }

    #### END AUTOCODE


}
