<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Rolo de L&atilde; 23cm Plus | Acess&oacute;rios de Pintura A Casa da Pintura</title>
	<meta name="Description" content="Acess�rios de Pintura, saiba como comprar o Rolo de L� 23cm Extra Plus e outros produtos para pintura na A Casa da Pintura" />
	<meta name="Keywords" content="rolo l� extra plus" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Rolo de l� 23cm Extra Plus" alt="Rolo de l� 23cm Extra Plus" src="../slices/pinceis-e-rolos/img-rolo-dela-extra-plus.jpg" />
						</div>
						<h2>Rolo de l� 23cm Extra Plus</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>19 MM Ideal para latex, PVA e acrilica a base de agua,oleos e esmalte</p>
							<p>Superficies rugosas, semi-rugosas e texturas suaves. Boa absorcao e retencao da tinta.</p>
							<p>Mobilidade e praticidade na aplicacao.</p>
							<p>Cobertura e acabamento uniformes.</p>
							<p>Alta absorcao e retencao de tinta.</p>
							<p>Super resistente e duravel.</p>
							<p><b>C�digo: 53</b></p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<ul>
									<li><b>Refer�ncia:</b> PC</li>
									<li>Marca: A Casa da Pintura</li>
									<li>Unidade: PC</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>