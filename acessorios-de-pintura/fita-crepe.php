<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Fita Crepe | A Casa da Pintura</title>
	<meta name="Description" content="Fita Crepe: Especialmente indicado para empaleamento de pintura automotiva." />
	<meta name="Keywords" content="Fita crepe tintas" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Fita Crepe" alt="Fita Crepe" src="../slices/pinceis-e-rolos/img-fita-crepe.jpg" />
						</div>
						<h2>Fita Crepe</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Especialmente indicado para empaleamento de pintura automotiva.</p>
							<p>Papel crepado tratado e adesivo controlado.</p>
							<p>Resiste a temperaturas de at� 93 C.</p>
							<p>Medidas de 16, 19 e 25 mm x 50m.</p>
							<p>N�o deixa res�duo quando removida, evitando-se retrabalhos.</p>
							<p>Alta flexibilidade, �tima para contornos</p>
							<p>Maior resist�ncia.</p>
							<p>Maior conformabilidade.</p>	
							<p>Tamanhos: 19X50 e 50X50</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>