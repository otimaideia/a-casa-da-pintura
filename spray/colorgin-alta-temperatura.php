<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Colorgin Alta Temperatura| Spray A Casa da Pintura</title>
	<meta name="Description" content="Colorgin Alta Temperatura � uma tinta em spray especialmente desenvolvida para pintura de partes externas de objetos" />
	<meta name="Keywords" content="tintas spray colorgin" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Colorgin Alta Temperatura" alt="Colorgin Alta Temperatura" src="../slices/spray-colorgin/img-colorgin-alta-temperatura.jpg" />
						</div>
						<h2>Colorgin Alta Temperatura</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p><b>Colorgin Alta Temperatura </b>� uma  tinta em spray especialmente desenvolvida para pintura de partes externas de  objetos ou superf�cies met�licas, expostas a altas temperaturas, apresentando  resist�ncia de at� 600�C.</p>	
							<p><b>Indicada para:</b> escapamentos, chamin�s, lareiras,  caldeiras, tubula��es e parte externa de churrasqueiras e fog�es. </p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<ul>
									<li><b>Importante:</b></li>
									<li>Este  produto n�o deve ser aplicado em locais que ter�o contato com alimentos, como a  parte interna de fog�es e microondas;</li>
									<li>Durante  o aquecimento da pe�a, a pel�cula aplicada poder� apresentar um leve  amolecimento, por�m, com o resfriamento, a tinta volta a suas caracter�sticas  normais;</li>
									<li>N�o  aplicar sobre isopor, superf�cies de pl�stico ou acr�lico;</li>
									<li>O  produto na cor alum�nio poder� liberar part�culas de pigmento no manuseio da  pe�a aplicada.<strong></strong></li>
								</ul>
								<p><b>Acabamento:</b> fosco e met�lico</p>
								<p><b>Rendimento  Aproximado:</b> de 1,0m� a 1,2 m� por embalagem, por  dem�o.</p>
								<p><b>Preparo da Superf�cie/  Aplica��o:</b></p>
								<ul>
									<li>Lixe  a superf�cie e remova toda a tinta j� existente na superf�cie que ser� pintada. </li>
									<li>Elimine  a poeira, gordura ou qualquer contaminante.</li>
									<li>Aguarde  a secagem e aplique de 2 a  3 dem�os bem finas do produto, em movimentos constantes e uniformes, no sentido  horizontal e vertical.</li>
									<li>Aplique  a uma dist�ncia de 30 cm  da superf�cie.</li>
									<li>Ao  final do uso, limpe a v�lvula virando a lata para baixo e pressione at� que  saia apenas g�s.</li>
								</ul>
								<p><b>Secagem:</b> </p>
								<ul>
									<li> Entre dem�os: 5   a 10 minutos;</li>
									<li> Ao toque: 30 minutos;</li>
									<li> Manuseio: 3 horas;</li>
									<li>Total: 24 horas;</li>
								</ul>
								<p> Para testes mec�nicos recomenda-se aguardar 72 horas</p>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>