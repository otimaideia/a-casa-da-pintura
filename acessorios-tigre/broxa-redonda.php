<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Broxa Redonda Tigre | Acess&oacute;rios de Pintura A Casa da Pintura</title>
    <meta name="Description" content="Broxa Redonda Tigre, confira mais detalhes deste acess�rio de pintura" />
    <meta name="Author" content="Wender S. Souza" />
    <meta name="Robots" content="index, follow" />
    <meta name="revisit-after" content="1 day" />
    <? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
    <div id="Pagina">
        <div id="Linha1">
            <div id="ConteudoLinha1">
                <? include "../componentes/topo.php"; ?>
            </div>
        </div>
        <div id="Linha2">
            <div id="ConteudoLinha2">
                <div id="ConteudoProdutos">
                    <div id="Informacoes">
                        <a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
                        <div id="ImagemProduto">
                            <img title="Broxa Redonda" alt="Broxa Redonda" src="../slices/acessorios-tigre/img-brocha-redonda.jpg" />
                        </div>
                        <h2>Broxa Redonda</h2>
                        <div id="InformacoesProduto">
                            <span class="Titulo">Descri��o do produto</span>
                            <p>Cabo: <b>longo -  cor natural</b></p>
                            <p>Composi��o: <b>sint�tica  - cor gris</b></p>
                            <p>Formato: <b>redonda</b></p>
                            <p>Ideal para: <b>caia��o</b></p>
                            <p>Indica��o: <b>chapisco</b></p>
                            <p>T�cnica: <b>base cal</b></p>
                            <p>Virola: <b>pl�stico</b></p>
                            <p><b>Refer�ncia: 1275 </b></p>
                        </div>
                        <div id="InformacoesAdicionais">
                            <div id="Detalhes">
                                <span id="Detalhe">Detalhes:</span>
                                <p>N�o existem maiores detalhes para este produto</p>
                            </div>
                        </div>
                    </div>
                    <? include "../componentes/solicitar-orcamento.php"; ?>
                    <? include "../componentes/outros-produtos.php"; ?>
                </div>
            </div>
        </div>
        <div id="Linha3">
            <? include "../componentes/rodape-tintas.php"; ?>
        </div>
    </div>
    <div id="mask"></div>
</body>
</html>