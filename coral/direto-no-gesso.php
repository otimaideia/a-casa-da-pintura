<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Coral Direto no Gesso | A Casa da Pintura</title>
	<meta name="Description" content="Coral Direto no Gesso: Direto no Gesso � a tinta especialmente desenvolvida para aplica��o diretamente sobre o gesso sem a necessidade de uso de fundo" />
	<meta name="Keywords" content="Tintas Coral Direto Gesso A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="http://www.belatintas.com.br/tinta-p-piso-preto-coralpiso-18lt.html" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Tinta Coral Direto no Gesso" alt="Tinta Coral Direto no Gesso" src="../slices/tintas-coral/img-direto-gesso.jpg" />
						</div>
						<h2>Tinta Coral Direto no Gesso</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Direto no Gesso � a tinta especialmente desenvolvida para aplica��o diretamente sobre o gesso sem a necessidade de uso de fundo, pois a tinta n�o amarela com o tempo e fixa o p� solto espec�fico do gesso, de forma que a superf�cie n�o sofra descascamento. Como se n�o bastasse, Direto no Gesso tem boa cobertura, rendimento e f�cil retoque. Possui acabamento fosco e est� dispon�vel na cor branca, mas voc� pode obter outras cores fazendo misturas com o Corante Base �gua Coral.</p>
						</div>
						<div id="InformacoesAdicionais">
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<ul>
									<li><b>Embalagem/Rendimento</b></li>
									<li>Lata <b>18 L</b>: 150 a 200 m� por dem�o</li>
									<li>Gal�o <b>3,6 L</b>: 30 a 40 m� por dem�o</li>
								</ul>
								<ul>
									<li><b>Aplica��o</b></li>
									<li>Rolo de l� de p�lo baixo ou pincel de cerdas macias. Limpe as ferramentas com �gua e sab�o.</li>
									<li><b>Dilui��o</b></li>
									<li>Diluir com �gua pot�vel. Primeira dem�o para selar: de 20 a 50%. Demais dem�os: de 10 a 20%</li>
									<li><b>Acabamento:</b> Fosco</li>
									<li><b>Secagem: </b>Ao toque: 30 min, entre dem�os: 4 horas, final: 4 horas</li>
								</ul>
							</div> 
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>