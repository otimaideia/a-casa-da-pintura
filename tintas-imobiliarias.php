<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Imobili&aacute;rias, tintas industriais e tintas automotivas | Casa da Pintura</title>
	<title>Tintas Imobiliárias | Coral, Suvinil, Esmalte, Massa Corrida </title>
	<meta name="description" content="Tudo que você precisa para fazer aquela reforma na sua casa! Tintas Imobiliárias para sua casa, apartamento, tudo na Casa da Pintura!"/>
	<meta name="keywords" content="pinta piso, chega de mofo, tintas imobiliarias, tinta para casa, tinta para parede, esmalte, chega de mofo, direto no gesso, decora, selador, sol e chuva" />
	<? include "componentes/includes.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<h2>Tintas Imobili&aacute;rias - <a href="/contato.php" title="Fa&ccedil;a seu or&ccedil;amento">Fa&ccedil;a seu or&ccedil;amento</a></h2>					

					<ul>
						<li>
							<a title="Tintas Coral" href="tintas-coral.php"><img src="slices/img-logotipo-coral.gif" title="Tintas Coral" alt="Tintas Coral" /></a>
							<a title="Tintas Coral" href="tintas-coral.php">Tintas Coral</a>
						</li>
						<li>
							<a title="Tintas Suvinil" href="tintas-suvinil.php"><img src="slices/img-logotipo-suvinil.jpg" title="Tintas Suvinil" alt="Tintas Suvinil" /></a>
							<a title="Tintas Suvinil" href="tintas-suvinil.php">Tintas Suvinil</a>
						</li>
					</ul>
					<h2><a href="/tintas-coral.php" title="Tintas Coral">Tintas Coral</a></h2>
					<ul>
						<li>
							<a href="coral/coralit-ac-bco-gelo.php" title="Tinta Coralite Acetinado Branco Gelo"><img src="slices/tintas-coral/img-coralit-ac-bco-gelo.jpg" alt="Tinta Coralite Acetinado Branco Gelo" title="Tinta Coralite Acetinado Branco Gelo"></a>
							<h3><a href="coral/coralit-ac-bco-gelo.php" title="Tinta Coralite Acetinado Branco Gelo">Coralit AC Bco Gelo</a></h3>
						</li>
						<li>
							<a href="coral/esmalte-fosco-coralit.php" title="Tinta Esmalte Fosco Coralit"><img src="slices/tintas-coral/img-esmalte-fosco-coralit.jpg" alt="Tinta Esmalte Fosco Coralit" title="Tinta Esmalte Fosco Coralit"></a>
							<h3><a href="coral/esmalte-fosco-coralit.php" title="Tinta Esmalte Fosco Coralit">Esmalte Fosco Coralit</a></h3>
						</li>
						<li>
							<a href="coral/rende-muito-coralatex-fosco.php"><img title="Tinta Rende Muito - Coralatex Fosco" alt="Tinta Rende Muito - Coralatex Fosco" src="slices/tintas-coral/img-coral-rende-muito.jpg" /></a>
							<a href="coral/rende-muito-coralatex-fosco.php" title="Tinta Rende Muito - Coralatex Fosco"><h3>Rende Muito</h3></a>							
						</li>
						<li>
							<a href="coral/coralmur-zero-odor.php"><img title="Tinta Coralmur Zero Odor" alt="Tinta Coralmur Zero Odor" src="slices/tintas-coral/img_coral_3_em_1.jpg" /></a>
							<a href="coral/coralmur-zero-odor.php" title="Tinta Coralmur Zero Odor"><h3>Coral 3 em 1</h3></a>							
						</li>
						<li class="NoMargin">
							<a href="coral/coralar-esm-acetinado.php"><img title="Tinta Decora Acetinado" alt="Tinta Decora Acetinado" src="slices/tintas-coral/img-decora-acetinado.jpg" /></a>
							<a href="coral/coralar-esm-acetinado.php" title="Tinta Decora Acetinado"><h3>Decora Acetinado</h3></a>							
						</li>
						<li>
							<a href="coral/decora-brancos.php"><img title="Tinta Decora Brancos" alt="Tinta Decora Brancos" src="slices/tintas-coral/img-decora-brancos.jpg" /></a>
							<a href="coral/decora-brancos.php" title="Tinta Decora Brancos"><h3>Decora Brancos</h3></a>							
						</li>
						<li>
							<a href="coral/decora-cores.php"><img title="Tinta Decora Cores" alt="Tinta Decora Cores" src="slices/tintas-coral/img-decora-cores.jpg" /></a>
							<a href="coral/decora-cores.php" title="Tinta Decora Cores"><h3>Decora Cores</h3></a>							
						</li>		
						<li>
							<a href="coral/decora-luz-espaco.php"><img title="Tinta Decora Luz e Espa&ccedil;o" alt="Tinta Decora Luz e Espa&ccedil;o " src="slices/tintas-coral/img-decora-luz-espaco.jpg" /></a>
							<a href="coral/decora-luz-espaco.php" title="Tinta Decora Luz e Espa&ccedil;o"><h3>Decora Luz e Espa&ccedil;o</h3></a>							
						</li>
						<li>
							<a href="coral/super-lavavel.php"><img title="Tinta Super Lav&aacute;vel" alt="Tinta Super Lav&aacute;vel" src="slices/tintas-coral/img-super-lavavel.jpg" /></a>
							<a href="coral/super-lavavel.php" title="Tinta Super Lav&aacute;vel"><h3>Super Lav&aacute;vel</h3></a>							
						</li>
						<li class="NoMargin">
							<a href="coral/pinta-piso.php"><img title="Tinta Pinta Piso" alt="Tinta Pinta Piso" src="slices/tintas-coral/img-pinta-piso.jpg" /></a>
							<a href="coral/pinta-piso.php" title="Tinta Pinta Piso"><h3>Pinta Piso</h3></a>							
						</li>
						<li>
							<a href="coral/direto-no-gesso.php"><img title="Tinta Direto no Gesso" alt="Tinta Direto no Gesso" src="slices/tintas-coral/img-direto-gesso.jpg" /></a>
							<a href="coral/direto-no-gesso.php" title="Tinta Direto no Gesso"><h3>Direto no Gesso</h3></a>							
						</li>
						<li>
							<a href="coral/massa-acrilica.php"><img title="Massa Acr&iacute;lica Coral" alt="Massa Acr&iacute;lica Coral" src="slices/tintas-coral/img-massa-acrilica-coral.jpg" /></a>
							<a href="coral/massa-acrilica.php" title="Massa Acr&iacute;lica Coral"><h3>Massa Acr&iacute;lica</h3></a>							
						</li>					
						<li>
							<a href="coral/seladora-madeira.php"><img title="Tintas Seladora Madeira" alt="Tintas Seladora Madeira" src="slices/tintas-coral/img-seladora-madeira.jpg" /></a>
							<a href="coral/seladora-madeira.php" title="Tinta Seladora Madeira"><h3>Seladora Madeira</h3></a>							
						</li>
						<li>
							<a href="coral/coralar-esmalte.php"><img title="Tintas Coralar Esmalte" alt="Tintas Coralar Esmalte" src="slices/tintas-coral/img-coralar-esmalte.jpg" /></a>
							<a href="coral/coralar-esmalte.php" title="Tintas Coralar Esmalte"><h3>Coralar Esmalte</h3></a>							
						</li>	
						<li class="NoMargin">
							<a href="coral/solvente-para-hammerite.php"><img title="Solvente para Hammerite" alt="Solvente para Hammerite" src="slices/tintas-coral/img-solvente-hammerite.jpg" /></a>
							<a href="coral/solvente-para-hammerite.php" title="Solvente para Hammerite"><h3>Solvente Hammerite</h3></a>							
						</li>
						<li>
							<a href="coral/chega-de-mofo.php"><img title="Coral Chega de Mofo" alt="Coral Chega de Mofo" src="slices/tintas-coral/img-chega-de-mofo.jpg" /></a>
							<a href="coral/chega-de-mofo.php" title="Coral Chega de Mofo"><h3>Coral Chega de Mofo</h3></a>							
						</li>
						<li>
							<a href="coral/ceramica.php"><img title="Cer&acirc;mica Coral" alt="Cer&acirc;mica Coral" src="slices/tintas-coral/img-ceramica-coral.jpg" /></a>
							<a href="coral/ceramica.php" title="Cer&acirc;mica Coral"><h3>Cer&acirc;mica</h3></a>							
						</li>
						<li>
							<a href="coral/grafite.php"><img title="Grafite Coral" alt="Grafite Coral" src="slices/tintas-coral/img-grafite-coral.jpg" /></a>
							<a href="coral/grafite.php" title="Grafite Coral"><h3>Grafite</h3></a>							
						</li>
						<li>
							<a href="coral/massa-para-madeira.php"><img title="Massa para Madeira" alt="Massa para Madeira" src="slices/tintas-coral/img-massa-para-madeira.jpg" /></a>
							<a href="coral/massa-para-madeira.php" title="Massa para Madeira"><h3>Massa para Madeira</h3></a>							
						</li>
						<li class="NoMargin">
							<a href="coral/sol-e-chuva.php"><img title="Tintas Sol e Chuva" alt="Tintas Sol e Chuva" src="slices/tintas-coral/img-sol-e-chuva.jpg" /></a>
							<a href="coral/sol-e-chuva.php" title="Tintas Sol e Chuva"><h3>Sol e Chuva</h3></a>							
						</li>
						<li>
							<a href="coral/selador-acrilico.php"><img title="Tintas Selador Acr&iacute;lico" alt="Tintas Selador Acr&iacute;lico" src="slices/tintas-coral/img-selador-acrilico.jpg" /></a>
							<a href="coral/selador-acrilico.php" title="Tintas Selador Acr&iacute;lico"><h3>Selador Acr&iacute;lico</h3></a>							
						</li>
						<li>
							<a href="coral/hammerite-esmalte-sintetico.php"><img title="Hammerite Esmalte Sint&eacute;tico Anti-Ferrugem" alt="Hammerite Esmalte Sint&eacute;tico Anti-Ferrugem" src="slices/tintas-coral/img-esmalte-sintetico.jpg" /></a>
							<a href="coral/hammerite-esmalte-sintetico.php" title="Hammerite Esmalte Sint&eacute;tico Anti-Ferrugem"><h3>Esmalte Sint&eacute;tico</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-cetol.php"><img title="Verniz Sparlack Cetol" alt="Verniz Sparlack Cetol" src="slices/tintas-coral/img-cetol.jpg" /></a>
							<a href="coral/sparlack-cetol.php" title="Verniz Sparlack Cetol"><h3>Cetol</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-cetol-antiderrapante.php"><img title="Verniz Cetol Anti Derrapante" alt="Verniz Cetol Antiderrapante" src="slices/tintas-coral/img-cetol.jpg" /></a>
							<a href="coral/sparlack-cetol-antiderrapante.php" title="Verniz Cetol Anti Derrapante"><h3>Cetol Anti Derrap GL</h3></a>							
						</li>
						<li class="NoMargin">
							<a href="coral/sparlack-neutrex.php"><img title="Verniz Sparlack Neutrex" alt="Verniz Sparlack Neutrex" src="slices/tintas-coral/img-sparlack-neutrex.jpg" /></a>
							<a href="coral/sparlack-neutrex.php" title="Verniz Sparlack Neutrex"><h3>Neutrex</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-extra-maritimo.php"><img title="Verniz Sparlack Extra Mar&iacute;timo" alt="Verniz Sparlack Extra Mar&iacute;timo" src="slices/tintas-coral/img-extra-maritimo.jpg" /></a>
							<a href="coral/sparlack-extra-maritimo.php" title="Verniz Sparlack Extra Mar&iacute;timo"><h3>Extra Mar&iacute;timo</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-maritimo-fosco.php"><img title="Verniz Sparlack Mar&iacute;timo Fosco" alt="Verniz Sparlack Mar&iacute;timo Fosco" src="slices/tintas-coral/img-maritimo-fosco.jpg" /></a>
							<a href="coral/sparlack-maritimo-fosco.php" title="Verniz Sparlack Mar&iacute;timo Fosco"><h3>Mar&iacute;timo Fosco</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-seladora.php"><img title="Verniz Sparlack Seladora" alt="Verniz Sparlack Seladora" src="slices/tintas-coral/img-seladora.jpg" /></a>
							<a href="coral/sparlack-seladora.php" title="Verniz Sparlack Seladora"><h3>Seladora</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-tingidor.php"><img title="Sparlack Tingidor" alt="Sparlack Tingidor" src="slices/tintas-coral/img-tingidor.jpg" /></a>
							<a href="coral/sparlack-tingidor.php" title="Sparlack Tingidor"><h3>Tingidor</h3></a>							
						</li>
						<li class="NoMargin">
							<a href="coral/fundo-preparador-paredes-base-agua.php"><img title="Fundo Preparador de Paredes Base &aacute;gua" alt="Fundo Preparador de Paredes Base &aacute;gua" src="slices/tintas-coral/img-preparador.jpg" /></a>
							<a href="coral/fundo-preparador-paredes-base-agua.php" title="Fundo Preparador de Paredes Base &aacute;gua"><h3>Fundo Preparador</h3></a>							
						</li>
						<li>
							<a href="coral/corante-base-dagua.php"><img title="Corante Base &aacute;gua Coral" alt="Corante Base &aacute;gua Coral" src="slices/tintas-coral/img-corante-base-agua.jpg" /></a>
							<a href="coral/corante-base-dagua.php" title="Corante Base &aacute;gua Coral"><h3>Corante Base &aacute;gua Coral</h3></a>							
						</li>
						<li>
							<a href="coral/coralar-acrilico.php"><img title="Coralar Acr&iacute;lico" alt="Coralar Acr&iacute;lico" src="slices/tintas-coral/img-coralar-acrilico.jpg" /></a>
							<a href="coral/coralar-acrilico.php" title="Coralar Acr&iacute;lico"><h3>Coralar Acr&iacute;lico</h3></a>							
						</li>
						<li>
							<a href="coral/verniz-acrilico.php"><img title="Verniz Acr&iacute;lico" alt="Verniz Acr&iacute;lico" src="slices/tintas-coral/img-verniz-acrilico.jpg" /></a>
							<a href="coral/verniz-acrilico.php" title="Verniz Acr&iacute;lico"><h3>Verniz Acr&iacute;lico</h3></a>							
						</li>
						<li>
							<a href="coral/zarcoral.php"><img title="Zarcoral " alt="Zarcoral" src="slices/tintas-coral/img-zarcoral.jpg" /></a>
							<a href="coral/zarcoral.php" title="Zarcoral"><h3>Zarcoral</h3></a>							
						</li>
					</ul>
					<h2><a href="/tintas-suvinil.php" title="Tintas Suvinil">Tintas Suvinil</a></h2>
					<h2>ACR&iacute;LICO ACETINADO - SUVINIL</h2>
					<ul>
						<li>
							<a title="Premium Limpa Facil" href="suvinil/acrilico-premium-limpa-facil.php"><img src="slices/tintas-suvinil/img-suvinil-acrilico-fosco.jpg" title="Tintas Coral" alt="Tintas Coral" /></a>
							<h3><a title="Premium Limpa Facil" href="suvinil/acrilico-premium-limpa-facil.php">Premium Limpa Facil</a></h3>
						</li>
						<li>
							<a title="Premium Toque de Seda" href="suvinil/acrilico-premium-toque-de-seda.php"><img src="slices/tintas-suvinil/img-acrilico-premium-toque-de-seda.JPG" title="Premium Toque de Seda" alt="Premium Toque de Seda" /></a>
							<h3><a title="Premium Toque de Seda" href="suvinil/acrilico-premium-toque-de-seda.php">Premium Toque de Seda</a></h3>
						</li>
					</ul>
					<h2>ACR&iacute;LICO SEMI-BRILHO - SUVINIL</h2>
					<ul>
						<li>
							<a title="SUVINIL ACR&iacute;LICO PREMIUM SEMIBRILHO" href="suvinil/acrilico-premium-semibrilho.php"><img src="slices/tintas-suvinil/img-acrilico-premium-semibrilho.jpg" title="SUVINIL ACR&iacute;LICO PREMIUM SEMIBRILHO" alt="SUVINIL ACR&iacute;LICO PREMIUM SEMIBRILHO" /></a>
							<h3><a title="SUVINIL ACR&iacute;LICO PREMIUM SEMIBRILHO" href="suvinil/acrilico-premium-semibrilho.php">Suvinil Acr&iacute;lico Premium Semibrilho</a></h3>
						</li>
					</ul>
					<h2>LATEX PVA - SUVINIL</h2>
					<ul>
						<li>
							<a title="Latex PVA Maxx " href="suvinil/latex-maxx.php"><img src="slices/tintas-suvinil/img-latex-maxx.jpg" title="Latex PVA Maxx " alt="Latex PVA Maxx " /></a>
							<h3><a title="Latex PVA Maxx " href="suvinil/latex-maxx.php">Latex PVA Maxx</a></h3>
						</li>
					</ul><h2>MASSA ACR&iacute;LICA - SUVINIL</h2>
					<ul>
						<li>
							<a title="SUVINIL MASSA ACRILICA" href="suvinil/massa-acrilica.php"><img src="slices/tintas-suvinil/img-massa-acrilica.jpg" title="SUVINIL MASSA ACRILICA" alt="SUVINIL MASSA ACRILICA" /></a>
							<h3><a title="SUVINIL MASSA ACRILICA" href="suvinil/massa-acrilica.php">Massa Acrilica </a></h3>
						</li>
					</ul>
					<h2>MASSA CORRIDA - PVA</h2>
					<ul>
						<li>
							<a title="Massa Corrida" href="suvinil/massa-corrida.php"><img src="slices/tintas-suvinil/img-massa-acrilica.jpg" title="Massa Corrida" alt="Massa Corrida" /></a>
							<h3><a title="Massa Corrida" href="suvinil/massa-corrida.php">Massa Corrida </a></h3>
						</li>
					</ul>
					<h2>SINTETICO ACETINADO - SUVINIL</h2>
					<ul>
						<li>
							<a title="Esmalte Acetinado " href="suvinil/esmalte-acetinado.php"><img src="slices/tintas-suvinil/img-esmalte_sintetico.jpg" title="Esmalte Acetinado " alt="Esmalte Acetinado " /></a>
							<h3><a title="Esmalte Acetinado " href="suvinil/esmalte-acetinado.php">Esmalte Acetinado  </a></h3>
						</li>
					</ul>
					<h2>SINTETICO FOSCO - SUVINIL</h2>
					<ul>
						<li>
							<a title="Esmalte Fosco " href="suvinil/esmalte-fosco.php"><img src="slices/tintas-suvinil/img-esmalte_sintetico.jpg" title="Esmalte Fosco " alt="Esmalte Fosco " /></a>
							<h3><a title="Esmalte Fosco " href="suvinil/esmalte-fosco.php">Esmalte Fosco</a></h3>
						</li>
					</ul>
					<h2>SINTETICO AUTO BRILHO</h2>
					<ul>
						<li>
							<a title="Esmalte Auto Brilho" href="suvinil/esmalte-auto-brilho.php"><img src="slices/tintas-suvinil/img-suvinil-acrilico-fosco.jpg" title="Esmalte Auto Brilho" alt="Esmalte Auto Brilho" /></a>
							<h3><a title="Esmalte Auto Brilho" href="suvinil/esmalte-auto-brilho.php">Esmalte Auto Brilho</a></h3>
						</li>
					</ul>
					<h2>VERNIZ MARITIMO - SUVINIL</h2>
					<ul>
						<li>
							<a title="Verniz Maritimo" href="suvinil/verniz-maritimo.php"><img src="slices/tintas-suvinil/img-verniz-maritimo.jpg" title="Verniz Maritimo" alt="Verniz Maritimo" /></a>
							<h3><a title="Verniz Maritimo" href="suvinil/verniz-maritimo.php">Verniz Maritimo</a></h3>
						</li>
					</ul>
					<h2>BASES ACRILICAS - SUVINIL</h2>
					<ul>
						<li>
							<a title="Acrilico Fosco " href="suvinil/acrilico-fosco.php"><img src="slices/tintas-suvinil/img-suvinil-acrilico-fosco.jpg" title="Acrilico Fosco " alt="Acrilico Fosco " /></a>
							<h3><a title="Acrilico Fosco " href="suvinil/acrilico-fosco.php">Acrilico Fosco </a></h3>
						</li>
						<li>
							<a title="Acrilico SemiBrilho" href="suvinil/acrilico-premium-semibrilho.php"><img src="slices/tintas-suvinil/img-acrilico-premium-semibrilho.jpg" title="Acrilico SemiBrilho " alt="Acrilico SemiBrilho " /></a>
							<h3><a title="Acrilico SemiBrilho " href="suvinil/acrilico-premium-semibrilho.php">Acrilico SemiBrilho </a></h3>
						</li>
						<li>
							<a title="Acrilico Limpa F&aacute;CIL " href="suvinil/acrilico-limpa-facil.php"><img src="slices/tintas-suvinil/img-acrilico-toque-de-seda.jpg" title="Acrilico Limpa F&aacute;CIL " alt="Acrilico Limpa F&aacute;CIL " /></a>
							<h3><a title="Acrilico Limpa F&aacute;CIL " href="suvinil/acrilico-limpa-facil.php">Acrilico Limpa F&aacute;CIL</a></h3>
						</li>
						<li>
							<a title="Toque de Seda" href="suvinil/acrilico-toque-de-seda.php"><img src="slices/tintas-suvinil/img-acrilico-toque-de-seda.jpg" title="Toque de Seda" alt="Toque de Seda" /></a>
							<h3><a title="Toque de Seda" href="suvinil/acrilico-toque-de-seda.php">Toque de Seda</a></h3>
						</li>
						<li>
							<a title="Contra Microfissuras " href="suvinil/contra-microfissuras.php"><img src="slices/tintas-suvinil/img-contra-microfissuras.jpg" title="Contra Microfissuras " alt="Contra Microfissuras " /></a>
							<h3><a title="Contra Microfissuras " href="suvinil/contra-microfissuras.php">Contra Microfissuras </a></h3>
						</li>
					</ul>
					<h2>PISOS PREMIUM - SUVINIL</h2>
					<ul>
						<li>
							<a title="Pisos Premium" href="suvinil/pisos-premium.php"><img src="slices/tintas-suvinil/img-pisos-premium.jpg" title="Pisos Premium" alt="Pisos Premium" /></a>
							<h3><a title="Pisos Premium" href="suvinil/pisos-premium.php">Pisos Premium</a></h3>
						</li>
						<li>
							<a title="Fundo Preparador" href="suvinil/fundo-preparador.php"><img src="slices/tintas-suvinil/img-fundo-reparador-suvinil.jpg" title="Fundo Preparador" alt="Fundo Preparador" /></a>
							<h3><a title="Fundo Preparador" href="suvinil/fundo-preparador.php">Fundo Preparador</a></h3>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "componentes/rodape.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>