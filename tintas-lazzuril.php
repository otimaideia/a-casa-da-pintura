<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Lazzuril | Tintas A Casa da Pintura</title>
	<meta name="Description" content="Encontre diversas op��es de tintas da marca Lazzuril no site da Casa da Pintura: Tintas Bicomponente, Verniz, Clearcoat, Primer, Esmalte, Poliester.  " />
	<meta name="Keywords" content="op��es tintas marca Lazzuril site Casa da Pintura Tintas Bicomponente Verniz Clearcoat Primer Esmalte Poliester" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "componentes/includes.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<h2>Tintas Lazzuril - Fa�a seu or�amento</h2>
					<ul>
						<li>
							<a href="lazzuril/verniz-bicomponente-8000.php" title="Verniz Bicomponente 8000" alt="Verniz Bicomponente 8000"><img src="slices/tintas-lazzuril/img-verniz-bicomponente-8000.jpg" alt="Verniz Bicomponente 8000" title="Verniz Bicomponente 8000"></a>
							<h3><a href="lazzuril/verniz-bicomponente-8000.php" title="Verniz Bicomponente 8000">Bicomponente 8000</a></h3>
						</li>		
						<li>
							<a href="lazzuril/verniz-alto-solidos-8937.php" title="Verniz Alto S�lido" alt="Verniz Alto S�lido"><img src="slices/tintas-lazzuril/img-verniz-auto-solidos.jpg" alt="Verniz Alto S�lido" title="Verniz Alto S�lido"></a>
							<h3><a href="lazzuril/verniz-alto-solidos-8937.php" title="Verniz Alto S�lido">Verniz Alto S�lido</a></h3>
						</li>	
						<li>
							<a href="lazzuril/ulltra-performance-clearcoat-CC900.php" title="Ultra Performance Clearcoat CC900" alt="Ultra Performance Clearcoat CC900"><img src="slices/tintas-lazzuril/img-clearcoat-CC900.jpg" alt="Ultra Performance Clearcoat CC900" title="Ultra Performance Clearcoat Cc900"></a>
							<h3><a href="lazzuril/ulltra-performance-clearcoat-CC900.php" title="Ultra Performance Clearcoat CC900">Clearcoat CC900</a></h3>
						</li>	
						<li>
							<a href="lazzuril/ulltra-performance-clearcoat-CC940.php" title="Ultra Performance Clearcoat CC940" alt="Ultra Performance Clearcoat CC940"><img src="slices/tintas-lazzuril/img-clearcoat-CC900.jpg" alt="Ultra Performance Clearcoat CC940" title="Ultra Performance Clearcoat CC940"></a>
							<h3><a href="lazzuril/ulltra-performance-clearcoat-CC940.php" title="Ultra Performance Clearcoat CC940">Clearcoat CC940</a></h3>
						</li>		
						<li class="NoMargin">
							<a href="lazzuril/verniz-bicomponente-8500.php" title="Verniz Bi-Componente 8500" alt="Verniz Bi-Componente 8500"><img src="slices/tintas-lazzuril/img-verniz-bicomponente-8500.jpg" alt="Verniz Bi-Componente 8500" title="Verniz Bi-Componente 8500"></a>
							<h3><a href="lazzuril/verniz-bicomponente-8500.php" title="Verniz Bi-Componente 8500">Bicomponente 8500 </a></h3>
						</li>	
						<li>
							<a href="lazzuril/primer-universal-018.php" title="Primer Universal 018" alt="Primer Universal 018"><img src="slices/tintas-lazzuril/img-primer-poliuretano-8100-820.jpg" alt="Primer Universal 018" title="Primer Universal 018"></a>
							<h3><a href="lazzuril/primer-universal-018.php" title="Primer Universal 018">Primer Universal 018 </a></h3>
						</li>
						<li>
							<a href="lazzuril/primer-ultrafill-020-022.php" title="Primer Ultrafill 020" alt="Primer Ultrafill 020"><img src="slices/tintas-lazzuril/img-primer-poliuretano-8100-820.jpg" alt="Primer Universal 020" title="Primer Universal 020"></a>
							<h3><a href="lazzuril/primer-ultrafill-020-022.php" title="Primer Ultrafill 020">Primer Ultrafill 020</a></h3>
						</li>
						<li>
							<a href="lazzuril/primer-poliuretano-p710.php" title="Primer Poliur. p710" alt="Primer Poliur. p710"><img src="slices/tintas-lazzuril/img-primer-poliur-p710.jpg" alt="Primer Poliur. p710" title="Primer Poliur. p710"></a>
							<h3><a href="lazzuril/primer-poliuretano-p710.php" title="Primer Poliur. p710">Primer Poliur. p710  </a></h3>
						</li>
						<li>
							<a href="lazzuril/primer-poliuretano-8100-8200.php" title="Primer Poliur. 8100" alt="Primer Poliur. 8100"><img src="slices/tintas-lazzuril/img-primer-poliuretano-8100-820.jpg" alt="Primer Poliur. 8100" title="Primer Poliur. 8100"></a>
							<h3><a href="lazzuril/primer-poliuretano-8100-8200.php" title="Primer Poliur. 8100">Primer Poliur. 8100</a></h3>
						</li>
						<li class="NoMargin">
							<a href="lazzuril/ultra-7000-poliester.php" title="Ultra 7000 Poliester " alt="Ultra 7000 Poliester "><img src="slices/tintas-lazzuril/img-clearcoat-CC900.jpg" alt="Ultra 7000 Poliester " title="Ultra 7000 Poliester "></a>
							<h3><a href="lazzuril/ultra-7000-poliester.php" title="Ultra 7000 Poliester ">Ultra 7000 Poliester </a></h3>
						</li>
						<li>
							<a href="lazzuril/endurecedor-lento.php" title="Endurecedor Lento" alt="Endurecedor Lento"><img src="slices/tintas-lazzuril/img-verniz-auto-solidos.jpg" alt="Endurecedor Lento" title="Endurecedor Lento"></a>
							<h3><a href="lazzuril/endurecedor-lento.php" title="Endurecedor Lento">Endurecedor Lento</a></h3>
						</li>
						<li>
							<a href="lazzuril/endurecedor-rapido.php" title="Endurecedor R�pido" alt="Endurecedor R�pido"><img src="slices/tintas-lazzuril/img-verniz-auto-solidos.jpg" alt="Endurecedor R�pido" title="Endurecedor R�pido"></a>
							<h3><a href="lazzuril/endurecedor-rapido.php" title="Endurecedor R�pido">Endurecedor R�pido</a></h3>
						</li>
						<li>
							<a href="lazzuril/aditivo-anticratera.php" title="Aditivo Anticratera" alt="Aditivo Anticratera"><img src="slices/tintas-lazzuril/img-verniz-auto-solidos.jpg" alt="Aditivo Anticratera" title="Aditivo Anticratera"></a>
							<h3><a href="lazzuril/aditivo-anticratera.php" title="Aditivo Anticratera">Aditivo Anticratera</a></h3>
						</li>
						<li>
							<a href="lazzuril/acelerador-de-secagem.php" title="Acelerador Secagem " alt="Acelerador Secagem "><img src="slices/tintas-lazzuril/img-verniz-auto-solidos.jpg" alt="Acelerador Secagem " title="Acelerador Secagem "></a>
							<h3><a href="lazzuril/acelerador-de-secagem.php" title="Acelerador Secagem ">Acelerador Secagem </a></h3>
						</li>
						<li class="NoMargin">
							<a href="lazzuril/seladora-para-plastico.php" title="Seladora para Pl�stico" alt="Seladora para Pl�stico"><img src="slices/tintas-lazzuril/img-primer-poliuretano-8100-820.jpg" alt="Seladora para Pl�stico" title="Seladora para Pl�stico"></a>
							<h3><a href="lazzuril/seladora-para-plastico.php" title="Seladora para Pl�stico">Seladora para Pl�stico</a></h3>
						</li>
						<li>
							<a href="lazzuril/verniz-bicomponente-6100.php" title="Bicomponente 6100 " alt="Bicomponente 6100"><img src="slices/tintas-lazzuril/img-verniz-componente-6100.jpg" alt="Bicomponente 6100 " title="Bicomponente 6100 "></a>
							<h3><a href="lazzuril/verniz-bicomponente-6100.php" title="Bicomponente 6100 ">Bicomponente 6100</a></h3>
						</li>
						<li>
							<a href="lazzuril/verniz-bicomponente-7100.php" title="Bicomponente 7100" alt="Bicomponente 7100"><img src="slices/tintas-lazzuril/img-verniz-componente-6100.jpg" alt="Bicomponente 7100" title="Bicomponente 7100"></a>
							<h3><a href="lazzuril/verniz-bicomponente-7100.php" title="Bicomponente 7100">Bicomponente 7100</a></h3>
						</li>
						<li>
							<a href="lazzuril/verniz-bicomponente-4500.php" title="Bicomponente 4500 " alt="Bicomponente 45004500 "><img src="slices/tintas-lazzuril/img-verniz-componente-4000.jpg" alt="Bicomponente 4500 " title="Bicomponente 4500"></a>
							<h3><a href="lazzuril/verniz-bicomponente-4500.php" title="Bicomponente 4500 ">Bicomponente 4500</a></h3>
						</li>
						<li>
							<a href="lazzuril/verniz-bicomponente-4000.php" title="Bicomponente 4000 " alt="Bicomponente 4000 "><img src="slices/tintas-lazzuril/img-verniz-componente-4000.jpg" alt="Bicomponente 4000 " title="Bicomponente 4000"></a>
							<h3><a href="lazzuril/verniz-bicomponente-4000.php" title="Bicomponente 4000 ">Bicomponente 4000</a></h3>
						</li>
						<li class="NoMargin">
							<a href="lazzuril/primer-poliuretano.php" title="Primer Poiuretano" alt="Primer Poiuretano"><img src="slices/tintas-lazzuril/img-primer-poliur-p710.jpg" alt="Primer Poiuretano" title="Primer Poiuretano"></a>
							<h3><a href="lazzuril/primer-poliuretano.php" title="Primer Poiuretano">Primer Poiuretano</a></h3>
						</li>
						<li>
							<a href="lazzuril/massa-pequenas-correcoes.php" title="Massa Pequenas Corre��es" alt="Massa Pequenas Corre��es"><img src="slices/tintas-lazzuril/img-massa_para_pequenas_correcoes.jpg" alt="Massa Pequenas Corre��es" title="Massa Pequenas Corre��es"></a>
							<h3><a href="lazzuril/massa-pequenas-correcoes.php" title="Massa Pequenas Corre��es">Massa Peq. Corre��es </a></h3>
						</li>
						<li>
							<a href="lazzuril/awx-poliester-base-agua.php" title="Awx � Poliester Base �gua" alt="Awx � Poliester Base �gua"><img src="slices/tintas-lazzuril/img-awx-poliester-base-agua.jpg" alt="Awx � Poliester Base �gua" title="Awx � Poliester Base �gua"></a>
							<h3><a href="lazzuril/awx-poliester-base-agua.php" title="Awx � Poliester Base �gua">AWX</a></h3>
						</li>
						<li>
							<a href="lazzuril/ultra-7000-high-performance-clearcoat-hpc15.php" title="Ultra 7000" alt="Ultra 7000"><img src="slices/tintas-lazzuril/img-clearcoat-CC900.jpg" alt="Ultra 7000" title="Ultra 7000"></a>
							<h3><a href="lazzuril/ultra-7000-high-performance-clearcoat-hpc15.php" title="Ultra 7000">Ultra 7000</a></h3>
						</li>
						<li>
							<a href="lazzuril/acabamento-poliester-ms.php" title="Acabamento Poli�ster MS" alt="Acabamento Poli�ster MS"><img src="slices/tintas-lazzuril/img-verniz-bicomponente-8500.jpg" alt="Acabamento Poli�ster MS" title="Acabamento Poli�ster MS"></a>
							<h3><a href="lazzuril/acabamento-poliester-ms.php" title="Acabamento Poli�ster MS">Poli�ster MS</a></h3>
						</li>
						<li class="NoMargin">
							<a href="lazzuril/primer-2kp-411.php" title="Primer 2K P- 411" alt="Primer 2K P- 411"><img src="slices/tintas-lazzuril/img-primer-poliur-p710.jpg" alt="Primer 2K P- 411" title="Primer 2K P- 411"></a>
							<h3><a href="lazzuril/primer-2kp-411.php" title="Primer 2K P- 411">Primer 2K P- 411</a></h3>
						</li>
						<li>
							<a href="lazzuril/esmalte-sintetico-catalisado.php" title="Esmalte Sint�tico Catalisado" alt="Esmalte Sint�tico Catalisado"><img src="slices/tintas-lazzuril/img-esmalte-sintetico-catalisad.jpg" alt="Esmalte Sint�tico Catalisado" title="Esmalte Sint�tico Catalisado"></a>
							<h3><a href="lazzuril/esmalte-sintetico-catalisado.php" title="Esmalte Sint�tico Catalisado">Esmalte Sint�tico</a></h3>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "componentes/rodape.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>