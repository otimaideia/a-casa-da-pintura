<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Solvente para Hammerite Coral | A Casa da Pintura</title>
	<meta name="Description" content="Coral Solvente para Hammerite: O Solvente Hammerite � indicado para limpar e desengordurar as superf�cies antes de pintar" />
	<meta name="Keywords" content="Tintas Coral solvente hammerite A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Solvente para Hammerite" alt="Solvente para Hammerite" src="../slices/tintas-coral/img-solvente-hammerite.jpg" />
						</div>
						<h2>Solvente para Hammerite</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>O Solvente Hammerite � indicado para: limpar e desengordurar as superf�cies antes de pintar, lavar as ferramentas, diluir os produtos da marca Hammerite quando aplicados com rolo ou rev�lver.</p>
						</div>				
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>	<ul>
								<li><b>Embalagem/Rendimento</b></li>
								<li>Lata 0,5 L: N�o aplic�vel</li>
							</ul>
							<ul>
								<li><b>Aplica��o</b></li>
								<li>Para limpeza e dilui��o</li>
							</ul>
							<ul>
								<li><b>Dilui��o</b></li>
								<li>Pronto para uso</li>
							</ul>
						</div>
						<div id="TintasDisponiveis">
							<span id="TintasDisponiveisTitulo">Tintas Dispon�veis</span>
							<ul>
								<li>500ML</li>
							</ul>
						</div>
					</div>
				</div>
				<? include "../componentes/solicitar-orcamento.php"; ?>
				<? include "../componentes/outros-produtos.php"; ?>
			</div>
		</div>
	</div>
	<div id="Linha3">
		<? include "../componentes/rodape-tintas.php"; ?>
	</div>
</div>
<div id="mask"></div>
</body>
</html>