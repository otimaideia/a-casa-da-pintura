<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas | Tintas Residenciais, Industriais e Automotivas</title>
	<meta name="description" content="As melhores tintas voc� encontra na Casa da Pintura! Tintas Residenciais, Automotivas e Industriais. Confira pre�os especiais para "/>
	<meta name="keywords" content="tinta, tintas, residenciais, industriais, automotiva, pintura, a casa da pintura, loja de tintas, esmalte, coral, suvinil, tintas em geral, tinta atacado" />

	<? include "componentes/includes.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<h2>Tintas - <a href="/contato.php" title="Fa�a seu or�ameto">Fa�a seu or�amento</a></h2>
					<h2><a href="/tintas-coral.php" title="Tintas Coral">Tintas Coral</a></h2>
					<ul>
						<li>
							<a href="coral/coralit-ac-bco-gelo.php" title="Tinta Coralite Acetinado Branco Gelo"><img src="slices/tintas-coral/img-coralit-ac-bco-gelo.jpg" alt="Tinta Coralite Acetinado Branco Gelo" title="Tinta Coralite Acetinado Branco Gelo"></a>
							<h3><a href="coral/coralit-ac-bco-gelo.php" title="Tinta Coralite Acetinado Branco Gelo">Coralit AC Bco Gelo</a></h3>
						</li>
						<li>
							<a href="coral/esmalte-fosco-coralit.php" title="Tinta Esmalte Fosco Coralit"><img src="slices/tintas-coral/img-esmalte-fosco-coralit.jpg" alt="Tinta Esmalte Fosco Coralit" title="Tinta Esmalte Fosco Coralit"></a>
							<h3><a href="coral/esmalte-fosco-coralit.php" title="Tinta Esmalte Fosco Coralit">Esmalte Fosco Coralit</a></h3>
						</li>
						<li>
							<a href="coral/rende-muito-coralatex-fosco.php"><img title="Tinta Rende Muito - Coralatex Fosco" alt="Tinta Rende Muito - Coralatex Fosco" src="slices/tintas-coral/img-coral-rende-muito.jpg" /></a>
							<a href="coral/rende-muito-coralatex-fosco.php" title="Tinta Rende Muito - Coralatex Fosco"><h3>Rende Muito</h3></a>							
						</li>
						<li>
							<a href="coral/coralmur-zero-odor.php"><img title="Tinta Coralmur Zero Odor" alt="Tinta Coralmur Zero Odor" src="slices/tintas-coral/img_coral_3_em_1.jpg" /></a>
							<a href="coral/coralmur-zero-odor.php" title="Tinta Coralmur Zero Odor"><h3>Coral 3 em 1</h3></a>							
						</li>
						<li class="NoMargin">
							<a href="coral/coralar-esm-acetinado.php"><img title="Tinta Decora Acetinado" alt="Tinta Decora Acetinado" src="slices/tintas-coral/img-decora-acetinado.jpg" /></a>
							<a href="coral/coralar-esm-acetinado.php" title="Tinta Decora Acetinado"><h3>Decora Acetinado</h3></a>							
						</li>
						<li>
							<a href="coral/decora-brancos.php"><img title="Tinta Decora Brancos" alt="Tinta Decora Brancos" src="slices/tintas-coral/img-decora-brancos.jpg" /></a>
							<a href="coral/decora-brancos.php" title="Tinta Decora Brancos"><h3>Decora Brancos</h3></a>							
						</li>
						<li>
							<a href="coral/decora-cores.php"><img title="Tinta Decora Cores" alt="Tinta Decora Cores" src="slices/tintas-coral/img-decora-cores.jpg" /></a>
							<a href="coral/decora-cores.php" title="Tinta Decora Cores"><h3>Decora Cores</h3></a>							
						</li>		
						<li>
							<a href="coral/decora-luz-espaco.php"><img title="Tinta Decora Luz e Espa�o" alt="Tinta Decora Luz e Espa�o " src="slices/tintas-coral/img-decora-luz-espaco.jpg" /></a>
							<a href="coral/decora-luz-espaco.php" title="Tinta Decora Luz e Espa�o"><h3>Decora Luz e Espa�o</h3></a>							
						</li>
						<li>
							<a href="coral/super-lavavel.php"><img title="Tinta Super Lav�vel" alt="Tinta Super Lav�vel" src="slices/tintas-coral/img-super-lavavel.jpg" /></a>
							<a href="coral/super-lavavel.php" title="Tinta Super Lav�vel"><h3>Super Lav�vel</h3></a>							
						</li>
						<li class="NoMargin">
							<a href="coral/pinta-piso.php"><img title="Tinta Pinta Piso" alt="Tinta Pinta Piso" src="slices/tintas-coral/img-pinta-piso.jpg" /></a>
							<a href="coral/pinta-piso.php" title="Tinta Pinta Piso"><h3>Pinta Piso</h3></a>							
						</li>
						<li>
							<a href="coral/direto-no-gesso.php"><img title="Tinta Direto no Gesso" alt="Tinta Direto no Gesso" src="slices/tintas-coral/img-direto-gesso.jpg" /></a>
							<a href="coral/direto-no-gesso.php" title="Tinta Direto no Gesso"><h3>Direto no Gesso</h3></a>							
						</li>
						<li>
							<a href="coral/massa-acrilica.php"><img title="Massa Acr�lica Coral" alt="Massa Acr�lica Coral" src="slices/tintas-coral/img-massa-acrilica-coral.jpg" /></a>
							<a href="coral/massa-acrilica.php" title="Massa Acr�lica Coral"><h3>Massa Acr�lica</h3></a>							
						</li>					
						<li>
							<a href="coral/seladora-madeira.php"><img title="Tintas Seladora Madeira" alt="Tintas Seladora Madeira" src="slices/tintas-coral/img-seladora-madeira.jpg" /></a>
							<a href="coral/seladora-madeira.php" title="Tinta Seladora Madeira"><h3>Seladora Madeira</h3></a>							
						</li>
						<li>
							<a href="coral/coralar-esmalte.php"><img title="Tintas Coralar Esmalte" alt="Tintas Coralar Esmalte" src="slices/tintas-coral/img-coralar-esmalte.jpg" /></a>
							<a href="coral/coralar-esmalte.php" title="Tintas Coralar Esmalte"><h3>Coralar Esmalte</h3></a>							
						</li>	
						<li class="NoMargin">
							<a href="coral/solvente-para-hammerite.php"><img title="Solvente para Hammerite" alt="Solvente para Hammerite" src="slices/tintas-coral/img-solvente-hammerite.jpg" /></a>
							<a href="coral/solvente-para-hammerite.php" title="Solvente para Hammerite"><h3>Solvente Hammerite</h3></a>							
						</li>
						<li>
							<a href="coral/chega-de-mofo.php"><img title="Coral Chega de Mofo" alt="Coral Chega de Mofo" src="slices/tintas-coral/img-chega-de-mofo.jpg" /></a>
							<a href="coral/chega-de-mofo.php" title="Coral Chega de Mofo"><h3>Coral Chega de Mofo</h3></a>							
						</li>
						<li>
							<a href="coral/ceramica.php"><img title="Cer�mica Coral" alt="Cer�mica Coral" src="slices/tintas-coral/img-ceramica-coral.jpg" /></a>
							<a href="coral/ceramica.php" title="Cer�mica Coral"><h3>Cer�mica</h3></a>							
						</li>
						<li>
							<a href="coral/grafite.php"><img title="Grafite Coral" alt="Grafite Coral" src="slices/tintas-coral/img-grafite-coral.jpg" /></a>
							<a href="coral/grafite.php" title="Grafite Coral"><h3>Grafite</h3></a>							
						</li>
						<li>
							<a href="coral/massa-para-madeira.php"><img title="Massa para Madeira" alt="Massa para Madeira" src="slices/tintas-coral/img-massa-para-madeira.jpg" /></a>
							<a href="coral/massa-para-madeira.php" title="Massa para Madeira"><h3>Massa para Madeira</h3></a>							
						</li>
						<li class="NoMargin">
							<a href="coral/sol-e-chuva.php"><img title="Tintas Sol e Chuva" alt="Tintas Sol e Chuva" src="slices/tintas-coral/img-sol-e-chuva.jpg" /></a>
							<a href="coral/sol-e-chuva.php" title="Tintas Sol e Chuva"><h3>Sol e Chuva</h3></a>							
						</li>
						<li>
							<a href="coral/selador-acrilico.php"><img title="Tintas Selador Acr�lico" alt="Tintas Selador Acr�lico" src="slices/tintas-coral/img-selador-acrilico.jpg" /></a>
							<a href="coral/selador-acrilico.php" title="Tintas Selador Acr�lico"><h3>Selador Acr�lico</h3></a>							
						</li>
						<li>
							<a href="coral/hammerite-esmalte-sintetico.php"><img title="Hammerite Esmalte Sint�tico Anti-Ferrugem" alt="Hammerite Esmalte Sint�tico Anti-Ferrugem" src="slices/tintas-coral/img-esmalte-sintetico.jpg" /></a>
							<a href="coral/hammerite-esmalte-sintetico.php" title="Hammerite Esmalte Sint�tico Anti-Ferrugem"><h3>Esmalte Sint�tico</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-cetol.php"><img title="Verniz Sparlack Cetol" alt="Verniz Sparlack Cetol" src="slices/tintas-coral/img-cetol.jpg" /></a>
							<a href="coral/sparlack-cetol.php" title="Verniz Sparlack Cetol"><h3>Cetol</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-cetol-antiderrapante.php"><img title="Verniz Cetol Anti Derrapante" alt="Verniz Cetol Antiderrapante" src="slices/tintas-coral/img-cetol.jpg" /></a>
							<a href="coral/sparlack-cetol-antiderrapante.php" title="Verniz Cetol Anti Derrapante"><h3>Cetol Anti Derrap GL</h3></a>							
						</li>
						<li class="NoMargin">
							<a href="coral/sparlack-neutrex.php"><img title="Verniz Sparlack Neutrex" alt="Verniz Sparlack Neutrex" src="slices/tintas-coral/img-sparlack-neutrex.jpg" /></a>
							<a href="coral/sparlack-neutrex.php" title="Verniz Sparlack Neutrex"><h3>Neutrex</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-extra-maritimo.php"><img title="Verniz Sparlack Extra Mar�timo" alt="Verniz Sparlack Extra Mar�timo" src="slices/tintas-coral/img-extra-maritimo.jpg" /></a>
							<a href="coral/sparlack-extra-maritimo.php" title="Verniz Sparlack Extra Mar�timo"><h3>Extra Mar�timo</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-maritimo-fosco.php"><img title="Verniz Sparlack Mar�timo Fosco" alt="Verniz Sparlack Mar�timo Fosco" src="slices/tintas-coral/img-maritimo-fosco.jpg" /></a>
							<a href="coral/sparlack-maritimo-fosco.php" title="Verniz Sparlack Mar�timo Fosco"><h3>Mar�timo Fosco</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-seladora.php"><img title="Verniz Sparlack Seladora" alt="Verniz Sparlack Seladora" src="slices/tintas-coral/img-seladora.jpg" /></a>
							<a href="coral/sparlack-seladora.php" title="Verniz Sparlack Seladora"><h3>Seladora</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-tingidor.php"><img title="Sparlack Tingidor" alt="Sparlack Tingidor" src="slices/tintas-coral/img-tingidor.jpg" /></a>
							<a href="coral/sparlack-tingidor.php" title="Sparlack Tingidor"><h3>Tingidor</h3></a>							
						</li>
						<li class="NoMargin">
							<a href="coral/fundo-preparador-paredes-base-agua.php"><img title="Fundo Preparador de Paredes Base �gua" alt="Fundo Preparador de Paredes Base �gua" src="slices/tintas-coral/img-preparador.jpg" /></a>
							<a href="coral/fundo-preparador-paredes-base-agua.php" title="Fundo Preparador de Paredes Base �gua"><h3>Fundo Preparador</h3></a>							
						</li>
						<li>
							<a href="coral/corante-base-dagua.php"><img title="Corante Base �gua Coral" alt="Corante Base �gua Coral" src="slices/tintas-coral/img-corante-base-agua.jpg" /></a>
							<a href="coral/corante-base-dagua.php" title="Corante Base �gua Coral"><h3>Corante Base �gua Coral</h3></a>							
						</li>
						<li>
							<a href="coral/coralar-acrilico.php"><img title="Coralar Acr�lico" alt="Coralar Acr�lico" src="slices/tintas-coral/img-coralar-acrilico.jpg" /></a>
							<a href="coral/coralar-acrilico.php" title="Coralar Acr�lico"><h3>Coralar Acr�lico</h3></a>							
						</li>
						<li>
							<a href="coral/verniz-acrilico.php"><img title="Verniz Acr�lico" alt="Verniz Acr�lico" src="slices/tintas-coral/img-verniz-acrilico.jpg" /></a>
							<a href="coral/verniz-acrilico.php" title="Verniz Acr�lico"><h3>Verniz Acr�lico</h3></a>							
						</li>
						<li>
							<a href="coral/zarcoral.php"><img title="Zarcoral " alt="Zarcoral" src="slices/tintas-coral/img-zarcoral.jpg" /></a>
							<a href="coral/zarcoral.php" title="Zarcoral"><h3>Zarcoral</h3></a>							
						</li class="NoMargin">
					</ul>
					<h2><a href="tintas-advance.php"title="Advance Tintas">Advance Tintas</a></h2>
					<h2>Base �gua - Linha Ecocl�nic</h2>
					<ul>
						<li>
							<a href="advance/ecoclinic-acrilico.php" title="Tinta Advance Ecoclinic Acr�lico" alt="Tinta Advance Ecoclinic Acr�lico"><img src="slices/tintas-advance/img-ecoclinic-acrilico.jpg" alt="Tinta Advance Ecoclinic Acr�lico" title="Tinta Advance Ecoclinic Acr�lico"></a>
							<h3><a href="advance/esmalte-altobrilho-coralit.php" title="Tinta Advance Ecoclinic Acr�lico">Ecoclinic Acr�lico</a></h3>
						</li>
						<li>
							<a href="advance/ecomar.php" title="Tinta Advance Ecomar"><img src="slices/tintas-advance/img-ecoclinic-acrilico.jpg" alt="Tinta Advance Ecomar" title="Tinta Advance Ecomar"></a>
							<h3><a href="advance/ecomar.php" title="Tinta Advance Ecomar">Ecomar</a></h3>
						</li>		
						<li>
							<a href="advance/ecopoxi-828.php" title="Tinta Advance Ecopoxi 828 "><img src="slices/tintas-advance/img-ecopoxi.jpg" alt="Tinta Advance Ecopoxi 828 " title="Tinta Advance Ecopoxi 828 "></a>
							<h3><a href="advance/ecopoxi-828.php" title="Tinta Advance Ecopoxi 828 ">Ecopoxi 828</a></h3>
						</li>	
						<li>
							<a href="advance/ecopoxi-841-primer.php" title="Ecopoxi 841 Primer"><img src="slices/tintas-advance/img-ecopoxi-841-primer.jpg" alt="Ecopoxi 841 Primer" title="Ecopoxi 841 Primer"></a>
							<h3><a href="advance/ecopoxi-841-primer.php" title="Tinta Advance Ecopoxi 828 ">Ecopoxi 841 Primer</a></h3>
						</li>				
					</ul>
					<h2>Base Solvente - Linha Epoxi</h2>
					<ul>
						<li>
							<a href="advance/96-acabamento-brilhante.php" title="96 Acab. Brilhante" alt="96 Acab. Brilhante"><img src="slices/tintas-advance/img-epoxi-acabamento-brilhante.jpg" alt="96 Acab. Brilhante" title="96 Acab. Brilhante"></a>
							<h3><a href="advance/96-acabamento-brilhante.php" title="96 Acab. Brilhante">96 Acab. Brilhante</a></h3>
						</li>
						<li>
							<a href="advance/adepoxi-86df.php" title="Adepoxi 86 Df" alt="Adepoxi 86 Df"><img src="slices/tintas-advance/img-epoxi-acabamento-brilhante.jpg" alt="Adepoxi 86 Df" title="Adepoxi 86 Df"></a>
							<h3><a href="advance/adepoxi-86df.php" title="Adepoxi 86 Df">Adepoxi 86 Df</a></h3>
						</li>
						<li>
							<a href="advance/adepoxi-80df.php" title="Adepoxi 80 Df" alt="Adepoxi 80 Df"><img src="slices/tintas-advance/img-adepoxi-80.jpg" alt="Adepoxi 80 Df" title="Adepoxi 80 Df"></a>
							<h3><a href="advance/adepoxi-80df.php" title="Adepoxi 80 Df">Adepoxi 80 Df</a></h3>
						</li>
						<li>
							<a href="advance/70-tl-primer.php" title="70 TL Primer" alt="70 TL Primer"><img src="slices/tintas-advance/img-70-tl-primer.jpg" alt="70 TL Primer" title="70 TL Primer"></a>
							<h3><a href="advance/70-tl-primer.php" title="70 TL Primer">70 TL Primer</a></h3>
						</li>
						<li class="NoMargin">
							<a href="advance/122-primer.php" title="122 Primer" alt="122 Primer"><img src="slices/tintas-advance/img-122-primer.jpg" alt="122 Primer" title="122 Primer"></a>
							<h3><a href="advance/122-primer.php" title="122 Primer">122 Primer</a></h3>
						</li>
						<li>
							<a href="advance/870-primer-fosco.php" title="870 Primer Fosco" alt="870 Primer Fosco"><img src="slices/tintas-advance/img-870-primer-fosco.jpg" alt="870 Primer Fosco" title="122 Primer"></a>
							<h3><a href="advance/870-primer-fosco.php" title="122 Primer">870 Primer Fosco</a></h3>
						</li>
						<li>
							<a href="advance/878-primer-acetinado.php" title="878 Primer Acetinado" alt="878 Primer Acetinado"><img src="slices/tintas-advance/img-870-primer-fosco.jpg" alt="878 Primer Acetinado" title="878 Primer Acetinado"></a>
							<h3><a href="advance/878-primer-acetinado.php" title="878 Primer Acetinado">878 Primer Acetinado</a></h3>
						</li>
						<li>
							<a href="advance/70-tl-acabamento.php" title="70 TL Acabamento" alt="70 TL Acabamento"><img src="slices/tintas-advance/img-70-tl-primer.jpg" alt="70 TL Acabamento" title="70 TL Acabamento"></a>
							<h3><a href="advance/70-tl-acabamento.php" title="70 TL Acabamento">70 TL Acabamento</a></h3>
						</li>					
					</ul>
					<h2>Base Solvente - Alqu�dico</h2>
					<ul>
						<li>
							<a href="advance/503-adlux-df-brilhante-extra-rapido.php" title="503 Adlux DF" alt="503 Adlux DF"><img src="slices/tintas-advance/img-530-adlux-df-brilhante.jpg" alt="503 Adlux DF" title="503 Adlux DF"></a>
							<h3><a href="advance/503-adlux-df-brilhante-extra-rapido.php" title="503 Adlux DF">503 Adlux DF  </a></h3>
						</li>
						<li>
							<a href="advance/601-adlux-df-semibrilho-extra-rapido.php" title="601 Adlux DF" alt="601 Adlux DF"><img src="slices/tintas-advance/img-601-adlux.jpg" alt="601 Adlux DF" title="601 Adlux DF"></a>
							<h3><a href="advance/601-adlux-df-semibrilho-extra-rapido.php" title="601 Adlux DF">601 Adlux DF</a></h3>
						</li>
						<li>
							<a href="advance/519-adlux-primier-fosco.php" title="519 Adlux P Fosco" alt="519 Adlux P Fosco "><img src="slices/tintas-advance/img-519-adlux-p-fosco.jpg" alt="601 Adlux DF" title="519 Adlux P Fosco"></a>
							<h3><a href="advance/519-adlux-primier-fosco.php" title="519 Adlux P Fosco">519 Adlux P Fosco</a></h3>
						</li>
						<li>
							<a href="advance/630-adlux-acabamento-brilhante.php" title="630 Adlux Acabamento Brilhante" alt="630 Adlux Acabamento Brilhante"><img src="slices/tintas-advance/img-630-adlux.jpg" alt="630 Adlux Acabamento Brilhante" title="630 Adlux Acabamento Brilhante"></a>
							<h3><a href="advance/630-adlux-acabamento-brilhante.php" title="630 Adlux Acabamento Brilhante">630 Adlux Brilhante</a></h3>
						</li>
					</ul>
					<h2>Base Solvente - Poliuretano</h2>
					<ul>
						<li>
							<a href="advance/772-DF-sem-brilho.php" title="772 DF Sem Brilho" alt="772 DF Sem Brilho"><img src="slices/tintas-advance/img-772-df-sem-brilho.jpg" alt="772 DF Sem Brilho" title="772 DF Sem Brilho"></a>
							<h3><a href="advance/772-DF-sem-brilho.php" title="772 DF Sem Brilho">772 DF Sem Brilho</a></h3>
						</li>
						<li>
							<a href="advance/adpoly-7990-acabamento.php" title="7AdPoly 7990 Acabamento" alt="AdPoly 7990 Acabamento"><img src="slices/tintas-advance/img-adpoly-7990.jpg" alt="AdPoly 7990 Acabamento" title="AdPoly 7990 Acabamento"></a>
							<h3><a href="advance/adpoly-7990-acabamento.php" title="AdPoly 7990 Acabamento">AdPoly 7990 Acabamento</a></h3>
						</li>
					</ul>
					<h2><a href="/tintas-lazzuril.php" title="Tintas Lazzuril">Tintas Lazzuril</a></h2>
					<ul>
						<li>
							<a href="lazzuril/verniz-bicomponente-8000.php" title="Verniz Bicomponente 8000" alt="Verniz Bicomponente 8000"><img src="slices/tintas-lazzuril/img-verniz-bicomponente-8000.jpg" alt="Verniz Bicomponente 8000" title="Verniz Bicomponente 8000"></a>
							<h3><a href="lazzuril/verniz-bicomponente-8000.php" title="Verniz Bicomponente 8000">Bicomponente 8000</a></h3>
						</li>		
						<li>
							<a href="lazzuril/verniz-alto-solidos-8937.php" title="Verniz Alto S�lido" alt="Verniz Alto S�lido"><img src="slices/tintas-lazzuril/img-verniz-auto-solidos.jpg" alt="Verniz Alto S�lido" title="Verniz Alto S�lido"></a>
							<h3><a href="lazzuril/verniz-alto-solidos-8937.php" title="Verniz Alto S�lido">Verniz Alto S�lido</a></h3>
						</li>	
						<li>
							<a href="lazzuril/ulltra-performance-clearcoat-CC900.php" title="Ultra Performance Clearcoat CC900" alt="Ultra Performance Clearcoat CC900"><img src="slices/tintas-lazzuril/img-clearcoat-CC900.jpg" alt="Ultra Performance Clearcoat CC900" title="Ultra Performance Clearcoat Cc900"></a>
							<h3><a href="lazzuril/ulltra-performance-clearcoat-CC900.php" title="Ultra Performance Clearcoat CC900">Clearcoat CC900</a></h3>
						</li>	
						<li>
							<a href="lazzuril/ulltra-performance-clearcoat-CC940.php" title="Ultra Performance Clearcoat CC940" alt="Ultra Performance Clearcoat CC940"><img src="slices/tintas-lazzuril/img-clearcoat-CC900.jpg" alt="Ultra Performance Clearcoat CC940" title="Ultra Performance Clearcoat CC940"></a>
							<h3><a href="lazzuril/ulltra-performance-clearcoat-CC940.php" title="Ultra Performance Clearcoat CC940">Clearcoat CC940</a></h3>
						</li>		
						<li class="NoMargin">
							<a href="lazzuril/verniz-bicomponente-8500.php" title="Verniz Bi-Componente 8500" alt="Verniz Bi-Componente 8500"><img src="slices/tintas-lazzuril/img-verniz-bicomponente-8500.jpg" alt="Verniz Bi-Componente 8500" title="Verniz Bi-Componente 8500"></a>
							<h3><a href="lazzuril/verniz-bicomponente-8500.php" title="Verniz Bi-Componente 8500">Bicomponente 8500 </a></h3>
						</li>	
						<li>
							<a href="lazzuril/primer-universal-018.php" title="Primer Universal 018" alt="Primer Universal 018"><img src="slices/tintas-lazzuril/img-primer-poliuretano-8100-820.jpg" alt="Primer Universal 018" title="Primer Universal 018"></a>
							<h3><a href="lazzuril/primer-universal-018.php" title="Primer Universal 018">Primer Universal 018 </a></h3>
						</li>
						<li>
							<a href="lazzuril/primer-ultrafill-020-022.php" title="Primer Ultrafill 020" alt="Primer Ultrafill 020"><img src="slices/tintas-lazzuril/img-primer-poliuretano-8100-820.jpg" alt="Primer Universal 020" title="Primer Universal 020"></a>
							<h3><a href="lazzuril/primer-ultrafill-020-022.php" title="Primer Ultrafill 020">Primer Ultrafill 020</a></h3>
						</li>
						<li>
							<a href="lazzuril/primer-poliuretano-p710.php" title="Primer Poliur. p710" alt="Primer Poliur. p710"><img src="slices/tintas-lazzuril/img-primer-poliur-p710.jpg" alt="Primer Poliur. p710" title="Primer Poliur. p710"></a>
							<h3><a href="lazzuril/primer-poliuretano-p710.php" title="Primer Poliur. p710">Primer Poliur. p710  </a></h3>
						</li>
						<li>
							<a href="lazzuril/primer-poliuretano-8100-8200.php" title="Primer Poliur. 8100" alt="Primer Poliur. 8100"><img src="slices/tintas-lazzuril/img-primer-poliuretano-8100-820.jpg" alt="Primer Poliur. 8100" title="Primer Poliur. 8100"></a>
							<h3><a href="lazzuril/primer-poliuretano-8100-8200.php" title="Primer Poliur. 8100">Primer Poliur. 8100</a></h3>
						</li>
						<li class="NoMargin">
							<a href="lazzuril/ultra-7000-poliester.php" title="Ultra 7000 Poliester " alt="Ultra 7000 Poliester "><img src="slices/tintas-lazzuril/img-clearcoat-CC900.jpg" alt="Ultra 7000 Poliester " title="Ultra 7000 Poliester "></a>
							<h3><a href="lazzuril/ultra-7000-poliester.php" title="Ultra 7000 Poliester ">Ultra 7000 Poliester </a></h3>
						</li>
						<li>
							<a href="lazzuril/endurecedor-lento.php" title="Endurecedor Lento" alt="Endurecedor Lento"><img src="slices/tintas-lazzuril/img-verniz-auto-solidos.jpg" alt="Endurecedor Lento" title="Endurecedor Lento"></a>
							<h3><a href="lazzuril/endurecedor-lento.php" title="Endurecedor Lento">Endurecedor Lento</a></h3>
						</li>
						<li>
							<a href="lazzuril/endurecedor-rapido.php" title="Endurecedor R�pido" alt="Endurecedor R�pido"><img src="slices/tintas-lazzuril/img-verniz-auto-solidos.jpg" alt="Endurecedor R�pido" title="Endurecedor R�pido"></a>
							<h3><a href="lazzuril/endurecedor-rapido.php" title="Endurecedor R�pido">Endurecedor R�pido</a></h3>
						</li>
						<li>
							<a href="lazzuril/aditivo-anticratera.php" title="Aditivo Anticratera" alt="Aditivo Anticratera"><img src="slices/tintas-lazzuril/img-verniz-auto-solidos.jpg" alt="Aditivo Anticratera" title="Aditivo Anticratera"></a>
							<h3><a href="lazzuril/aditivo-anticratera.php" title="Aditivo Anticratera">Aditivo Anticratera</a></h3>
						</li>
						<li>
							<a href="lazzuril/acelerador-de-secagem.php" title="Acelerador Secagem " alt="Acelerador Secagem "><img src="slices/tintas-lazzuril/img-verniz-auto-solidos.jpg" alt="Acelerador Secagem " title="Acelerador Secagem "></a>
							<h3><a href="lazzuril/acelerador-de-secagem.php" title="Acelerador Secagem ">Acelerador Secagem </a></h3>
						</li>
						<li class="NoMargin">
							<a href="lazzuril/seladora-para-plastico.php" title="Seladora para Pl�stico" alt="Seladora para Pl�stico"><img src="slices/tintas-lazzuril/img-primer-poliuretano-8100-820.jpg" alt="Seladora para Pl�stico" title="Seladora para Pl�stico"></a>
							<h3><a href="lazzuril/seladora-para-plastico.php" title="Seladora para Pl�stico">Seladora para Pl�stico</a></h3>
						</li>
						<li>
							<a href="lazzuril/verniz-bicomponente-6100.php" title="Bicomponente 6100 " alt="Bicomponente 6100"><img src="slices/tintas-lazzuril/img-verniz-componente-6100.jpg" alt="Bicomponente 6100 " title="Bicomponente 6100 "></a>
							<h3><a href="lazzuril/verniz-bicomponente-6100.php" title="Bicomponente 6100 ">Bicomponente 6100</a></h3>
						</li>
						<li>
							<a href="lazzuril/verniz-bicomponente-7100.php" title="Bicomponente 7100" alt="Bicomponente 7100"><img src="slices/tintas-lazzuril/img-verniz-componente-6100.jpg" alt="Bicomponente 7100" title="Bicomponente 7100"></a>
							<h3><a href="lazzuril/verniz-bicomponente-7100.php" title="Bicomponente 7100">Bicomponente 7100</a></h3>
						</li>
						<li>
							<a href="lazzuril/verniz-bicomponente-4500.php" title="Bicomponente 4500 " alt="Bicomponente 45004500 "><img src="slices/tintas-lazzuril/img-verniz-componente-4000.jpg" alt="Bicomponente 4500 " title="Bicomponente 4500"></a>
							<h3><a href="lazzuril/verniz-bicomponente-4500.php" title="Bicomponente 4500 ">Bicomponente 4500</a></h3>
						</li>
						<li>
							<a href="lazzuril/verniz-bicomponente-4000.php" title="Bicomponente 4000 " alt="Bicomponente 4000 "><img src="slices/tintas-lazzuril/img-verniz-componente-4000.jpg" alt="Bicomponente 4000 " title="Bicomponente 4000"></a>
							<h3><a href="lazzuril/verniz-bicomponente-4000.php" title="Bicomponente 4000 ">Bicomponente 4000</a></h3>
						</li>
						<li class="NoMargin">
							<a href="lazzuril/primer-poliuretano.php" title="Primer Poiuretano" alt="Primer Poiuretano"><img src="slices/tintas-lazzuril/img-primer-poliur-p710.jpg" alt="Primer Poiuretano" title="Primer Poiuretano"></a>
							<h3><a href="lazzuril/primer-poliuretano.php" title="Primer Poiuretano">Primer Poiuretano</a></h3>
						</li>
						<li>
							<a href="lazzuril/massa-pequenas-correcoes.php" title="Massa Pequenas Corre��es" alt="Massa Pequenas Corre��es"><img src="slices/tintas-lazzuril/img-massa_para_pequenas_correcoes.jpg" alt="Massa Pequenas Corre��es" title="Massa Pequenas Corre��es"></a>
							<h3><a href="lazzuril/massa-pequenas-correcoes.php" title="Massa Pequenas Corre��es">Massa Peq. Corre��es </a></h3>
						</li>
						<li>
							<a href="lazzuril/lazzudur-poliuretano-hs.php" title="Poliuretano" alt="Poliuretano"><img src="slices/tintas-lazzuril/img-lazzudur-poliuretano-hs.jpg" alt="Poliuretano" title="Poliuretano"></a>
							<h3><a href="lazzuril/lazzudur-poliuretano-hs.php" title="Poliuretano">Poliuretano</a></h3>
						</li>
						<li>
							<a href="lazzuril/awx-poliester-base-agua.php" title="Awx � Poliester Base �gua" alt="Awx � Poliester Base �gua"><img src="slices/tintas-lazzuril/img-awx-poliester-base-agua.jpg" alt="Awx � Poliester Base �gua" title="Awx � Poliester Base �gua"></a>
							<h3><a href="lazzuril/awx-poliester-base-agua.php" title="Awx � Poliester Base �gua">AWX</a></h3>
						</li>
						<li>
							<a href="lazzuril/ultra-7000-high-performance-clearcoat-hpc15.php" title="Ultra 7000" alt="Ultra 7000"><img src="slices/tintas-lazzuril/img-clearcoat-CC900.jpg" alt="Ultra 7000" title="Ultra 7000"></a>
							<h3><a href="lazzuril/ultra-7000-high-performance-clearcoat-hpc15.php" title="Ultra 7000">Ultra 7000</a></h3>
						</li>
						<li class="NoMargin">
							<a href="lazzuril/acabamento-poliester-ms.php" title="Acabamento Poli�ster MS" alt="Acabamento Poli�ster MS"><img src="slices/tintas-lazzuril/img-verniz-bicomponente-8500.jpg" alt="Acabamento Poli�ster MS" title="Acabamento Poli�ster MS"></a>
							<h3><a href="lazzuril/acabamento-poliester-ms.php" title="Acabamento Poli�ster MS">Poli�ster MS</a></h3>
						</li>
						<li>
							<a href="lazzuril/primer-2kp-411.php" title="Primer 2K P- 411" alt="Primer 2K P- 411"><img src="slices/tintas-lazzuril/img-primer-poliur-p710.jpg" alt="Primer 2K P- 411" title="Primer 2K P- 411"></a>
							<h3><a href="lazzuril/primer-2kp-411.php" title="Primer 2K P- 411">Primer 2K P- 411</a></h3>
						</li>
						<li>
							<a href="lazzuril/esmalte-sintetico-catalisado.php" title="Esmalte Sint�tico Catalisado" alt="Esmalte Sint�tico Catalisado"><img src="slices/tintas-lazzuril/img-esmalte-sintetico-catalisad.jpg" alt="Esmalte Sint�tico Catalisado" title="Esmalte Sint�tico Catalisado"></a>
							<h3><a href="lazzuril/esmalte-sintetico-catalisado.php" title="Esmalte Sint�tico Catalisado">Esmalte Sint�tico</a></h3>
						</li>
					</ul>
					<h2><a href="tintas-spray.php" title="Spray Colorgin">Spray Colorgin</a></h2>
					<ul>
						<li>
							<a href="spray/colorgin-alta-temperatura.php" title="Colorgin Alta Temperatura" alt="Colorgin Alta Temperatura"><img src="slices/spray-colorgin/img-colorgin-alta-temperatura.jpg" alt="Colorgin Alta Temperatura" title="Colorgin Alta Temperatura"></a>
							<h3><a href="spray/colorgin-alta-temperatura.php" title="Colorgin Alta Temperatura">Colorgin Alta Temperatura</a></h3>
						</li>
						<li>
							<a href="spray/colorgin-alumen.php" title="Colorgin Alumen" alt="Colorgin Alumen"><img src="slices/spray-colorgin/img-colorgin-alumen.jpg" alt="Colorgin Alumen" title="Colorgin Alumen"></a>
							<h3><a href="spray/colorgin-alumen.php" title="Colorgin Alumen">Colorgin Alumen</a></h3>
						</li>	
						<li>
							<a href="spray/colorgin-antiderrapante.php" title="Colorgin Antiderrapante" alt="Colorgin Antiderrapante "><img src="slices/spray-colorgin/img-colorin-antiderrapante.jpg" alt="Colorgin Antiderrapante" title="Colorgin Antiderrapante"></a>
							<h3><a href="spray/colorgin-antiderrapante.php" title="Colorgin Antiderrapante">Antiderrapante </a></h3>
						</li>
						<li>
							<a href="spray/colorgin-automotiva.php" title="Colorgin Automotiva" alt="Colorgin Automotiva"><img src="slices/spray-colorgin/img-colorgin-automotiva.jpg" alt="Colorgin Automotiva" title="Colorgin Automotiva"></a>
							<h3><a href="spray/colorgin-automotiva.php" title="Colorgin Automotiva">Automotiva</a></h3>
						</li>	
						<li class="NoMargin">
							<a href="spray/decor-primer-cinza.php" title="Decor Spray Primer Cinza" alt="Decor Spray Primer Cinza"><img src="slices/spray-colorgin/img-colorgin-decor-spray-primer.jpg" alt="Decor Spray Primer Cinza" title="Decor Spray Primer Cinza"></a>
							<h3><a href="spray/decor-primer-cinza.php" title="Decor Spray Primer Cinza">Decor Primer Cinza</a></h3>
						</li>
						<li>
							<a href="spray/colorgin-lubgin.php" title="Colorgin Lubgin" alt="Colorgin Lubgin"><img src="slices/spray-colorgin/img-colorgin-lubgin.jpg" alt="Colorgin Lubgin" title="Colorgin Lubgin"></a>
							<h3><a href="spray/colorgin-lubgin.php" title="Colorgin Lubgin">Lubgin</a></h3>
						</li>
						<li>
							<a href="spray/colorgin-decor.php" title="Colorgin Decor" alt="Colorgin Decor"><img src="slices/spray-colorgin/img-colorgin-decor-spray-primer.jpg" alt="Colorgin Decor" title="Colorgin Decor"></a>
							<h3><a href="spray/colorgin-decor.php" title="Colorgin Decor">Colorgin Decor</a></h3>
						</li>
						<li>
							<a href="spray/colorgin-fosforecente.php" title="Colorgin Fosforecente" alt="Colorgin Fosforecenter"><img src="slices/spray-colorgin/img-colorgin-fosforecente.jpg" alt="Colorgin Fosforecente" title="Colorgin Fosforecente"></a>
							<h3><a href="spray/colorgin-fosforecente.php" title="Colorgin Fosforecente">Colorgin Fosforecente</a></h3>
						</li>	
						<li>
							<a href="spray/colorgin-luminosa.php" title="Colorgin Luminosa" alt="Colorgin Luminosa"><img src="slices/spray-colorgin/img-colorgin-luminosa.jpg" alt="Colorgin Luminosa" title="Colorgin Luminosa"></a>
							<h3><a href="spray/colorgin-luminosa.php" title="Colorgin Luminosa">Colorgin Luminosa</a></h3>
						</li>
						<li class="NoMargin">
							<a href="spray/colorgin-uso-geral.php" title="Colorgin Uso Geral" alt="Colorgin Uso Geral"><img src="slices/spray-colorgin/img-colorgin-automotiva.jpg" alt="Colorgin Uso Geral" title="Colorgin Uso Geral"></a>
							<h3><a href="spray/colorgin-uso-geral.php" title="Colorgin Uso Geral">Colorgin Uso Geral</a></h3>
						</li>
						<li>
							<a href="spray/colorgin-uso-geral.php" title="Colorgin Pl�sticos" alt="Colorgin Pl�sticos"><img src="slices/spray-colorgin/img-colorgin-plasticos.jpg" alt="Colorgin Pl�sticos" title="Colorgin Pl�sticos"></a>
							<h3><a href="spray/colorgin-uso-geral.php" title="Colorgin Pl�sticos">Colorgin Pl�sticos</a></h3>
						</li>
						<li>
							<a href="spray/colorgin-silicone.php" title="Colorgin Silicone" alt="Colorgin Pl�sticos"><img src="slices/spray-colorgin/img-colorgin-silicone.jpg" alt="Colorgin Silicone" title="Colorgin Silicone"></a>
							<h3><a href="spray/colorgin-silicone.php" title="Colorgin Silicone">Colorgin Silicone</a></h3>
						</li>						
					</ul>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "componentes/rodape.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>