<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Hammerite Esmalte Sint&eacute;tico Anti-ferrugem | A Casa da Pintura</title>
	<meta name="Description" content="Coral Hammerite Esmalte Sint&eacute;tico Anti-ferrugem: O Esmalte Sint�tico Anti-Ferrugem Hammerite � um esmalte antioxidante que se aplica diretamente sobre ferro limpo ou enferrujado" />
	<meta name="Keywords" content="Tintas Coral Selador Madeira A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Tintas Hammerite Esmalte Sint&eacute;tico Anti-ferrugem" alt="Tintas Hammerite Esmalte Sint&eacute;tico Anti-ferrugem" src="../slices/tintas-coral/img-esmalte-sintetico.jpg" />
						</div>
						<h2>Tintas Hammerite Esmalte Sint&eacute;tico Anti-ferrugem</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>O Esmalte Sint&eacute;tico Anti-Ferrugem Hammerite &eacute; um esmalte  antioxidante que se aplica diretamente sobre ferro limpo ou enferrujado. N&atilde;o  necessita de fundo pr&eacute;vio. Previne e interrompe o processo de ferrugem. Seca em  1 hora. Forma uma pel&iacute;cula imperme&aacute;vel. Repele a &aacute;gua. Possui um grande poder  de cobertura e rendimento. Para superf&iacute;cies exteriores e interiores.</p>
						</div>				
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>	
								<ul>
									<li><b>Tintas Dispon�veis</b></li>
									<li>2206 HAMMERITE AC GALV. ALUMINIO PRETO 0.8LT</li>
									<li>2198 HAMMERITE BRILH. AMARELO 0.8LT</li>
									<li>2197 HAMMERITE BRILH. AMARELO 2.4LT</li>
									<li>376 HAMMERITE BRILH. AZUL 0.8LT</li>
									<li>379 HAMMERITE BRILH. AZUL 2.4LT</li>
									<li>372 HAMMERITE BRILH. BRANCO 0.8LT</li>
									<li>373 HAMMERITE BRILH. BRANCO 2.4LT</li>
									<li>375 HAMMERITE BRILH. CINZA 2.4LT</li>
									<li>2202 HAMMERITE BRILH. PRATA 2.4LT</li>
									<li>377 HAMMERITE BRILH. PRETO 0.8LT</li>
									<li>378 HAMMERITE BRILH. PRETO 2.4 LT</li>
									<li>2091 HAMMERITE BRILH. VERDE 2.4LT</li>
								</ul><ul>
								<li><b>Embalagem/Rendimento</b></li>
								<li>Brilhante</li>
								<li>Gal�o 2,4 L: Pincel 16,2 a 18,0 m� por dem�o </li>
								<li>Rolo 21,6 a 24,0 m� por dem�o</li>
								<li>Quarto 0,8 L: Pincel 5,4 a 6,0 m� por dem�o</li> 
								<li>Rolo 7,2 a 8,0 m� por dem�o
								</li>
							</ul>
							<ul>
								<li><b>Aplica&ccedil;&atilde;o</b></li>
								<li>Recomenda-se a aplica&ccedil;&atilde;o com pincel sobre superf&iacute;cies pequenas (como grades) e aplica&ccedil;&atilde;o com rolo sobre superf&iacute;cies grandes (como port&otilde;es).</li>
							</ul>
							<ul>
								<li><b>Dilui&ccedil;&atilde;o</b></li>
								<li>Aplica&ccedil;&atilde;o a pincel, n&atilde;o diluir. Aplica&ccedil;&atilde;o a rolo, diluir na propor&ccedil;&atilde;o de  10% com Solvente Hammerite. Aplica&ccedil;&atilde;o a rev&oacute;lver, diluir na propor&ccedil;&atilde;o de 50%  com Solvente Hammerite.&nbsp;<strong>N&atilde;o usar Aguarr&aacute;</strong>. </li>
							</ul>
							<ul>
								<li><b>Acabamento</b></li>
								<li>Brilhante</li>
							</ul>
							<ul>
								<li><b>Secagem</b></li>
								<li>Ao toque: 1 hora</li>
								<li>Entre dem�os: 1 a 8 horas</li>
								<li>Final: 1 hora</li>
								<li><b>Obs: Passando o tempo de repintura, deve-se esperar duas semanas para nova aplica&ccedil;&atilde;o</b></li>
							</ul>
						</div>
						<div id="TintasDisponiveis">
							<span id="TintasDisponiveisTitulo">Tintas Dispon�veis</span>
							<ul>
								<li>500ML</li>
							</ul>
						</div>
					</div>
				</div>
				<? include "../componentes/solicitar-orcamento.php"; ?>
				<? include "../componentes/outros-produtos.php"; ?>
			</div>
		</div>
	</div>
	<div id="Linha3">
		<? include "../componentes/rodape-tintas.php"; ?>
	</div>
</div>
<div id="mask"></div>
</body>
</html>