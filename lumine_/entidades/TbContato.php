<?php
#### START AUTOCODE
/**
 * Classe generada para a tabela "tb_contato"
 * in 2014-01-24
 * @author Hugo Ferreira da Silva
 * @link http://www.hufersil.com.br/lumine
 * @package entidades
 *
 */

class TbContato extends Lumine_Base {

    
    public $id;
    public $nmCliente;
    public $dsAssunto;
    public $dsEmail;
    public $cdTelefone;
    public $nmEstado;
    public $dsMensagem;
    public $dtCadastro;
    public $nmCidade;
    
    
    
    /**
     * Inicia os valores da classe
     * @author Hugo Ferreira da Silva
     * @return void
     */
    protected function _initialize()
    {
        $this->metadata()->setTablename('tb_contato');
        $this->metadata()->setPackage('entidades');
        
        # nome_do_membro, nome_da_coluna, tipo, comprimento, opcoes
        
        $this->metadata()->addField('id', 'id', 'int', 11, array('primary' => true, 'notnull' => true, 'autoincrement' => true));
        $this->metadata()->addField('nmCliente', 'nm_cliente', 'varchar', 45, array());
        $this->metadata()->addField('dsAssunto', 'ds_assunto', 'varchar', 100, array());
        $this->metadata()->addField('dsEmail', 'ds_email', 'varchar', 150, array());
        $this->metadata()->addField('cdTelefone', 'cd_telefone', 'varchar', 45, array());
        $this->metadata()->addField('nmEstado', 'nm_estado', 'varchar', 45, array());
        $this->metadata()->addField('dsMensagem', 'ds_mensagem', 'text', 4294967295, array());
        $this->metadata()->addField('dtCadastro', 'dt_cadastro', 'varchar', 45, array());
        $this->metadata()->addField('nmCidade', 'nm_cidade', 'varchar', 45, array());

        
    }

    #### END AUTOCODE


}
