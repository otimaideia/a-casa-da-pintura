<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Coral Decora Brancos | A Casa da Pintura</title>
	<meta name="Description" content="Coral Decora Brancos: Decora ajuda voc� a deixar a sua casa com a sua cara" />
	<meta name="Keywords" content="Tintas Coral Decora Brancos A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Tinta Decora Brancos" alt="Tinta Decora Brancos" src="../slices/tintas-coral/img-decora-brancos.jpg" />
						</div>
						<h2>Decora Brancos</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Decora ajuda voc� a deixar a sua casa com a sua cara. Porque a cor da parede � um elemento de decora��o muito importante para criar o ambiente que tanto quer, com seu toque especial. Se voc� se sente tranq�ilo, em escolher brancos, n�o � para menos, os brancos causam uma sensa��o de tranq�ilidade e paz.</p>
							<p>Ent�o se voc� quer uma decora��o onde tudo viva em harmonia, o branco � uma �tima pedida! � um acr�lico de acabamento muito superior e � indicado para ambientes externos e internos. Tem zero odor*, f�cil aplica��o, uma excelente cobertura e um acabamento sofisticado, encontradas nas vers�es fosca e semibrilho</p>
							<p>Decora Brancos faz parte da nova linha de produtos Zero Odor da Coral. Conhe�a os outros produtos da linha: Decora Cores, Decora Neutros, Decora Acabamento Acetinado, Super Lav�vel, Coralmur, Coralit Zero e Fundo Preparador Coralit Zero.	</p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>									
								<ul>
									<li><b>Embalagem/Rendimento</b></li>
									<li>Lata <b>18 L</b>: 200 a 300 m� por dem�o</li>
									<li>Gal�o <b>3,6 L</b>: 40 a 60 m� por dem�o</li>
								</ul>
								<ul>
									<li><b>Aplica��o</b></li>
									<li>Rolo de l� de p�lo baixo ou pincel de cerdas macias. Limpe as ferramentas com �gua e sab�o.</li>
									<li><b>Dilui��o</b></li>
									<li>Superf�cies n�o seladas, diluir a 1� dem�o em at� 50% e as demais 10 a 30% com �gua pot�vel. Superf�cies j� seladas: diluir todas as dem�os de 10 a 30% com �gua pot�vel.</li>
									<li><b>Acabamento:</b> Fosco/Semi-brilho</li>
									<li><b>Secagem: </b>Ao toque: 1/2 hora, entre dem�os: 4 horas, final: 4 horas</li>
								</ul>
							</div>
							<div id="TintasDisponiveis">
								<span id="TintasDisponiveisTitulo">Tintas Dispon�veis:</span>
								<ul>
									<li>760	DECORA SEMI-BRILHO BRANCO 18LT</li>		
									<li>759	DECORA SEMI-BRILHO BRANCO 3.6LT</li>						
								</ul>
							</div> 							
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>