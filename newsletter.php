<?php include 'lumine/config.php'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=charset=iso-8859-1" />
	<title> Newsletter | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas</title>
	<? include "componentes/includes.php"; ?>

	<!-- Google Code for Newsletters Conversion Page -->

	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 1058506448;
	var google_conversion_language = "en";
	var google_conversion_format = "2";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "LSgRCJCv_AcQ0I3e-AM";

	var google_conversion_value = 0;
	var google_remarketing_only = false;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
		<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1058506448/?value=0&amp;label=LSgRCJCv_AcQ0I3e-AM&amp;guid=ON&amp;script=0"/>
		</div>
	</noscript>
</head>
<body>
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "componentes/topo.php"; ?>
			</div>
		</div>	
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ContatoObrigado">
					<?php

					if (isset($_POST)) 
					{
						$newContato = new TbNewsletter;
						$newContato->nmCliente = $_POST['nome'];
						$newContato->dsEmail = $_POST['email'];
						

						$newContato->insert();

						$emailsender = $_POST['email'];
						$destinatario = "vendas@acasadapintura.com.br";

						if (PHP_OS == "Linux")
						$quebra_linha = "\n"; //Se for Linux
					elseif (PHP_OS == "WINNT")
						$quebra_linha = "\r\n"; // Se for Windows
					else
						//die("Este script nao esta preparado para funcionar com o sistema operacional de seu servidor");

						$quebra_linha = "\n";
					
					$headers = "MIME-Version: 1.1" . $quebra_linha;
					$headers .= "Content-type: text/html; charset=utf-8" . $quebra_linha;
					$headers .= "From: vendas@acasadapintura.com.br". $quebra_linha;
					$headers .= "Cc: marketing@acasadapintura.com.br" . $quebra_linha;
					$headers .= "Return-Path: " .$destinatario . $quebra_linha;
					
					$mensagem .= "Novo Cadastro Novidades". "<br />";
					$mensagem .= "<strong> Nome: </strong>" . utf8_encode($_POST["nome"]) . "<br />";
					$mensagem .= "<strong> Email: </strong>" . $_POST["email"] . "<br />";

					$sucesso = mail($destinatario, "Cadastro Novidades - A Casa da Pintura", $mensagem, $headers, "-r" . $emailsender);

					// para cliente

					$Mensagem = "";
					$headers="";
					
					$headers .= "MIME-Version: 1.1" . $quebra_linha;
					$headers .= "Content-type: text/html; charset=utf-8" . $quebra_linha;
					$headers .= "From: vendas@acasadapintura.com.br". $quebra_linha;
					$headers .= "Return-Path: vendas@acasadapintura.com.br";
					$headers .= "Reply-To: vendas@acasadapintura.com.br" . $quebra_linha;

					$Mensagem = "Obrigado por cadastrar seu e-mail, em breve enviaremos novidades!";

					$sucesso = mail($emailsender, $_POST["assunto"] . "Confirmação Cadastro Novidades - A Casa da Pintura", $Mensagem, $headers);


					if ($sucesso)
					{
						echo('<h2>Obrigado por cadastrar seu e-mail conosco! </h2>');
						echo('<p class="link"> Ir para <a href="index.php" title="P&aacute;gina inicial A Casa da Pintura">home.</a><img src="slices/icon-home.jpg" title="Ir para a p&aacute;gina inicial A Casa da Pintura" /alt="Ir para a página inicial A Casa da Pintura"> </p>');
					} 
					else
					{

						echo('<h2>Falha ao cadastrar email!</h2>');
						echo('<p> Por favor, tente novamente. </p>');
					}
				}

				?>
			</div>
		</div>
	</div>
	<div id="Linha3">
		<? include "componentes/rodape.php"; ?>
	</div>
</div>
<div id="mask"></div>
</body>
</html>