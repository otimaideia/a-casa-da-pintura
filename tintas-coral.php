<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Coral | Coralit, Rende Muito, Super Lav�vel, Chega de Mofo</title>
	<meta name="description" content="Confira linha completa Coral aqui na Casa da Pintura! Chega de Mofo, Linha Decora, Super Lav�vel, Direto no Gesso, Esmalte Sint�tico"/>
	<meta name="keywords" content="coralit, rende muito, super lav�vel, chega de mofo, direto no gesso, linha coral, tintas coral, a casa da pintura, seladora, zarcoral, coralar, corante" />
	<? include "componentes/includes.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<h2>Tintas Residenciais - <a href="/contato.php">Fa�a seu or�amento</a></h2>
					<h2>Tintas Coral</h2>
					<ul>
						<li>
							<a href="coral/coralit-ac-bco-gelo.php" title="Tinta Coralite Acetinado Branco Gelo"><img src="slices/tintas-coral/img-coralit-ac-bco-gelo.jpg" alt="Tinta Coralite Acetinado Branco Gelo" title="Tinta Coralite Acetinado Branco Gelo"></a>
							<h3><a href="coral/coralit-ac-bco-gelo.php" title="Tinta Coralite Acetinado Branco Gelo">Coralit AC Bco Gelo</a></h3>
						</li>
						<li>
							<a href="coral/esmalte-fosco-coralit.php" title="Tinta Esmalte Fosco Coralit"><img src="slices/tintas-coral/img-esmalte-fosco-coralit.jpg" alt="Tinta Esmalte Fosco Coralit" title="Tinta Esmalte Fosco Coralit"></a>
							<h3><a href="coral/esmalte-fosco-coralit.php" title="Tinta Esmalte Fosco Coralit">Esmalte Fosco Coralit</a></h3>
						</li>
						<li>
							<a href="coral/rende-muito-coralatex-fosco.php"><img title="Tinta Rende Muito - Coralatex Fosco" alt="Tinta Rende Muito - Coralatex Fosco" src="slices/tintas-coral/img-coral-rende-muito.jpg" /></a>
							<a href="coral/rende-muito-coralatex-fosco.php" title="Tinta Rende Muito - Coralatex Fosco"><h3>Rende Muito</h3></a>							
						</li>
						<li>
							<a href="coral/coralmur-zero-odor.php"><img title="Tinta Coralmur Zero Odor" alt="Tinta Coralmur Zero Odor" src="slices/tintas-coral/img-coralmur-zero-odor.jpg" /></a>
							<a href="coral/coralmur-zero-odor.php" title="Tinta Coralmur Zero Odor"><h3>Coralmur Zero Odor</h3></a>							
						</li>
						<li  class="NoMargin">
							<a href="coral/coralar-esm-acetinado.php"><img title="Tinta Decora Acetinado" alt="Tinta Decora Acetinado" src="slices/tintas-coral/img-decora-acetinado.jpg" /></a>
							<a href="coral/coralar-esm-acetinado.php" title="Tinta Decora Acetinado"><h3>Decora Acetinado</h3></a>							
						</li>
						<li>
							<a href="coral/decora-brancos.php"><img title="Tinta Decora Brancos" alt="Tinta Decora Brancos" src="slices/tintas-coral/img-decora-brancos.jpg" /></a>
							<a href="coral/decora-brancos.php" title="Tinta Decora Brancos"><h3>Decora Brancos</h3></a>							
						</li>
						<li>
							<a href="coral/decora-cores.php"><img title="Tinta Decora Cores" alt="Tinta Decora Cores" src="slices/tintas-coral/img-decora-cores.jpg" /></a>
							<a href="coral/decora-cores.php" title="Tinta Decora Cores"><h3>Decora Cores</h3></a>							
						</li>		
						<li>
							<a href="coral/decora-luz-espaco.php"><img title="Tinta Decora Luz e Espa�o" alt="Tinta Decora Luz e Espa�o " src="slices/tintas-coral/img-decora-luz-espaco.jpg" /></a>
							<a href="coral/decora-luz-espaco.php" title="Tinta Decora Luz e Espa�o"><h3>Decora Luz e Espa�o</h3></a>							
						</li>
						<li>
							<a href="coral/super-lavavel.php"><img title="Tinta Super Lav�vel" alt="Tinta Super Lav�vel" src="slices/tintas-coral/img-super-lavavel.jpg" /></a>
							<a href="coral/super-lavavel.php" title="Tinta Super Lav�vel"><h3>Super Lav�vel</h3></a>							
						</li>
						<li  class="NoMargin">
							<a href="coral/pinta-piso.php"><img title="Tinta Pinta Piso" alt="Tinta Pinta Piso" src="slices/tintas-coral/img-pinta-piso.jpg" /></a>
							<a href="coral/pinta-piso.php" title="Tinta Pinta Piso"><h3>Pinta Piso</h3></a>							
						</li>
						<li>
							<a href="coral/direto-no-gesso.php"><img title="Tinta Direto no Gesso" alt="Tinta Direto no Gesso" src="slices/tintas-coral/img-direto-gesso.jpg" /></a>
							<a href="coral/direto-no-gesso.php" title="Tinta Direto no Gesso"><h3>Direto no Gesso</h3></a>							
						</li>
						<li>
							<a href="coral/massa-acrilica.php"><img title="Massa Acr�lica Coral" alt="Massa Acr�lica Coral" src="slices/tintas-coral/img-massa-acrilica-coral.jpg" /></a>
							<a href="coral/massa-acrilica.php" title="Massa Acr�lica Coral"><h3>Massa Acr�lica</h3></a>							
						</li>					
						<li>
							<a href="coral/seladora-madeira.php"><img title="Tintas Seladora Madeira" alt="Tintas Seladora Madeira" src="slices/tintas-coral/img-seladora-madeira.jpg" /></a>
							<a href="coral/seladora-madeira.php" title="Tinta Seladora Madeira"><h3>Seladora Madeira</h3></a>							
						</li>
						<li>
							<a href="coral/coralar-esmalte.php"><img title="Tintas Coralar Esmalte" alt="Tintas Coralar Esmalte" src="slices/tintas-coral/img-coralar-esmalte.jpg" /></a>
							<a href="coral/coralar-esmalte.php" title="Tintas Coralar Esmalte"><h3>Coralar Esmalte</h3></a>							
						</li>	
						<li  class="NoMargin">
							<a href="coral/solvente-para-hammerite.php"><img title="Solvente para Hammerite" alt="Solvente para Hammerite" src="slices/tintas-coral/img-solvente-hammerite.jpg" /></a>
							<a href="coral/solvente-para-hammerite.php" title="Solvente para Hammerite"><h3>Solvente Hammerite</h3></a>							
						</li>
						<li>
							<a href="coral/chega-de-mofo.php"><img title="Coral Chega de Mofo" alt="Coral Chega de Mofo" src="slices/tintas-coral/img-chega-de-mofo.jpg" /></a>
							<a href="coral/chega-de-mofo.php" title="Coral Chega de Mofo"><h3>Coral Chega de Mofo</h3></a>							
						</li>
						<li>
							<a href="coral/ceramica.php"><img title="Cer�mica Coral" alt="Cer�mica Coral" src="slices/tintas-coral/img-ceramica-coral.jpg" /></a>
							<a href="coral/ceramica.php" title="Cer�mica Coral"><h3>Cer�mica</h3></a>							
						</li>
						<li>
							<a href="coral/grafite.php"><img title="Grafite Coral" alt="Grafite Coral" src="slices/tintas-coral/img-grafite-coral.jpg" /></a>
							<a href="coral/grafite.php" title="Grafite Coral"><h3>Grafite</h3></a>							
						</li>
						<li class="NoMargin">
							<a href="coral/massa-para-madeira.php"><img title="Massa para Madeira" alt="Massa para Madeira" src="slices/tintas-coral/img-massa-para-madeira.jpg" /></a>
							<a href="coral/massa-para-madeira.php" title="Massa para Madeira"><h3>Massa para Madeira</h3></a>							
						</li>
						<li  class="NoMargin">
							<a href="coral/sol-e-chuva.php"><img title="Tintas Sol e Chuva" alt="Tintas Sol e Chuva" src="slices/tintas-coral/img-sol-e-chuva.jpg" /></a>
							<a href="coral/sol-e-chuva.php" title="Tintas Sol e Chuva"><h3>Sol e Chuva</h3></a>							
						</li>
						<li>
							<a href="coral/selador-acrilico.php"><img title="Tintas Selador Acr�lico" alt="Tintas Selador Acr�lico" src="slices/tintas-coral/img-selador-acrilico.jpg" /></a>
							<a href="coral/selador-acrilico.php" title="Tintas Selador Acr�lico"><h3>Selador Acr�lico</h3></a>							
						</li>
						<li>
							<a href="coral/hammerite-esmalte-sintetico.php"><img title="Hammerite Esmalte Sint�tico Anti-Ferrugem" alt="Hammerite Esmalte Sint�tico Anti-Ferrugem" src="slices/tintas-coral/img-esmalte-sintetico.jpg" /></a>
							<a href="coral/hammerite-esmalte-sintetico.php" title="Hammerite Esmalte Sint�tico Anti-Ferrugem"><h3>Esmalte Sint�tico</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-cetol.php"><img title="Verniz Sparlack Cetol" alt="Verniz Sparlack Cetol" src="slices/tintas-coral/img-cetol.jpg" /></a>
							<a href="coral/sparlack-cetol.php" title="Verniz Sparlack Cetol"><h3>Cetol</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-cetol-antiderrapante.php"><img title="Verniz Cetol Anti Derrapante" alt="Verniz Cetol Antiderrapante" src="slices/tintas-coral/img-cetol.jpg" /></a>
							<a href="coral/sparlack-cetol-antiderrapante.php" title="Verniz Cetol Anti Derrapante"><h3>Cetol Anti Derrap GL</h3></a>							
						</li>
						<li  class="NoMargin">
							<a href="coral/sparlack-neutrex.php"><img title="Verniz Sparlack Neutrex" alt="Verniz Sparlack Neutrex" src="slices/tintas-coral/img-sparlack-neutrex.jpg" /></a>
							<a href="coral/sparlack-neutrex.php" title="Verniz Sparlack Neutrex"><h3>Neutrex</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-extra-maritimo.php"><img title="Verniz Sparlack Extra Mar�timo" alt="Verniz Sparlack Extra Mar�timo" src="slices/tintas-coral/img-extra-maritimo.jpg" /></a>
							<a href="coral/sparlack-extra-maritimo.php" title="Verniz Sparlack Extra Mar�timo"><h3>Extra Mar�timo</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-maritimo-fosco.php"><img title="Verniz Sparlack Mar�timo Fosco" alt="Verniz Sparlack Mar�timo Fosco" src="slices/tintas-coral/img-maritimo-fosco.jpg" /></a>
							<a href="coral/sparlack-maritimo-fosco.php" title="Verniz Sparlack Mar�timo Fosco"><h3>Mar�timo Fosco</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-seladora.php"><img title="Verniz Sparlack Seladora" alt="Verniz Sparlack Seladora" src="slices/tintas-coral/img-seladora.jpg" /></a>
							<a href="coral/sparlack-seladora.php" title="Verniz Sparlack Seladora"><h3>Seladora</h3></a>							
						</li>
						<li>
							<a href="coral/sparlack-tingidor.php"><img title="Sparlack Tingidor" alt="Sparlack Tingidor" src="slices/tintas-coral/img-tingidor.jpg" /></a>
							<a href="coral/sparlack-tingidor.php" title="Sparlack Tingidor"><h3>Tingidor</h3></a>							
						</li>
						<li  class="NoMargin">
							<a href="coral/fundo-preparador-paredes-base-agua.php"><img title="Fundo Preparador de Paredes Base �gua" alt="Fundo Preparador de Paredes Base �gua" src="slices/tintas-coral/img-preparador.jpg" /></a>
							<a href="coral/fundo-preparador-paredes-base-agua.php" title="Fundo Preparador de Paredes Base �gua"><h3>Fundo Preparador</h3></a>							
						</li>
						<li>
							<a href="coral/corante-base-dagua.php"><img title="Corante Base �gua Coral" alt="Corante Base �gua Coral" src="slices/tintas-coral/img-corante-base-agua.jpg" /></a>
							<a href="coral/corante-base-dagua.php" title="Corante Base �gua Coral"><h3>Corante Base �gua Coral</h3></a>							
						</li>
						<li>
							<a href="coral/coralar-acrilico.php"><img title="Coralar Acr�lico" alt="Coralar Acr�lico" src="slices/tintas-coral/img-coralar-acrilico.jpg" /></a>
							<a href="coral/coralar-acrilico.php" title="Coralar Acr�lico"><h3>Coralar Acr�lico</h3></a>							
						</li>
						<li>
							<a href="coral/verniz-acrilico.php"><img title="Verniz Acr�lico" alt="Verniz Acr�lico" src="slices/tintas-coral/img-verniz-acrilico.jpg" /></a>
							<a href="coral/verniz-acrilico.php" title="Verniz Acr�lico"><h3>Verniz Acr�lico</h3></a>							
						</li>
						<li>
							<a href="coral/zarcoral.php"><img title="Zarcoral " alt="Zarcoral" src="slices/tintas-coral/img-zarcoral.jpg" /></a>
							<a href="coral/zarcoral.php" title="Zarcoral"><h3>Zarcoral</h3></a>							
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "componentes/rodape.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>