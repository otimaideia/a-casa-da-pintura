<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Loja Santo Andr� | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas </title>
<? include "componentes/includes.php"; ?>
</head>
<body>
<div id="Pagina">
	<div id="Linha1">
		<div id="ConteudoLinha1">
			<? include "componentes/topo.php"; ?>
		</div>
	</div>	
	<div id="Linha2">
		<div id="ConteudoLinha2">
			<div id="Lojas">
				<h2>Loja de Tintas Santo Andr�</h2>
				<p>Av. Itamarati, 1157 � Pq. Ja�atuba � Santo Andr� � SP</p>
				<p>Telefone: (11) 4436-6666</p>
				<div id="MapaLoja">
					<iframe width="980" height="420" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=A+Casa+da+Pintura+Avenida+Itamarati,+1157+-+Vila+Curu%C3%A7%C3%A1,+Santo+Andr%C3%A9+-+SP,+09290-730+%E2%80%8E&amp;aq=&amp;sll=-23.665909,-46.452062&amp;sspn=0.010652,0.021136&amp;ie=UTF8&amp;hq=A+Casa+da+Pintura+%E2%80%8E&amp;hnear=Avenida+Itamarati,+1157+-+Vila+Curu%C3%A7%C3%A1,+S%C3%A3o+Paulo,+09290-730&amp;t=m&amp;cid=5367046502874751392&amp;ll=-23.631944,-46.509333&amp;spn=0.033026,0.084028&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
				</div>
			</div>
		</div>
	</div>		
	<div id="Linha3">
		<? include "componentes/rodape.php"; ?>
	</div>	
</div>
<div id="mask"></div>
</body>
</html>