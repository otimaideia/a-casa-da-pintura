<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Loja S�o Bernardo do Campo | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas </title>
<? include "componentes/includes.php"; ?>
</head>
<body>
<div id="Pagina">
	<div id="Linha1">
		<div id="ConteudoLinha1">
			<? include "componentes/topo.php"; ?>
		</div>
	</div>	
	<div id="Linha2">
		<div id="ConteudoLinha2">
			<div id="Lojas">
				<h2>Loja de Tintas S�o Bernardo do Campo</h2>
				<p>Av. Pereira Barreto, 646 - Centro - SBC</p>
				<p>Telefone: (11) 4121-9292</p>
				<div id="MapaLoja">
					<iframe width="980" height="420" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=A+Casa+da+Pintura+Avenida+Pereira+Barreto,+646+-+Baeta+Neves,+S%C3%A3o+Bernardo+do+Campo+-+SP,+&amp;aq=&amp;sll=-23.644976,-46.513474&amp;sspn=0.010654,0.021136&amp;ie=UTF8&amp;hq=A+Casa+da+Pintura&amp;hnear=Avenida+Pereira+Barreto,+646+-+Baeta+Neves,+S%C3%A3o+Paulo&amp;t=m&amp;ll=-23.687486,-46.547034&amp;spn=0.008253,0.021007&amp;z=16&amp;iwloc=A&amp;output=embed"></iframe>
				</div>
			</div>
		</div>
	</div>		
	<div id="Linha3">
		<? include "componentes/rodape.php"; ?>
	</div>	
</div>
<div id="mask"></div>
</body>
</html>