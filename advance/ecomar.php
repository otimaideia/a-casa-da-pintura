<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Ecomar | Advance Tintas</title>
	<meta name="Description" content="Ecomar Advance Tintas: Indicado como revestimento est&eacute;tico e protetivo de estruturas met&aacute;licas e equipamentos." />
	<meta name="Keywords" content="Tintas advance tintas ecomar A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>					
						<div id="ImagemProduto">
							<img title="Tinta Advance Ecomar" alt="Tinta Advance Ecomar" src="../slices/tintas-advance/img-ecoclinic-acrilico.jpg" />
						</div>
						<h2>Tinta Advance Ecomar</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Indicado como revestimento est�tico e protetivo de estruturas met�licas e equipamentos.</p>
							<p><b>Locais para Aplica��o:</b> Alvenaria e Tanques de Concreto.</p>
							<span class="Titulo">Mais informa��es sobre a tinta</span>
							<p>Ecomar 912 WBH Esmalte Sint�tico Acabamento Acetinado � uma tinta   especialmente desenvolvida para proteger e embelezar as superf�cies   externas e internas de metais, alvenaria, concreto e madeiras, de   maneira mais f�cil e econ�mica. Possui aspectos Fosco, Semi Brilho e   Acetinado. � um sistema de cura por evapora��o da �gua, por�m de r�pida   secagem. Produto de baixo VOC (Compostos Org�nicos Vol�teis) e de baixo   odor, o que permite ao usu�rio trabalhar em condi��es mais adequadas em   rela��o a sa�de e meio ambiente. Resistente as intemp�ries, mantendo o   brilho e cor por muito mais tempo.    Atende as Normas Hospitalares: ASTM 3273, ABNT 15301, JIS 2801:2006.    Indicado como revestimento est�tico e protetivo de estruturas met�licas,   equipamentos em geral, com primer / acabamento para interiores e   exteriores. Quando necess�rio pode-se usar o Ecoclinic Acr�lico Selador   Pigmentado, visando selar substratos novos como: reboco e alvenaria. No   caso de superf�cies met�licas novas, para obter prote��o anticorrosiva   adicional deve-se tamb�m utilizar o Ecomar 771 Primer Sint�tico.   Normalmente apenas duas dem�os de (20 a 30 micrometros) do Ecomar 912   WBH Esmalte Sint�tico propicia uma boa prote��o � intemp�rie e   anticorrosiva. Apresenta boa dureza, flexibilidade e ades�o.</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>