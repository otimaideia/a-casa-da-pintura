<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Fundo Preparador de Paredes Base &Aacute;gua | A Casa da Pintura</title>
	<meta name="Description" content="Coral Fundo Preparador de Paredes Base &Aacute;gua: O Fundo Preparador Base �gua foi especialmente desenvolvido para preparar paredes novas que receber�o pintura" />
	<meta name="Keywords" content="Tintas Coral Fundo Preparador Parede Base �gua A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Fundo Preparador De Paredes Base �gua" alt="Fundo Preparador De Paredes Base �gua" src="../slices/tintas-coral/img-preparador.jpg" />
						</div>
						<h2>Fundo Preparador De Paredes Base �gua</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>O Fundo Preparador Base �gua foi especialmente desenvolvido para preparar paredes novas que receber�o pintura ou para solucionar os problemas da superf�cie, que pode estar descascada, saponificada ou pintada com cal. O Fundo Preparador Base �gua pode ser aplicado facilmente tanto em ambientes internos quanto externos e garante maior durabilidade � pintura. Al�m disso, com seu uso, a parede adere melhor � tinta de acabamento e forma uma barreira contra a alcalinidade. N�o indicamos a aplica��o direta deste produto em superf�cies de gesso corrido. Neste caso, recomendamos utilizar a tinta Coral Direto no Gesso.</p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes</span>
								<ul>
									<li><b>Embalagem/Rendimento</b></li>
									<li>Lata <b>18 L</b>: 150 a 275 m� por dem�o</li>
									<li>Gal�o <b>3,6  L</b>: 30 a  55 m�  por dem�o</li>
								</ul>
								<ul>
									<li><b>Aplica��o</b></li>
									<li>Rolo de l� ou pincel. Limpe as ferramentas com �gua e sab�o</li>
								</ul>
								<ul>
									<li><b>Dilui��o</b></li>
									<li>Pronto para uso, n�o precisa ser dilu�do.</li>
								</ul>
								<ul>
									<li><b>Acabamento</b></li>
									<li>Fosco</li>
								</ul>
								<ul>
									<li><b>Secagem</b></li>
									<li>Ao toque: 30 min</li>
									<li>Entre dem�os: 4 horas</li>
									<li>Final: 4 hora</li>
								</ul>
							</div> 
							<div id="TintasDisponiveis">
								<span id="TintasDisponiveisTitulo">Tintas Dispon�veis</span>
								<ul>
									<li></li>
									<li>387	FUNDO PREP. BASE DAGUA 18LT - CORAL	18LT</li>
									<li>388	FUNDO PREP. BASE DAGUA 3.6LT - CORAL 3.6LT</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>