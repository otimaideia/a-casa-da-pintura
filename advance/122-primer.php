<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>122 Primer | Advance Tintas</title>
	<meta name="Description" content="122 Primer: Indicada para equipamentos e estruturas em geral em ambientes industriais e orla mar�tima." />
	<meta name="Keywords" content="Tintas advance 122 primer A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="122 Primer" alt="122 Primer" src="../slices/tintas-advance/img-122-primer.jpg" />
						</div>
						<h2>122 Primer</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Indicada para equipamentos e estruturas em geral em ambientes industriais e orla mar�tima.</p>
							<p><b>Locais para Aplica��o:</b>  Metais, Paredes e Pisos</p>
							<p><a target="_blank" title="Dados t�cnicos 122 Primer" href="pdf/122-primer.pdf">Dados T�cnicos (PDF)</a></p>
							<span class="Titulo">Mais informa��es sobre a tinta</span>
							<p>Revestimento anticorrosivo ep�xi de alta espessura de dois ../componentes,   de grande resist�ncia a �lcalis, solu��es salinas, �gua doce, salgada  e abras�o.  Tinta de fundo anticorrosiva indicada para equipamentos e estruturas em   geral em ambientes industriais e orla mar�tima.</p>
						</div>

						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>