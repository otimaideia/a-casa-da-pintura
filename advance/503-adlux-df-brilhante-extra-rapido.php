<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>503 adlux df brilhante extra r&aacute;pido | Advance Tintas</title>
	<meta name="Description" content="530 adlux df brilhante extra r�pido: Tinta DF de secagem extra r�pida. Recomendado para ambientes variados." />
	<meta name="Keywords" content="Tintas advance tintas ecomar A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="503 Adlux Df Brilhante Extra R�pido" alt="503 Adlux Df Brilhante Extra R�pido" src="../slices/tintas-advance/img-530-adlux-df-brilhante.jpg" />
						</div>
						<h2>503 Adlux Df Brilhante Extra R�pido</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Recomendado para ambientes variados.</p>
							<p><b>Locais para Aplica��o:</b>  Metais</p>
							<p><a target="_blank" title="Dados t�cnicos 503 Adlux Df Brilhante Extra R�pido" href="pdf/adlux530.pdf">Dados T�cnicos (PDF)</a></p>
							<span class="Titulo">Mais informa��es sobre a tinta</span>
							<p>Tinta de fundo e acabamento alqu�dico modificada brilhante de secagem r�pida. Monocomponente de excelente prote��o anticorrosiva. Apresenta vantagens com rela��o aos produtos tradicionais por n�o necessitar de primer, o que diminui o custo de estocagem e m�o de obra; � um produto de Dupla Fun��o com excelente resist�ncia anticorrosiva, possuindo em sua f�rmula pigmentos inibidores de corros�o at�xicos. Utilizado como primer e acabamento para interiores e exteriores com excelente ader�ncia e prote��o anticorrosiva sobre superf�cies de a�o carbono fosfatizadas ou desengraxadas. Recomendado para pintura de tratores, implementos agr�colas, motores, estruturas met�licas, maquin�rio e equipamentos em geral. Recomendada aplica��o com espessura de 30 a 40 microns forma uma pel�cula firme, flex�vel, brilhante e resistente ao contato com �leos minerais, graxas e excelente cobertura.</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>