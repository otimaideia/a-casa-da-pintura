<?php include 'lumine/config.php'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=charset=iso-8859-1" />
	<title> Contato | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas</title>
	<? include "componentes/includes.php"; ?>
</head>
<body>
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "componentes/topo.php"; ?>
			</div>
		</div>	
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ContatoObrigado">
					<?php
					echo('<h2>Obrigado por entrar em contato conosco! </h2>');
					echo('<p> Em breve retornaremos.</p>');
					echo('<p class="link"> Ir para <a href="index.php" title="P&aacute;gina inicial A Casa da Pintura">home.</a><img src="slices/icon-home.jpg" title="Ir para a p&aacute;gina inicial A Casa da Pintura" /alt="Ir para a página inicial A Casa da Pintura"> </p>');
					?> 
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "componentes/rodape.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>