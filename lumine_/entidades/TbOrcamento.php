<?php
#### START AUTOCODE
/**
 * Classe generada para a tabela "tb_orcamento"
 * in 2014-01-27
 * @author Hugo Ferreira da Silva
 * @link http://www.hufersil.com.br/lumine
 * @package entidades
 *
 */

class TbOrcamento extends Lumine_Base {

    
    public $id;
    public $nmCliente;
    public $dsEmail;
    public $cdTelefone;
    public $nmEmpresa;
    public $hrContato;
    public $dsMensagenm;
    public $linklProduto;
    
    
    
    /**
     * Inicia os valores da classe
     * @author Hugo Ferreira da Silva
     * @return void
     */
    protected function _initialize()
    {
        $this->metadata()->setTablename('tb_orcamento');
        $this->metadata()->setPackage('entidades');
        
        # nome_do_membro, nome_da_coluna, tipo, comprimento, opcoes
        
        $this->metadata()->addField('id', 'id', 'int', 11, array('primary' => true, 'notnull' => true, 'autoincrement' => true));
        $this->metadata()->addField('nmCliente', 'nm_cliente', 'varchar', 45, array());
        $this->metadata()->addField('dsEmail', 'ds_email', 'varchar', 120, array());
        $this->metadata()->addField('cdTelefone', 'cd_telefone', 'varchar', 45, array());
        $this->metadata()->addField('nmEmpresa', 'nm_empresa', 'varchar', 100, array());
        $this->metadata()->addField('hrContato', 'hr_contato', 'varchar', 45, array());
        $this->metadata()->addField('dsMensagenm', 'ds_mensagenm', 'text', 4294967295, array());
        $this->metadata()->addField('linklProduto', 'linkl_produto', 'varchar', 200, array());

        
    }

    #### END AUTOCODE


}
