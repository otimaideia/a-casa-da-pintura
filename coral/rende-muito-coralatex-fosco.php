<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Coral Rende Muito Coralatex Fosco | A Casa da Pintura</title>
	<meta name="Description" content="Coral Rende Muito Coralatex Fosco: Por ter uma maior consist�ncia, permite uma dilui��o de at� 60% de �gua, superando os produtos convencionais" />
	<meta name="Keywords" content="Tintas Coral rende muito coralatex fosco Gesso A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="http://www.belatintas.com.br/tinta-acrilica-fosca-palha-coralatex-18l-18l.html" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Tinta Rende Muito - Coralatex Fosco" alt="Tinta Rende Muito - Coralatex Fosco" src="../slices/tintas-coral/img-coral-rende-muito.jpg" />
						</div>
						<h2>Rende Muito - Coralatex Fosco</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Pintar com qualidade ficou ainda mais f�cil. Coral Rende Muito � uma tinta de alta consist�ncia que permite uma dilui��o superior aos produtos convencionais. Voc� consegue 80% de dilui��o com �gua, podendo pintar at� 500 m� (lata) por dem�o. Com sua nova f�rmula, possui 30% mais cobertura que a f�rmula anterior. � muito mais rendimento com uma excelente cobertura e o melhor custo benef�cio. Muito � um produto inovador e se apresenta como uma solu��o muito mais sustent�vel que as outras tintas, pois com uma lata de Rende Muito voc� tem quase duas latas de tinta.</p>
						</div>				
						<div id="InformacoesAdicionais">
							<div id="Detalhes">
								<span id="Detalhe">Detalhes</span>
								<b>Embalagem/Rendimento:</b>
								<ul>
									<li>Lata <b>18 Litros cor pronta</b>: at� 500m� por dem�o</li>
									<li>Gal�o <b>3,6 Litros cor pronta</b>: at� 100m� por dem�o</li>
									<li>Quarto <b>0,9 Litros cor pronta</b>: at� 25m� por dem�o</li>
									<li><b>Rendimento por dem�o Sistema Tintom�trico</b></li>
									<li>Lata <b>16 Litros LOC</b>: at� 450m� por dem�o.</li>
									<li>Gal�o <b>3,2 Litros LOC</b>: at� 90m� por dem�o</li>
									<li>Quarto <b>0,8 Litros LOC</b>: at� 22m� por dem�o</li>
									<li><b>Aplica��o</b></li>
									<li>Rolo de l� de p�lo baixo ou pincel de cerdas macias. Limpe as ferramentas com �gua e sab�o</li>
									<li><b>Dilui��o</b></li>
									<li>Superf�cies seladas ou n�o seladas diluir at� 80% com �gua pot�vel para todas as dem�os.</li>
									<li><b>Acabamento:</b> Fosco</li>
									<li><b>Secagem: </b>Ao toque: 30 min, entre dem�os: 4 horas, final: 4 horas</li>
								</ul>
							</div> 
							<div id="TintasDisponiveis">
								<span id="TintasDisponiveisTitulo">Tintas Dispon�veis</span>
								<ul>
									<li>3058 - RENDE MUITO AMAR. CANARIO 18LT (CORALATEX)</li>
									<li>3059 - RENDE MUITO AMAR. CANARIO 3.6LT (CORALATEX)</li>
									<li>673 - RENDE MUITO AMAR. VANILA 18LT (CORALATEX</li>
									<li>672 - RENDE MUITO AMAR. VANILA 3.6LT (CORALATEX)</li>
									<li>676 - RENDE MUITO AREIA 18LT (CORALATEX)</li>
									<li>675 - RENDE MUITO AREIA 3.6LT (CORALATEX)</li>
									<li>2485 - RENDE MUITO AZ PROFUNDO 18LT (CORALATEX)</li>
									<li>678 - RENDE MUITO AZ PROFUNDO 3.6LT (CORALATEX)</li>
									<li>2237 - RENDE MUITO AZUL SERENO 18LT (CORALATEX)</li>
									<li>2236 - RENDE MUITO AZUL SERENO 3.6LT (CORALATEX)</li>
									<li>3897 - RENDE MUITO BCO GELO 0.9LT (CORALATEX)</li>
									<li>681 - RENDE MUITO BCO GELO 18LT (CORALATEX)</li>
									<li>680 - RENDE MUITO BCO GELO 3.6LT (CORALATEX)</li>
									<li>2279 - RENDE MUITO BRANCO 0.9LT (CORALATEX)</li>
									<li>684 - RENDE MUITO BRANCO 18LT (CORALATEX)</li>
									<li>683 - RENDE MUITO BRANCO 3.6LT (CORALATEX)</li>
									<li>5683 - RENDE MUITO CAJU/AMARELO FREVO 18LT</li>
									<li>5684 - RENDE MUITO CAJU/AMARELO FREVO 3.6LT</li>
									<li>687 - RENDE MUITO CAMURCA 18LT (CORALATEX)</li>
									<li>686 - RENDE MUITO CAMURCA 3.6LT (CORALATEX)</li>
									<li>5615 - RENDE MUITO CARANGUEJO/LARANJA MARACATU 18LT</li>
									<li>5682 - RENDE MUITO CARANGUEJO/LARANJA MARACATU 3.6LT</li>
									<li>693 - RENDE MUITO CONCRETO 18LT (CORALATEX)</li>
									<li>692 - RENDE MUITO CONCRETO 3.6LT (CORALATEX)</li>
									<li>694 - RENDE MUITO CROMO SUAVE 18LT (CORALATEX)</li>
									<li>696 - RENDE MUITO CROMO SUAVE 3.6LT (CORALATEX)</li>
									<li>699 - RENDE MUITO FLAMINGO 18LT (CORALATEX)</li>
									<li>698 - RENDE MUITO FLAMINGO 3.6LT (CORALATEX)</li>
									<li>3715 - RENDE MUITO LARANJA CITRICO 18LT (NOVO)</li>
									<li>3716 - RENDE MUITO LARANJA CITRICO 3.6LT (NOVO)</li>
									<li>2239 - RENDE MUITO LARANJA IMPER 18LT (CORALATEX)</li>
									<li>2238 - RENDE MUITO LARANJA IMPER 3.6LT (CORALATEX)</li>
									<li>3717 - RENDE MUITO LILAS 18LT (NOVO)</li>
									<li>3718 - RENDE MUITO LILAS 3.6LT (NOVO)</li>
									<li>702 - RENDE MUITO MARFIM 18LT (CORALATEX)</li>
									<li>701 - RENDE MUITO MARFIM 3.6LT (CORALATEX)</li>
									<li>4277 - RENDE MUITO OCEANO 18LT</li>
									<li>5885 - RENDE MUITO OCEANO 3.6LT</li>
									<li>705 - RENDE MUITO PALHA 18LT (CORALATEX)</li>
									<li>704 - RENDE MUITO PALHA 3.6LT (CORALATEX)</li>
									<li>708 - RENDE MUITO PEROLA 18LT (CORALATEX)</li>
									<li>707 - RENDE MUITO PEROLA 3.6LT (CORALATEX)</li>
									<li>711 - RENDE MUITO PESSEGO 18LT (CORALATEX)</li>
									<li>710 - RENDE MUITO PESSEGO 3.6LT (CORALATEX)</li>
									<li>5689 - RENDE MUITO ROSA ACAI 18LT (CORALATEX)</li>
									<li>3719 - RENDE MUITO ROSA BONECA 18LT (NOVO)</li>
									<li>3720 - RENDE MUITO ROSA BONECA 3.6LT (NOVO)</li>
									<li>5545 - RENDE MUITO TERRACOTA RM</li>
									<li>370 18LT (LOTE UNICO)</li>
									<li>2243 - RENDE MUITO VD PRIMAVERA 18LT (CORALATEX)</li>
									<li>2242 - RENDE MUITO VD PRIMAVERA 3.6LT (CORALATEX)</li>
									<li>2241 - RENDE MUITO VERDE ANGRA 18LT (CORALATEX)</li>
									<li>2240 - RENDE MUITO VERDE ANGRA 3.6LT (CORALATEX)</li>
									<li>3064 - RENDE MUITO VERDE KIWI 18LT (CORALATEX)</li>
									<li>3065 - RENDE MUITO VERDE KIWI 3.6LT (CORALATEX)</li>
									<li>3721 - RENDE MUITO VERDE LIMAO 18LT (NOVO)</li>
									<li>3722 - RENDE MUITO VERDE LIMAO 3.6LT (NOVO)</li>
									<li>2486 - RENDE MUITO VM CARDINAL 18LT (CORALATEX)</li>
									<li>715 - RENDE MUITO VM CARDINAL 3.6LT (CORALATEX)</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>