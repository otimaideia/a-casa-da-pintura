<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Corante Base &Aacute;gua Coral | A Casa da Pintura</title>
	<meta name="Description" content="Coral Corante Base &Aacute;gua: Indicado para tingir tintas l&aacute;tex &agrave; base de &aacute;gua - PVA e Acr&iacute;lica, e  esmaltes a base de &aacute;gua" />
	<meta name="Keywords" content="Tintas Coral Base Corante �gua A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Corante Base �gua Coral" alt="Corante Base �gua Coral" src="../slices/tintas-coral/img-corante-base-agua.jpg" />
						</div>
						<h2>Corante Base �gua Coral</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Indicado para tingir tintas l�tex � base de �gua - PVA e Acr�lica, e esmaltes a base de �gua. A Bisnaga de Cor Coral � dispon�vel em 9 cores, que combinadas possibilitam a obten��o de milhares de cores e tonalidades.</p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>									
								<ul>
									<li><b>Embalagem/Rendimento</b></li>
									<li>Frasco 50 ml: m�ximo um frasco de corante por gal�o (3,6L)</li>
								</ul>
								<ul>
									<li><b>Aplica��o</b></li>
									<li>Agitar o corante antes de usar adicionando-o aos poucos � tinta, sempre  sob agita��o, at� atingir a cor desejada</li>
								</ul>
							</div> 
							<div id="TintasDisponiveis">
								<span id="TintasDisponiveisTitulo">Tintas Dispon�veis:</span>
								<ul>
									<li><b>Tintas Dispon�veis</b></li>
									<li>397	CORANTE AMARELO - CORAL</li>
									<li>398	CORANTE AZUL - CORAL</li>
									<li>399	CORANTE CASTANHO - CORAL</li>
									<li>400	CORANTE LARANJA - CORAL</li>
									<li>401	CORANTE OCRE - CORAL</li>
									<li>402	CORANTE PRETO - CORAL</li>
									<li>403	CORANTE VERDE - CORAL</li>
									<li>404	CORANTE VERMELHO - CORAL</li>
									<li>405	CORANTE VIOLETA - CORAL</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>					
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>