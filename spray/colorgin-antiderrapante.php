<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Colorgin AntiDerrapante | Spray A Casa da Pintura</title>
	<meta name="Description" content="Colorgin Antiderrapante � uma tinta acr�lica em spray, especialmente desenvolvida para aplica��o em superf�cies lisas e escorregadias" />
	<meta name="Keywords" content="tintas spray colorgin" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Colorgin Antiderrapante" alt="Colorgin Antiderrapante" src="../slices/spray-colorgin/img-colorin-antiderrapante.jpg" />
						</div>
						<h2>Colorgin Antiderrapante</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p class="detalhes"><b>Colorgin Antiderrapante </b>� uma tinta acr�lica em spray, especialmente  desenvolvida para aplica��o em superf�cies lisas e escorregadias,  proporcionando seguran�a em �reas internas ou externas. � f�cil de aplicar e  tem secagem r�pida.</p>
							<p class="detalhes"><b>Indicada para:</b> cer�micas, ard�sias, cimentados lisos e madeiras, aplicada em escadas, rampas,  quadras etc. Pode ser usada tamb�m para aplica��o sobre superf�cies pintadas  com NOVACOR PISO e NOVACOR RESINA IMPERMEABILIZANTE (Nestes casos, aguardar 30  dias para cura total do produto). </p>	
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<p class="detalhes"><b>Acabamento:</b> antiderrapante</p>
								<p class="detalhes"><b>Rendimento  Aproximado:</b>de 1,1m� a 1,3m� por embalagem por dem�o.</p>
								<p class="detalhes"><b>Preparo da Superf�cie/  Aplica��o:</b> </p>
								<ul>
									Elimine  a poeira, gordura ou qualquer contaminante;
									<li>Agite  o tubo de COLORGIN ANTIDERRAPANTE durante 2 ou 3 minutos para total  homogeneiza��o do produto;</li>
									<li>Aplique  de 2 a 3  dem�os bem finas do produto, em movimentos constantes e uniformes;</li>
									<li>Aplique  a uma dist�ncia de 25 cm  da superf�cie</li>
									<li>Ao  final do uso, limpe a v�lvula virando a lata para baixo e pressione at� que  saia apenas g�s.<strong></strong></li>
								</ul>
								<p class="detalhes"><b>Secagem:</b></p>
								<ul><li>Entre dem�os: 2   a 4 minutos;</li>
									<li>Ao toque: 30 minutos;</li>
									<li> Total: 24 horas;</li>
									<li>Para testes mec�nicos aguardar 72 horas.</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>