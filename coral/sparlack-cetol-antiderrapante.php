<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Detalhes | A Casa da Pintura</title>
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Produto">
						<div id="Informacoes">
							<a id="FazerPedido" href="http://www.belatintas.com.br/verniz-cetol-deck-antiderrapante-sparlack-galao-galao.html" target="_blank" title="Fazer pedido">Fazer pedido</a>
							<div id="ImagemProduto">
								<img title="Sparlack Cetol" alt="Sparlack Cetol" src="../slices/tintas-coral/img-cetol.jpg" />
							</div>
							<h2>Sparlack Cetol Deck Gl Semi Brilho</h2>
							<div id="InformacoesProduto">
								<span class="Titulo">Descri��o do produto</span>
								<p>Sparlack Cetol Deck Antiderrapante � um revestimento transparente de alt�ssima durabilidade, super resistente aos efeitos do sol, da �gua e ao trafego de pessoas, e ainda conta com a��o antiderrapante. Cetol Deck antiderrapante � indicado para uso externo em decks e varandas.</p>
								<b>Sobre uso e durabilidade</b>
								<p>Impregna na madeira, formando uma pel�cula flex�vel e antiderrapante que n�o trinca e n�o descasca.</p>
								<p>Efetiva prote��o aos efeitos do sol e da �gua com resist�ncia ao trafego de pessoas. Pronto para uso, n�o diluir .</p>
								<b>Alt�ssima durabilidade de 6 anos</b>
								<p>Dispon�vel no acabamento: Semi Brilhante</p>
								<p>Aplica��o: Decks, varandas</p>
							</div>
							<div id="InformacoesAdicionais"> 
								<div id="TintasDisponiveis">
									<span id="TintasDisponiveisTitulo">Tintas Dispon�veis:</span>
									<ul>
										<li>3756 SPARLACK CETOL DECK  ANTI-DERRAP. GL SB 3.6 LT</li>
									</ul>
								</div>							
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>