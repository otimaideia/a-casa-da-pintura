<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Ecoclinic Acr&iacute;lico | Advance Tintas</title>
	<meta name="Description" content="Ecoclinic Acr�lico Advance Tintas: Especialmente recomendado na pintura de paredes e tetos de concreto ou de alvenaria." />
	<meta name="Keywords" content="Tintas advance tintas ecoclinic acrilico A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">		
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>					
						<div id="ImagemProduto">
							<img title="Tinta Advance Ecoclinic Acr�lico" alt="Tinta Advance Ecoclinic Acr�lico" src="../slices/tintas-advance/img-ecoclinic-acrilico.jpg" />
						</div>
						<h2>Tinta Advance Ecoclinic Acr�lico</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Especialmente recomendado na pintura de paredes e tetos de concreto ou de alvenaria.</p>
							<p><b>Locais para Aplica��o:</b> Alvenaria e Tanques de Concreto.</p>
							<span class="Titulo">Mais informa��es sobre a tinta</span>
							<p>Ecoclinic Acr�lico Hospitalar Acetinado � uma tinta acr�lica � base de �gua, especialmente formulada com bactericidas, fungicidas e algicidas at�xicos, que permanecem no filme ap�s a secagem. Possui aspectos Fosco, Semi Brilho e Acetinado. Tem excelente cobertura, oferece um acabamento uniforme e proporciona um aspecto decorativo e protetivo diferenciado. Oferece excelente resist�ncia ao intemperismo e muito boa resist�ncia a lavabilidade. Devido ao correto balanceamento dos aditivos fungicidas e algicidas na f�rmula, impede a contamina��o e desenvolvimento de cepas de fungos, algas e outros microorganismos em ambiente de alta umidade. Atende as Normas Hospitalares: ASTM 3273, ABNT 15301, NBR 11702 tipo 4.2.5, NBR 15079 Premium, JIS 2801:2006. Especialmente recomendado na pintura de paredes e tetos de concreto ou de alvenaria, normalmente em camadas de 50 a 60 micrometros (camada atingida em 2 dem�os). Muito utilizado onde as tintas l�tex convencionais n�o funcionam, principalmente em Hospitais, nas ind�strias de Bebidas, ind�strias Aliment�cias, Adegas de Vinho, Frigor�ficos, Usinas de A�ucar e �lcool, Latic�nios, Abatedouros de aves, hospitais etc. , bem como em regi�es sujeitas a alta umidade como banheiros e vesti�rios, camaras frias, etc. N�o � recomend�vel para servi�os de imers�o ou �reas que fiquem em contato direto com alimentos. PROPRIEDADES VE�CULO. </p>						
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>