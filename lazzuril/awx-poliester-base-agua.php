<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Detalhes | A Casa da Pintura</title>
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Awx � Poliester Base �gua" alt="Awx � Poliester Base �gua" src="../slices/tintas-lazzuril/img-awx-poliester-base-agua.jpg" />
						</div>
						<h2>Awx � Poliester Base �gua</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Indicada para pintura geral, parcial e retoques de ve�culos automotivos nacionais e importados, f�cil de usar e aplicar. Cores dispon�veis em sistema Lazzumix (lisas, met�licas e perolizadas). Possui alto brilho, secagem r�pida, boa resist�ncia � intemp�ries naturais e agentes qu�micos, quando usado com Verniz 8937 e CC940. O uso deste produto � exclusivo a profissionais devidamente treinados.</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>