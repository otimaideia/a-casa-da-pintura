<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Detalhes | A Casa da Pintura</title>
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Produto">
						<div id="Informacoes">
							<a id="FazerPedido" href="http://www.belatintas.com.br/verniz-neutrex-mogno-sparlack-1-4.html" target="_blank" title="Fazer pedido">Fazer pedido</a>
							<div id="ImagemProduto">
								<img title="Veniz Sparlack Neutrex" alt="Veniz Sparlack Neutrex" src="../slices/tintas-coral/img-sparlack-neutrex.jpg" />
							</div>
							<h2>Sparlack Neutrex</h2>
							<div id="InformacoesProduto">
								<span class="Titulo">Descri��o do produto</span>
								<p>Sparlack Neutrex � um verniz tingidor, com acabamento extremamente brilhante, formando uma pel�cula super r�gida e resistente. Tamb�m pode ser utilizado como um impermeabilizante em paredes de alvenaria, reboco e concreto.</p>
								<b>Sobre uso e durabilidade</b>
								<p>Tinge e impermeabiliza.</p>
								<p>Acabamento de alt�ssimo brilho.</p>
								<p>Resistente a umidade e bolor.</p>
								<b>Durabilidade de 3 anos</b>
								<p>Dispon�vel no acabamento: Alto Brilho</p>	
								<p>Aplica��o: Esquadrias, port�es de madeira e paredes de alvenaria.</p>							
							</div>
							<div id="InformacoesAdicionais"> 
								<div id="TintasDisponiveis">
									<span id="TintasDisponiveisTitulo">Tintas Dispon�veis:</span>
									<ul>
										<li>3504 SPARLACK Duplo Filtro Solar ACET. TRANSP. GL 3.6LT</li>
										<li>3040 SPARLACK Duplo Filtro Solar BRILH. MOGNO 1/4 0.9LT</li>
										<li>3039 SPARLACK Duplo Filtro Solar BRILH. MOGNO GL	3.6LT</li>
										<li>3038 SPARLACK Duplo Filtro Solar BRILH. TRANSP. 1/4 0.9LT</li>
										<li>3037 SPARLACK Duplo Filtro Solar BRILH. TRANSP. GL 3.6LT</li>
									</ul>
								</div>							
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>