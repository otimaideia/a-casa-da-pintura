<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>M�scara 3M 8822 PFF2 com v�lvula | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas</title>
	<meta name="Description" content="M�scara 3M com V�lvula: Respirador purificador de ar de seguran�a, tipo pe�a semi-facial filtrante para part�culas" />
	<meta name="Keywords" content="mascara 3m valvula" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="M�scara 3M 8822 PFF2 com v�lvula" alt="M�scara 3M 8822 PFF2 com v�lvula" src="../slices/pinceis-e-rolos/img-mascara3m-valvula.jpg" />
						</div>
						<h2>M�scara 3M 8822 PFF2 com v�lvula</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Respirador purificador de ar de seguran�a, tipo pe�a semi-facial  filtrante para part�culas</p>
							<p> Solda t�rmica em seu per�metro</p>
							<p> O respirador possui o meio filtrante composto  por camadas de microfibras sint�ticas tratadas eletrostaticamente</p>
							<p> A parte superior interna do respirador possui  uma tira de espuma cinza e a parte superior externa, uma tira de material  met�lico mold�vel</p>
							<p> O respirador possui em sua parte central um  dispositivo pl�stico branco, com formato retangular, dotado internamente de uma  v�lvula de exala��o</p>
							<p>Modelo descart�vel</p>
							<p><b>C�digo: CA 5657</b></p>	
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<ul>
									<li><b>Formato:</b> Concha</li>
									<li><b>Tamanho:</b> Regular</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>