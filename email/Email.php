<?php
include 'phpmailer/class.phpmailer.php';

class Email extends PHPMailer
{
	private $_Dados;
	public $Template;
	public $TemplateDir = "enviaForm/";
	
	public function Enviar()
	{
		$this->IsHTML(true);
		if(empty($this->Template))
		{
			throw new Exception("Template n�o pode estar vazio");
		}
		
		//$this->Body = $this->carregaTemplate(); //carregando o template para gerar o corpo do e-mail
		
		//die($this->Body);
		if(!$this->Send())
		{
			throw new Exception("O e-mail n�o p�de ser enviado");
		}
	}
	
	private function carregaTemplate()
	{
		$dados = $this->_Dados;
		//inserindo o template
		ob_start();
		require($this->TemplateDir . $this->Template . ".php");
		$template = ob_get_contents();
		ob_end_clean();

		return $template;
	}
	
	public function anexarArquivos($arquivos) //$_FILES
	{
		for($i=0;$i<count($arquivos['tmp_name']);$i++)
		{
			$this->AddAttachment($arquivos['tmp_name'][$i], $arquivos['name'][$i]);
		}

	}



	
	/**
	*Recebe um array com os dados que ser�o enviados por email
	*/
	public function __construct($dados)
	{
		$this->_Dados = $dados;
	}

}



?>