<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Centro de Distribui��o | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas </title>

<link href="css/reset.css" type="text/css" media="all" rel="stylesheet" />
<link href="css/estrutura.css" type="text/css" media="all" rel="stylesheet" />
<link href="css/tinycarousel.css" type="text/css" media="all" rel="stylesheet" />

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script type="text/javascript" src="js/cycle.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.tinycarousel.min.js"></script>

<!-- PLUGIN PARA CARREGAMENTO DE ESTADO CIDADE -->
<script type="text/javascript" src="http://cidades-estados-js.googlecode.com/files/cidades-estados-1.2-utf8.js"></script>

<!-- Jquery Validação -->
<script type="text/javascript" src="js/jquery-validate/dist/jquery.validate.min.js"></script>
</head>
<body>
<div id="Pagina">
	<div id="Linha1">
		<div id="ConteudoLinha1">
			<? include "componentes/topo.php"; ?>
		</div>
	</div>	
	<div id="Linha2">
		<div id="ConteudoLinha2">
			<div id="Lojas">
				<h2>Centro de Distribui��o</h2>
				<p>Av. Piraporinha, 1111 - Bairro Planalto SBC</p>
				<p>Telefone: (11) 2596-6666</p>
				<div id="MapaLoja">
					<iframe width="980" height="420" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=A+casa+da+Pintura+CENTRO+DE+DISTRIBUI%C3%87%C3%83O++Av.+Piraporinha,+1111+-+Bairro+Planalto+SBC&amp;aq=&amp;sll=-23.690315,-46.567268&amp;sspn=0.01065,0.021136&amp;ie=UTF8&amp;hq=A+casa+da+Pintura+CENTRO+DE+DISTRIBUI%C3%87%C3%83O++Av.+Piraporinha,+1111+-+Bairro+Planalto+SBC&amp;hnear=&amp;t=m&amp;cid=11663078834149639147&amp;ll=-23.67204,-46.569586&amp;spn=0.033016,0.084028&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
				</div>
			</div>
		</div>
	</div>		
	<div id="Linha3">
		<? include "componentes/rodape.php"; ?>
	</div>	
</div>
<div id="mask"></div>
</body>
</html>