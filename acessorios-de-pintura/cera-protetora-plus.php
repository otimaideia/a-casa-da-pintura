<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Cera Protetora Plus 500ml 3M | Acess&oacute;rios de Pintura</title>
	<meta name="Description" content="Cera Protetora Plus 500ml 3M: indicada para pinturas de autom�veis, de aplica��es manuais." />
	<meta name="Keywords" content="cera protetora casa pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Cera Protetora Plus 500ML 3M" alt="Cera Protetora Plus 500ML 3M" src="../slices/pinceis-e-rolos/img-cera-protetora-plus.jpg" />
						</div>
						<h2>Cera Protetora Plus 500ML 3M</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p><b>Cera Protetora Plus 500ml  da 3M </b>� uma cera l�quida  indicada para pinturas de autom�veis, de aplica��es manuais. Indicada para  prote��o e conserva��o de todos os tipos de pinturas automotivas. Proporciona  brilho intenso. � de f�cil aplica��o.</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>