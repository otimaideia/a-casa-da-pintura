<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Casa da Pintura</title>
	<? include "componentes/includes.php"; ?>
</head>
<body id="PaginaContato">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "componentes/topo.php"; ?>
			</div>
		</div>	
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoTrabalheConosco">
					<div id="TrabalheConoscoColuna1">
						<h2>Trabalhe Conosco</h2>
						<form id="FormularioTrabalheConosco"action="trabalhe-conosco-obrigado.php" method="post" enctype="multipart/form-data">
							<ol>
								<li>
									<label>Nome:</label>
									<input type="text" id="nome" class="Large required" name="nome" />	
								</li>
								<li>
									<label>E-mail:</label>
									<input type="text" id="email" class="Large required" name="email">
								</li>
								<li>
									<label>Telefone:</label>
									<input type="text" id="telefone" class="Large required" name="telefone">
								</li>
								<li>
									<label for="curriculum">Curriculum:</label>
									<input type="file" id="curriculo" name="curriculo" class="textfield large">
									<label class="curriculum2">Tipo de arquivos permitidos: .doc, .docx, .pdf, .txt</label>
								</li>
								<li id="Enviar">
									<input type="submit" name="enviar" value="Enviar" id="Enviar" />
								</li>
							</ol>
						</form>
					</div>
					<div id="TrabalheConoscoColuna2">
						<h2>Localiza&ccedil;&atilde;o</h2>
						<iframe width="499" height="337" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=A+casa+da+Pintura+CENTRO+DE+DISTRIBUI%C3%87%C3%83O++Av.+Piraporinha,+1111+-+Bairro+Planalto+SBC&amp;aq=&amp;sll=-23.690315,-46.567268&amp;sspn=0.01065,0.021136&amp;ie=UTF8&amp;hq=A+casa+da+Pintura+CENTRO+DE+DISTRIBUI%C3%87%C3%83O++Av.+Piraporinha,+1111+-+Bairro+Planalto+SBC&amp;hnear=&amp;t=m&amp;cid=11663078834149639147&amp;ll=-23.675813,-46.569586&amp;spn=0.02649,0.042915&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
					</div>				
				</div>
			</div>
		</div>		
		<div id="Linha3">
			<? include "componentes/rodape.php"; ?>
		</div>	
	</div>
	<div id="mask"></div>
</body>
</html>