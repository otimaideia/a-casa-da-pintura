<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Detalhes | A Casa da Pintura</title>
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Produto">
						<div id="Informacoes">
							<a id="FazerPedido" href="http://www.belatintas.com.br/verniz-brilhante-sparlack-maritimo-galao-galao.html" target="_blank" title="Fazer pedido">Fazer pedido</a>
							<div id="ImagemProduto">
								<img title="Veniz Sparlack Extra Mar�timo" alt="Veniz Sparlack Extra Mar�timo" src="../slices/tintas-coral/img-extra-maritimo.jpg" />
							</div>
							<h2>Sparlack Extra Mar�timo</h2>
							<div id="InformacoesProduto">
								<span class="Titulo">Descri��o do produto</span>
								<p>Sparlack Extra Mar�timo � um verniz transparente de acabamento brilhante ou acetinado, possui filtro solar e boa resist�ncia as intemp�ries.</p>
								<b>Sobre uso e durabilidade</b>
								<p>N�o altera a cor natural da madeira.</p>
								<p>Possui filtro solar.</p>
								<p>Boa resist�ncia a intemp�ries</p>
								<b>Durabilidade de 2 anos</b>
								<p>Dispon�vel no acabamento: Brilhante e acetinado</p>	
								<p>Aplica��o: Portas, Forros e esquadrias de madeira</p>							
							</div>
							<div id="InformacoesAdicionais"> 
								<div id="TintasDisponiveis">
									<span id="TintasDisponiveisTitulo">Tintas Dispon�veis:</span>
									<ul>
										<li>3225 SPARLACK EXTRA MARITIMO INCOLOR 1/4 0.9LT</li>
										<li>3226 SPARLACK EXTRA MARITIMO INCOLOR GL	3.6LT</li>
									</ul>
								</div>							
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>	
					<? include "../componentes/outros-produtos.php"; ?>	
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>