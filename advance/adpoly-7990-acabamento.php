<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>AdPoly 7990 Acabamento | Advance Tintas</title>
	<meta name="Description" content="AdPoly 7990 Acabamento: Revestimento bicomponente poliuretano acr�lico proporciona acabamento brilhante." />
	<meta name="Keywords" content="Tintas advance 772 df sem brilho A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="AdPoly 7990 Acabamento" alt="AdPoly 7990 Acabamento" src="../slices/tintas-advance/img-adpoly-7990.jpg" />
						</div>
						<h2>AdPoly 7990 Acabamento</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Revestimento bicomponente poliuretano acr�lico proporciona acabamento brilhante...</p>
							<p><b>Locais para Aplica��o:</b>  Metais</p>
							<p><a target="_blank" title="Dados t�cnicos AdPoly 7990 Acabamento" href="/2014/pdf/adpoly-7990.pdf">Dados T�cnicos (PDF)</a></p>
							<span class="Titulo">Mais informa��es sobre a tinta</span>
							<p>Revestimento bicomponente poliuretano acr�lico a base de isocianato alif�tico, proporcionando um acabamento brilhante com excelente flexibilidade e dureza. O acabamento final apresenta um filme liso, com resist�ncia ao intemper�smo e alto brilho. Aplicado em espessuras de 40 micrometros apresenta �tima resist�ncia a vapores e respingos �cidos. Certificado/ Aprova��o Petrobras- N- 1342- Esmalte Poliuretano de dois ../componentes. ( Atende aos requisitos qualitativos). Acabamento de sistemas de alto desempenho em revestimento anticorrosivo para equipamentos e estruturas met�licas em geral, onde se necessita de resist�ncia qu�mica e reten��o de brilho. Estas propriedades tornam este produto um excelente revestimento para ind�strias qu�micas, petroqu�micas, papel e celulose, na ind�stria el�trica e outras de processamento qu�mico. Devido ao seu alto brilho, secagem r�pida e resist�ncia a intemp�rie natural � indicado para pintura geral, parcial e retoques de ve�culos automotivos, transporte e outros.</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>