<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Detalhes | A Casa da Pintura</title>
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Produto">
						<div id="Informacoes">
							<a id="FazerPedido" href="http://www.belatintas.com.br/verniz-brilhante-sparlack-maritimo-galao-galao.html" target="_blank" title="Fazer pedido">Fazer pedido</a>
							<div id="ImagemProduto">
								<img title="Veniz Sparlack Mar�timo Fosco" alt="Veniz Sparlack Mar�timo Fosco" src="../slices/tintas-coral/img-maritimo-fosco.jpg" />
							</div>
							<h2>Sparlack Mar�timo Fosco</h2>
							<div id="InformacoesProduto">
								<span class="Titulo">Descri��o do produto</span>
								<p>Sparlack Mar�timo Fosco � um verniz transparente com acabamento encerado, n�o altera a cor da madeira. Proporciona prote��o �s intemp�ries pois possui filtro solar.</p>
								<b>Sobre uso e durabilidade</b>
								<p>Aspecto encerado.</p>
								<p>N�o altera a cor natural da madeira.</p>
								<p>Possui filtro solar.</p>
								<p>Boa resist�ncia a intemp�ries</p>
								<b>Durabilidade de 2 anos</b>
								<p>Dispon�vel no acabamento: Fosco</p>	
								<p>Aplica��o: Portas, Forros, m�veis e esquadrias de madeira</p>							
							</div>
							<div id="InformacoesAdicionais"> 
								<div id="TintasDisponiveis">
									<span id="TintasDisponiveisTitulo">Tintas Dispon�veis:</span>
									<ul>
										<li>3225 SPARLACK EXTRA MARITIMO INCOLOR 1/4 0.9LT</li>
										<li>3226 SPARLACK EXTRA MARITIMO INCOLOR GL	3.6LT</li>
									</ul>
								</div>							
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>