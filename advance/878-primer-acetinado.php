<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>878 Primer Acetinado | Advance Tintas</title>
	<meta name="Description" content="878 Primer Acetinado Advance Tintas: Tinta Anticorrosiva, indicada para equipamentos industriais." />
	<meta name="Keywords" content="Tintas advance tintas primer acetinado A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">	
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>				
						<div id="ImagemProduto">
							<img title="878 Primer Acetinado" alt="878 Primer Acetinado" src="../slices/tintas-advance/img-870-primer-fosco.jpg" />
						</div>
						<h2>878 Primer Acetinado</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Tinta Anticorrosiva, indicada para equipamentos industriais.</p>
							<p><b>Locais para Aplica��o:</b>  Metais</p>
							<span class="Titulo">Mais informa��es sobre a tinta</span>
							<p>Revestimento anticorrosivo ep�xi de alto s�lidos de dois ../componentes. Apresenta vantagens com rela��o aos produtos tradicionais por permitir aplica��o em alta espessura com baixo VOC e alto rendimento por m�. Produto de grande resist�ncia a �lcalis, solu��es salinas, �gua doce, salgada e abras�o. Tinta de fundo anticorrosiva, para equipamentos industriais, estruturas met�licas em geral, tubula��es, silos, exterior de tanques, para ambiente industrial e orla mar�tima. Usinas de A��car e �lcool, Sider�rgicas, F�bricas de Papel e Celulose, Industrias Qu�micas e Petroqu�micas, Calderaria e outras ind�strias.</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>	
					<? include "../componentes/solicitar-orcamento.php"; ?>				
					<? include "../componentes/outros-produtos.php"; ?>				
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>