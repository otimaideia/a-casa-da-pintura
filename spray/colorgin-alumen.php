<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Colorgin Alumen | Spray A Casa da Pintura</title>
	<meta name="Description" content="Colorgin Alumen � uma tinta acr�lica em spray, de secagem r�pida, com excelente poder de cobertura e alta resist�ncia a intemp�ries" />
	<meta name="Keywords" content="tintas spray colorgin alumen" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Colorgin Alumen" alt="Colorgin Alumen" src="../slices/spray-colorgin/img-colorgin-alumen.jpg" />
						</div>
						<h2>Colorgin Alumen</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p><b>Colorgin Alumen</b> � uma tinta acr�lica em spray, de  secagem r�pida, com excelente poder de cobertura e alta resist�ncia a  intemp�ries. Especialmente formulada para pintura de alum�nio anodizado,  alum�nio comum (sem tratamento) e n�o polido. Pode ser utilizada em �reas  internas e externas.</p>
							<p><b>Indicada para: </b>portas, grades, port�es, janelas,  esquadrias, outros objetos em alum�nio anodizado e outros metais.<strong></strong></p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<p><b>Acabamento: met�lico,  brilhante e fosco.</b></p>
								<p><b>Rendimento  Aproximado: </b>de 1,0m� a 1,4m� por embalagem por  dem�o.</p>
								<p><b>Preparo da Superf�cie/  Aplica��o:</b></p>
								<ul>
									<li>Lixe  a superf�cie.</li>
									<li>Elimine  a poeira, gordura ou qualquer contaminante.</li>
									<li>Aguarde  a secagem e aplique de 2 a  3 dem�os bem finas do produto, em movimentos constantes e uniformes, no sentido  horizontal e vertical.</li>
									<li>Aplique  a uma dist�ncia de 25 cm  da superf�cie.</li>
									<li>Aguarde  de 2 a 4  minutos entre dem�os. </li>
									<li>Ao  final do uso, limpe a v�lvula virando a lata para baixo e pressione at� que  saia apenas g�s.</li>
								</ul>
								<p> <b>Secagem:</b></p>
								<p>Entre dem�os: 2   a 4 minutos;</p>
								<p>Ao toque: 30 minutos;</p>
								<p>Total: 24 horas;</p>
								<p>*Para testes mec�nicos recomenda-se aguardar 72 horas.</p>
								<ul>
									<li><b>Importante:</b></li>

									<li>Quando  se tratar de retoque em alum�nio j� colorido, a cor ir� apresentar varia��o  devido incid�ncia de luz;</li>
									<li>A  cor branca deste produto pode apresentar um leve amarelamento com o tempo de  uso, devido �s intemp�ries;</li>
									<li>N�o  aplicar sobre isopor, superf�cies de pl�stico ou acr�lico.</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/solicitar-orcamento.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>