<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Esmalte Alto Brilho Coralit | A Casa da Pintura</title>
	<meta name="Description" content="Coral Esmalte Alto Briho Coralit: Coralit � o esmalte sint�tico de alta qualidade, durabilidade e resist�ncia" />
	<meta name="Keywords" content="Tintas Coral coralit esmalte alto brilho Gesso A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />	
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="http://www.belatintas.com.br/esmalte-brilhante-tabaco-coralit-1-16-1-17.html" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Esmalte Autobrilho Coralit" alt="Esmalte Altobrilho Coralit" src="../slices/tintas-coral/img-esmalte-altobrilho-coralit.jpg" />
						</div>
						<h2>Esmalte Altobrilho - Coralit</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Coralit � o esmalte sint�tico de alta qualidade, durabilidade e resist�ncia. Sua f�rmula cria uma pel�cula com prote��o prolongada, que conserva o brilho e a apar�ncia de novo por muito mais tempo. F�cil de aplicar, possui excelente poder de cobertura, rendimento e qualidade no acabamento. Ideal para superf�cies externas e internas de madeiras, metais ferrosos, galvanizados, alum�nio, cer�mica n�o vitrificada e alvenaria.</p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<ul>
									<li><b>Embalagem/Rendimento:</b> Gal�o 3,6 L : 40 a 50 m� por dem�o</li>
									<li><b>Aplica��o:</b> �reas grandes: rolo de espuma ou rev�lver. �reas pequenas: pincel de cerdas macias. Limpe as ferramentas com Coralraz.</li>
									<li><b>Dilui��o: </b> Usar diluente Coralraz. Aplica��o com pincel/rolo, diluir no m�ximo 10%. Com revolver, diluir no m�ximo 30%</li>
									<li><b>Acabamento:</b> Alto Brilho</li>
									<li><b>Secagem: </b> Ao toque: 1 a 3h, entre dem�os: 8 horas, final: 18 horas</li>
								</ul>
							</div>
							<div id="TintasDisponiveis">
								<span id="TintasDisponiveisTitulo">Tintas Dispon�veis</span>
								<ul>
									<li>70 - CORALIT AB ALUMINIO 0.9LT</li>
									<li>472 - CORALIT AB ALUMINIO 112ML</li>
									<li>471 - CORALIT AB ALUMINIO 225ML</li>
									<li>469 - CORALIT AB ALUMINIO GL</li>
									<li>478 - CORALIT AB AMARELO 0.9LT</li>
									<li>479 - CORALIT AB AMARELO 225ML</li>
									<li>477 - CORALIT AB AMARELO GL</li>
									<li>474 - CORALIT AB AMARELO TRATOR 0.9LT</li>
									<li>473 - CORALIT AB AMARELO TRATOR GL 3.6LT</li>
									<li>482 - CORALIT AB AREIA 0.9LT</li>
									<li>481 - CORALIT AB AREIA GL</li>
									<li>5565 - CORALIT AB AREIA RM 820 BASE P (LOTE UNICO)</li>
									<li>490 - CORALIT AB AZ DEL REY 0.9LT</li>
									<li>491 - CORALIT AB AZ DEL REY 225ML</li>
									<li>489 - CORALIT AB AZ DEL REY GL</li>
									<li>486 - CORALIT AB AZ FRANCA 0.9LT</li>
									<li>487 - CORALIT AB AZ FRANCA 225ML</li>
									<li>485 - CORALIT AB AZ FRANCA GL</li>
									<li>494 - CORALIT AB AZ MAR 0.9LT</li>
									<li>493 - CORALIT AB AZ MAR GL</li>
									<li>498 - CORALIT AB BRANCO 0.9LT</li>
									<li>499 - CORALIT AB BRANCO 225ML</li>
									<li>501 - CORALIT AB BRANCO GELO</li>
									<li>502 - CORALIT AB BRANCO GELO 0.9LT</li>
									<li>504 - CORALIT AB BRANCO GELO 112ML</li>
									<li>497 - CORALIT AB BRANCO GL</li>
									<li>506 - CORALIT AB CAMURCA 0.9LT</li>
									<li>505 - CORALIT AB CAMURCA GL</li>
									<li>510 - CORALIT AB COLORADO 0.9LT</li>
									<li>509 - CORALIT AB COLORADO GL</li>
									<li>514 - CORALIT AB CREME 0.9LT</li>
									<li>513 - CORALIT AB CREME GL</li>
									<li>518 - CORALIT AB CZ ESCURO 0.9LT</li>
									<li>517 - CORALIT AB CZ ESCURO GL</li>
									<li>522 - CORALIT AB CZ MEDIO 0.9LT</li>
									<li>521 - CORALIT AB CZ MEDIO GL</li>
									<li>526 - CORALIT AB LARANJA 0.9LT</li>
									<li>525 - CORALIT AB LARANJA GL</li>
									<li>530 - CORALIT AB MARFIM 0.9LT</li>
									<li>529 - CORALIT AB MARFIM GL</li>
									<li>534 - CORALIT AB MARROM 0.9LT</li>
									<li>533 - CORALIT AB MARROM GL</li>
									<li>538 - CORALIT AB MR CONHAQUE 0.9LT</li>
									<li>537 - CORALIT AB MR CONHAQUE GL</li>
									<li>542 - CORALIT AB PLATINA 0.9LT</li>
									<li>543 - CORALIT AB PLATINA 225ML</li>
									<li>541 - CORALIT AB PLATINA GL</li>
									<li>546 - CORALIT AB PRETO 0.9LT</li>
									<li>547 - CORALIT AB PRETO 225ML</li>
									<li>545 - CORALIT AB PRETO GL</li>
									<li>550 - CORALIT AB TABACO 0.9LT</li>
									<li>549 - CORALIT AB TABACO GL</li>
									<li>554 - CORALIT AB VD COLONIAL 0.9LT</li>
									<li>553 - CORALIT AB VD COLONIAL GL</li>
									<li>558 - CORALIT AB VD FOLHA 0.9LT</li>
									<li>559 - CORALIT AB VD FOLHA 225ML</li>
									<li>557 - CORALIT AB VD FOLHA GL</li>
									<li>562 - CORALIT AB VD NILO 0.9LT</li>
									<li>561 - CORALIT AB VD NILO GL</li>
									<li>566 - CORALIT AB VERMELHO 0.9LT</li>
									<li>567 - CORALIT AB VERMELHO 225ML</li>
									<li>565 - CORALIT AB VERMELHO GL</li>
									<li>570 - CORALIT AB VM GOYA 0.9LT</li>
									<li>569 - CORALIT AB VM GOYA GL</li>
									<li>5567 - CORALIT AC PRECIOSO MOMENTO RM 5389 BASE M (LOTE UNICO) GL</li>
									<li>5697 - CORALIT ZERO B'AGUA AB AMARELO GL</li>
									<li>5515 - CORALIT ZERO B'AGUA AB AZUL DEL REY GL</li>
									<li>2981 - CORALIT ZERO B'AGUA AB BRANCO 0.9LT</li>
									<li>2187 - CORALIT ZERO B'AGUA AB BRANCO GL</li>
									<li>5699 - CORALIT ZERO B'AGUA AB CINZA CLARO (PLATINA) GL</li>
									<li>5698 - CORALIT ZERO B'AGUA AB MARROM (TABACO) GL</li>
									<li>5700 - CORALIT ZERO B'AGUA AB PRETO GL</li>
									<li>5516 - CORALIT ZERO B'AGUA AB VERDE (FOLHA) GL</li>
									<li>5696 - CORALIT ZERO B'AGUA AB VERMELHO GL</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>					
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>