<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>870 Primer Fosco | Advance Tintas</title>
	<meta name="Description" content="870 Primer Fosco Advance Tintas: Indicado como revestimento anticorrosivo para ambientes industriais e orla mar�tima." />
	<meta name="Keywords" content="Tintas advance tintas ecopoxi A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="870 Primer Fosco" alt="870 Primer Fosco" src="../slices/tintas-advance/img-870-primer-fosco.jpg" />
						</div>
						<h2>870 Primer Fosco</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>

							<p>Indicado como revestimento anticorrosivo para ambientes industriais e orla mar�tima.</p>
							<p><b>Locais para Aplica��o:</b>  Metais</p>
							<p><a target="_blank" title="Dados t�cnicos 870 Primer Fosco" href="pdf/870-primer-fosco.pdf">Dados T�cnicos (PDF)</a></p>
							<span class="Titulo">Mais informa��es sobre a tinta</span>

							<p>Primer ep�xi poliamida �xido de ferro de dois ../componentes econ�mico,   anticorrosivo para sistema ep�xi e poliuretano. Indicado como revestimento anticorrosivo para ambientes industriais e   orla mar�tima, para equipamentos e estruturas met�licas em geral.</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>