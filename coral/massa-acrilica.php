<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Massa Acr�lica Coral | A Casa da Pintura</title>
	<meta name="Description" content="Coral Massa Acrilica: A Massa Acr�lica Coral tem alto poder de preenchimento e �tima ader�ncia" />
	<meta name="Keywords" content="massa acrilica acr�lica coral casa da pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="http://www.belatintas.com.br/massa-acrilica-coral-1-4-1-4.html" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Massa Acr�lica Coral" alt="Massa Acr�lica Coral" src="../slices/tintas-coral/img-massa-acrilica-coral.jpg" />
						</div>
						<h2>Massa Acr�lica</h2>
						<div id="InformacoesProduto">
							<p>A Massa Acr�lica Coral tem alto poder de preenchimento, �tima ader�ncia, � f�cil de lixar e aplicar, al�m de possuir secagem r�pida, elevada consist�ncia, excelente resist�ncia � alcalinidade e � intemp�rie. Indicado para uniformizar, nivelar e corrigir pequenas imperfei��es em superf�cies externas e internas de alvenaria e concreto.</p>
						</div>					
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes</span>
								<ul>
									<li><b>Embalagem/Rendimento:</b> Gal�o 3,6 L : 40 a 50 m� por dem�o</li>
									<li><b>Massa Grossa</b></li>
									<li>Lata <b>18 L</b>: 25 a 30 m� por dem�o</li>
									<li>Gal�o <b>3,6 L</b>: 5 a 6 m� por dem�o</li>
									<li>Quarto <b>0,9 L</b>: 1,25 a 1,50 m� por dem�o</li>
									<li><b>Massa Fina</b></li>
									<li>Lata <b>18 L</b>: 40 a 60 m� por dem�o</li>
									<li>Gal�o <b>3,6 L</b>: 8 a 12 m� por dem�o</li>
									<li>Quarto <b>0,9 L</b>: 2 a 3 m� por dem�o</li>
									<li><b>Aplica��o:</b> �reas grandes: rolo de espuma ou rev�lver. �reas pequenas: pincel de cerdas macias. Limpe as ferramentas com Coralraz.</li>
									<li><b>Dilui��o: </b> Usar diluente Coralraz. Aplica��o com pincel/rolo, diluir no m�ximo 10%. Com revolver, diluir no m�ximo 30%</li>
									<li><b>Acabamento:</b> Alto Brilho</li>
									<li><b>Secagem: </b> Ao toque: 1 a 3h, entre dem�os: 8 horas, final: 18 horas</li>
								</ul>
								<ul>
									<li><b>Aplica��o</b></li>
									<li>Esp�tula ou desempenadeira de a�o. Limpe as ferramentas com �gua e sab�o.</li>
								</ul>
								<ul>
									<li><b>Dilui��o</b></li>
									<li>Pronta para uso, n�o precisa diluir.</li>
								</ul>
								<ul>
									<li><b>Acabamento</b></li>
									<li>Fosco</li>
								</ul>
								<ul>
									<li><b>Secagem</b></li>
									<li>Ao toque: 30 min</li>
									<li>Entre dem�os: 4 horas</li>
									<li>Final: 5 horas</li>
								</ul>
							</div>
							<div id="TintasDisponiveis">
								<span id="TintasDisponiveisTitulo">Tintas Dispon�veis</span>
								<ul>
									<li>Massa Acrilica 0.9LT</li>
									<li>Massa Acrilica 1.8LT</li>
									<li>Massa Acrilica 3.6LT</li>								
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>