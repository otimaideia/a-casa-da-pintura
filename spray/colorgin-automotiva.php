<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Colorgin Automotiva  | Spray A Casa da Pintura</title>
	<meta name="Description" content="Colorgin Automotiva � uma tinta em spray de alta qualidade indicada para retoques de autos e pintura de motos" />
	<meta name="Keywords" content="tintas spray colorgin" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Colorgin Automotiva" alt="Colorgin Automotiva" src="../slices/spray-colorgin/img-colorgin-automotiva.jpg" />
						</div>
						<h2>Colorgin Automotiva</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p><b>Colorgin Automotiva </b>� uma tinta em spray de alta  qualidade indicada para retoques de autos e pintura de motos. Possui diversas  cores das mais variadas montadoras. Para correta identifica��o da cor de seu  carro, consulte o cat�logo de cores.</p>	
							<p><b>Indicada para</b>: retoques de autos e pinturas de  motos</p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<p>Acabamento: brilhante.</p>							
								<p><b>Rendimento Aproximado: </b>de 1,3m� a 1,7m� por embalagem por dem�o.</p>
								<p><b>Rendimento Aproximado: </b>de 1,3m� a 1,7m� por embalagem por dem�o.</p>
								<p><b>Preparo da superf�cie/  Aplica��o:</b> </p>
								<ul>
									<li>Elimine  a poeira, gordura ou qualquer contaminante;</li>
									<li>Aplique  massa r�pida de boa qualidade e espere secar, a fim de melhorar o nivelamento  da superf�cie;</li>
									<li>Lixe  com lixa d��gua 320, seque bem e aplique Colorgin Primer R�pido;</li>
									<li>Ap�s  a secagem, lixe com lixa d��gua 400 at� a superf�cie ficar bem homog�nea e  lisa;</li>
									<li>Aplique  as dem�os necess�rias de Colorgin Automotiva e espere secar;</li>
									<li>Ao  final do uso, limpe a v�lvula virando a lata para baixo e pressione at� que  saia apenas g�s.</li>
								</ul>
								<ul>
									<li><b>Secagem:</b></li>
									<li>Entre dem�os: 5   a 10 minutos;</li>
									<li>Ao toque: 30 minutos;</li>
									<li>Total: 24 horas;</li>
									<li>Para testes mec�nicos: ap�s 72 horas.</li>
								</ul>

							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>