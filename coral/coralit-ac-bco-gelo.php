<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Coralit Acetinado Branco Gelo | A Casa da Pintura</title>
	<meta name="Description" content="Coral Acetinado Branco Gelo Coralit: Usado geralmente em madeiras e metais, o esmalte acetinado cria uma pel�cula protetora que permite f�cil limpeza" />
	<meta name="Keywords" content="Tintas Coral coralit acetinado branco gelo Gesso A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="http://www.belatintas.com.br/esmalte-brilhante-tabaco-coralit-1-16-1-17.html" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Coralit Acetinado Branco Gelo" alt="Coralit Acetinado Branco Gelo" src="../slices/tintas-coral/img-coralit-ac-bco-gelo.jpg" />
						</div>
						<h2>Coralit Acetinado Branco Gelo</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Usado geralmente em madeiras e metais, o esmalte acetinado cria uma pel�cula protetora que permite f�cil limpeza. O esmalte acetinado tem um leve brilho e vai deixar as portas e janelas do seu im�vel do jeito que voc� gosta. Use aguarr�s como solvente...</p>
						</div>
						<div id="InformacoesAdicionais">
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>		
								<ul>
									<li><b>Org�o Normatiza��o:</b> ABNT</li>
									<li><b>Produto Qu�mico:</b> Sim</li>
									<li><b>Produto Qu�mico Utiliza��o: </b> Decora��o, pintura em alvenaria, madeiras e metais</li>
									<li><b>Tipo Unidade:</b> Unidade</li>
									<li><b>Peso Produto: </b> 4,566 kg</li>
									<li><b>Altura Produto: </b> 0,19 m</li>
									<li><b>Largura Produto: </b> 0,345 m</li>
									<li><b>Comprimento Produto:</b> 0,34 m</li>
									<li><b>Possui Garantia?</b> Sim</li>
								</ul>
							</div> 
							<div id="TintasDisponiveis">
								<span id="TintasDisponiveisTitulo">Tintas Dispon�veis:</span>
								<ul>
									<li>581 - CORALIT AC BCO GELO 0.9LT</li>
									<li>580 - CORALIT AC BCO GELO GL</li>
									<li>579 - CORALIT AC BRANCO 0.9LT</li>
									<li>578 - CORALIT AC BRANCO GL</li>
									<li>2982 - CORALIT ZERO B'AGUA AC BRANCO 0.9LT</li>
									<li>573 - CORALIT ZERO B'AGUA AC BRANCO GL</li>
									<li>3217 - CORALIT ZERO B'AGUA AC GELO 0.9LT</li>
									<li>3218 - CORALIT ZERO B'AGUA AC GELO GL</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>