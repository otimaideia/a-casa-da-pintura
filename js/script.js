/* INICIO CARREGAMENTO DE ESTADO/CIDADE */
window.onDomReady(function() {	
	new dgCidadesEstados({
		estado: document.getElementById('estado'),
		cidade: document.getElementById('cidade')
	});  
});

$('#estado').change(function() {
	$("#estado-escolhido1").val($(this).val());
});

$('#cidade').change(function() {
	$("#cidade-escolhida1").val($(this).val());
});

/* FINAL CARREGAMENTO DE ESTADO/CIDADE */

$(document).ready(function(){
	$("#telefone").mask("(99) 9999-9999");

	$('#SliderProdutos').tinycarousel({ display: 2 }); 

	$('a[name=NossasLojas]').click(
		function(e) {e.preventDefault(); 
			var id = $(this).attr('href');
			var maskHeight = $(document).height();
			var maskWidth = $(window).width(); 

			$('#mask').css({'width':maskWidth,'height':maskHeight}); 
			$('#mask').fadeIn(1000);
			$('#mask').fadeTo("slow",0.8); 
			var winH = $(window).height();
			var winW = $(window).width(); 
			$(id).css('top',  winH/2-$(id).height()/2);
			$(id).css('left', winW/2-$(id).width()/2);
			$(id).fadeIn(2000); });
	$('.window .close').click(function (e) {e.preventDefault();
		$('#mask').hide();
		$('.window').hide(); }); 
	$('#mask').click(function () {
		$(this).hide(); 
		$('.window').hide(); }); 

		//Pop-up Telefones

		$('a[name=Telefone]').click(function(e) {e.preventDefault(); 
			var id = $(this).attr('href'); 
			var maskHeight = $(document).height(); 
			var maskWidth = $(window).width(); 
			$('#mask').css({'width':maskWidth,'height':maskHeight}); 
			$('#mask').fadeIn(1000); $('#mask').fadeTo("slow",0.8); 
			var winH = $(window).height();
			var winW = $(window).width(); 
			$(id).css('top',  winH/2-$(id).height()/2);
			$(id).css('left', winW/2-$(id).width()/2);
			$(id).fadeIn(2000); }); 
		$('.Telefone .close').click(function (e) {e.preventDefault();
			$('#mask').hide(); 
			$('.Telefone').hide(); });
		$('#mask').click(function () {
			$(this).hide();
			$('.Telefone').hide(); 
		}); 

		// Slider página inicial 	
		$('#Slider #Images ul').cycle({ fx: 'fade' });

		//Toggle Pop-up Telefone

		$("#telefoneresidencial").hide();
		$("#telefoneindustrial").hide();
		$("#telefoneautomotiva").hide();
		$("#residencial").click(  function(){
			$("#telefoneresidencial").toggle("slow");
		});
		$("#industrial").click(  function(){
			$("#telefoneindustrial").toggle("slow");
		});
		$("#automotiva").click(  function(){
			$("#telefoneautomotiva").toggle("slow");
		});

		//Toggle nossas lojas
		$("#TelefoneDiadema").hide();
		$("#LojaDiadema").click(  function(){
			$("#TelefoneDiadema").toggle("slow");
		});

		$("#TelefoneSantoAndre").hide();
		$("#LojaSantoAndre").click(  function(){
			$("#TelefoneSantoAndre").toggle("slow");
		});

		$("#TelefoneSaoCaetano").hide();
		$("#LojaSaoCaetano").click(  function(){
			$("#TelefoneSaoCaetano").toggle("slow");
		});

		$("#TelefoneSaoBernador").hide();
		$("#LojaSaoBernardo").click(  function(){
			$("#TelefoneSaoBernardo").toggle("slow");
		});

		$("#TelefoneFreiGaspar").hide();
		$("#LojaFreiGaspar").click(  function(){
			$("#TelefoneFreiGaspar").toggle("slow");
		});

		$("#TelefoneAutomotivo").hide();
		$("#LojaAutomotivo").click(  function(){
			$("#TelefoneAutomotivo").toggle("slow");
		});

		$("#TelefoneMaua").hide();
		$("#LojaMaua").click(  function(){
			$("#TelefoneMaua").toggle("slow");
		});

		$("#TelefoneCentroDistribuicao").hide();
		$("#LojaCentroDistribuicao").click(  function(){
			$("#TelefoneCentroDistribuicao").toggle("slow");
		});

		//Validação formulário de contato

		$("#FormularioContato").validate({
			rules: {
				nome:"required",
				email:{required: true, email: true }, telefone:{required:true }, estado:{required: true }, cidade:{required: true }, assunto:{required: true }, mensagem: {required: true } 
			},
			messages: {
				nome: "Por favor, insira seu nome.", email : {required: "Por favor, insira seu email.", email: "Insira um email válido."}, telefone : {required: "Por favor, insira um telefone."}, estado: {required: "Por favor, escolha um estado."}, cidade: {required: "Por favor, escolha uma cidade."}, assunto: {required: "Por favor, escolha um assunto."}, mensagem: {required: "Por favor, insira uma mensagem."} 
			}
		});

		//Validação newsletter

		$("#FormularioOrcamento").validate({
			rules: {
				nome:"required",
				email:{required: true, email: true },
				telefone: {required: true},
				empresa: {required: true},
				horario: {required: true},
				mensagem: {required: true}
			},
			messages: {
				nome: "Por favor, insira seu nome.", email : {required: "Por favor, insira seu email.", email: "Insira um email válido."},telefone : {required: "Por favor, insira um telefone."}, empresa : {required: "Por favor, insira uma empresa."},horario : {required: "Selecione um horário."},mensagem : {required: "Por favor, insira uma mensagem."}
			}
		});

		//Validação trabalhe conosco

		$("#FormularioTrabalheConosco").validate({
			rules: {
				nome:"required",
				email:{required: true, email: true },
				telefone: {required: true},
				curriculo: {required: true}
			},
			messages: {
				nome: "Por favor, insira seu nome.", email : {required: "Por favor, insira seu email.", email: "Insira um email válido."},telefone : {required: "Por favor, insira um telefone."}, curriculo : {required: "Por favor, selecione um arquivo."}
			}
		});

	});		

window.onload = function(){
	document.getElementById("ativar").checked = true;
}

