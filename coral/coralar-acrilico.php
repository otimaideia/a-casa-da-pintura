<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Coral Coralar Acr&iacute;lico | A Casa da Pintura</title>
	<meta name="Description" content="Coral Coralar Acr&iacute;lico: Coralar � uma tinta acr�lica para quem deseja economia com qualidade" />
	<meta name="Keywords" content="Tintas Coral Direto Gesso A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Coralar Acr�lico" alt="Coralar Acr�lico" src="../slices/tintas-coral/img-coralar-acrilico.jpg" />
						</div>
						<h2>Coralar Acr�lico</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Coralar � uma tinta acr�lica para quem deseja economia com qualidade. � de f�cil aplica��o, r�pida secagem, m�nimo respingamento e oferece bom acabamento. Coralar resiste bem ao sol e � chuva, al�m de ter �tima resist�ncia e Cobertura. A qualidade de sua pel�cula propicia boa ader�ncia �s mais diferentes superf�cies. Indicado para pintura de superf�cies de alvenaria, cer�mica n�o vitrificada e blocos de cimentos em ambientes internos e externos.</p>
							<b>E agora sua nova f�rmula oferece maior cobertura e baixa emiss�o de CO2, contribuindo com o meio ambiente.</b>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>								
								<ul>
									<li><b>Embalagem/Rendimento</b></li>
									<li>Lata 18 L: at� 250 m� por dem�o</li>
									<li>Gal�o 3,6 L: at� 50m2 por dem�o</li>
								</ul>
								<ul>
									<li><b>Aplica��o</b></li>
									<li>Rolo de l� de p�lo baixo ou pincel de cerdas macias. Limpe as  ferramentas com �gua e sab�o</li>
								</ul>
								<ul>
									<li><b>Dilui��o</b></li>
									<li>Superf�cies n�o seladas: diluir a 1� dem�o em at� 50% e as demais de 10 a 20% com �gua pot�vel.  Superf�cies j� seladas: diluir todas as dem�os de 10 a 20% com �gua pot�vel</li>
								</ul>
								<ul>
									<li><b>Acabamento</b></li>
									<li>Fosco</li>
								</ul>
								<ul>
									<li><b>Secagem</b></li>
									<li>Ao toque: 30 min</li>
									<li>Entre dem�os: 4 horas</li>
									<li> Final: 4 horas</li>
								</ul>
							</div> 
							<div id="TintasDisponiveis">
								<span id="TintasDisponiveisTitulo">Tintas Dispon�veis:</span>
								<ul>
									<li>648 CORALAR ACR FC AMARELO VANILLA 18LT</li>
									<li>649 CORALAR ACR FC AMARELO VANILLA 3.6LT (LOTE UNICO)</li>
									<li>650 CORALAR ACR FC AREIA 18LT</li>
									<li>651 CORALAR ACR FC AREIA 3.6LT (LOTE UNICO)</li>
									<li>2167 CORALAR ACR FC AZUL ARPOADOR 18LT</li>
									<li>652 CORALAR ACR FC AZUL ARPOADOR 3.6LT (LOTE UNICO)</li>
									<li>2251 CORALAR ACR FC AZUL PRAIA 18 LTS 18LT</li>
									<li>2252 CORALAR ACR FC AZUL PRAIA 3.6LT (LOTE UNICO)</li>
									<li>653 CORALAR ACR FC BRANCO GELO 18LT</li>
									<li>654 CORALAR ACR FC BRANCO GELO 3.6LT (LOTE UNICO)</li>
									<li>655 CORALAR ACR FC BRANCO NEVE 18LT</li>
									<li>656 CORALAR ACR FC BRANCO NEVE 3.6LT 3.6LT</li>
									<li>658 CORALAR ACR FC CAMURCA 18LT</li>
									<li>657 CORALAR ACR FC CAMURCA 3.6LT (LOTE UNICO)</li>
									<li>661 CORALAR ACR FC CONCRETO 18LT</li>
									<li>662 CORALAR ACR FC CONCRETO 3.6LT (LOTE UNICO)</li>
									<li>660 CORALAR ACR FC CROMO SUAVE 18LT</li>
									<li>659 CORALAR ACR FC CROMO SUAVE 3.6LT (LOTE UNICO)</li>
									<li>664 CORALAR ACR FC MARFIM 18LT</li>
									<li>663 CORALAR ACR FC MARFIM 3.6LT (LOTE UNICO)</li>
									<li>666 CORALAR ACR FC PALHA 18LT</li>
									<li>665 CORALAR ACR FC PALHA 3.6LT (LOTE UNICO)</li>
									<li>667 CORALAR ACR FC PEROLA 18LT</li>
									<li>668 CORALAR ACR FC PEROLA 3.6LT (LOTE UNICO)</li>
									<li>669 CORALAR ACR FC PESSEGO 18LT</li>
									<li>670 CORALAR ACR FC PESSEGO 3.6LT (LOTE UNICO)</li>
									<li>2255 CORALAR ACR FC ROSA MELODIA 18LT</li>
									<li>2168 CORALAR ACR FC VERDE PISCINA 18LT</li>
									<li>671 CORALAR ACR FC VERDE PISCINA 3.6LT (LOTE UNICO)</li>
									<li>2253 CORALAR ACR FC VERDE VALE 18LT</li>
									<li>2254 CORALAR ACR FC VERDE VALE 3.6LT (LOTE UNICO)</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>					
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>