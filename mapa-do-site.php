<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Mapa do Site | Loja de Tintas A Casa da Pintura</title>
	<meta name="Description" content="Mapa do Site da loja de tintas A Casa da Pintura. Saiba como comprar tintas de qualidade no site da Casa da pintura" />
	<meta name="Keywords" content="apa Site loja tintas comprar " />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "componentes/includes.php"; ?>
</head>
<body>
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "componentes/topo.php"; ?>
			</div>
		</div>	
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoMapa">
					<h2>Mapa do Site</h2>	
					<ul>
						<li><strong>Loja de Tintas - P�gina Inicial</strong></li>
						<li><a href="index.php" title="Home Page - Loja de Tintas">Home Page - Loja de Tintas</a></li>
					</ul>
					<ul>
						<li><strong>Tintas Imobili�rias</strong></li>
						<li><a href="tintas-imobiliarias.php" title="Tintas Imobili�rias">Tintas Imobili�rias</a></li>
					</ul>
					<ul>
						<li><strong>Tintas Automotivas</strong></li>
						<li><a href="tintas-automotivas.php" title="Tintas Automotivas">Tintas Automotivas</a></li>
					</ul>
					<ul>
						<li><strong>Tintas Industriais</strong></li>
						<li><a href="tintas-automotivas.php" title="Tintas Industriais">Tintas Industriais</a></li>
						<li><a href="advance-tintas.php" title="Tintas Industriais - Advance Tintas">Tintas Industriais - Advance Tintas</a></li>
						<li><a href="maquina-advance-color.php" title="Tintas Industriais - Desenvolvemos Cores">Tintas Industriais - Desenvolvemos Cores</a></li>
					</ul>
					<ul>	
						<li><strong>Tintas Coral</strong></li>
						<li><a href="tintas-coral.php" title="Tintas Coral">Tintas Coral</a></li>
						<li><a href="coral/chega-de-mofo.php" title="Tintas Coral Chega de Mofo">Tintas Coral Chega de Mofo </a></li>
						<li><a href="coral/fundo-galvanizado-branco.php"  title="Tintas Coral Fundo para Galvanizado Branco">Tintas Coral Fundo para Galvanizado Branco</a></li>
						<li><a href="coral/direto-no-gesso.php" title="Tintas Coral Direto no Gesso">Tintas Coral Direto no Gesso</a></li>
						<li><a href="coral/ceramica.php" title="Cer�mica Coral">Cer�mica Coral</a></li>
						<li><a href="coral/coralar-acrilico.php" title="Tintas Coral Coralar Acr�lico">Tintas Coral Coralar Acr�lico</a></li>
						<li><a href="coral/coralar-esm-acetinado.php" title="Tintas Coralar Esmalte Acetinado">Tintas Coralar Esmalte Acetinado</a></li>
						<li><a href="coral/coralar-esmalte.php" title="Tintas Coral Coralar Esmalte">Tintas Coral Coralar Esmalte</a></li>
						<li><a href="coral/coralit-ac-bco-gelo.php" title="Tintas Coralit Acetinado Branco Gelo">Tintas Coralit Acetinado Branco Gelo</a></li>
						<li><a href="coral/coralmur-zero-odor.php" title="Tintas Coral Coralmur Zero Odor">Tintas Coral Coralmur Zero Odor</a></li>
						<li><a href="coral/corante-base-dagua.php" title="Tintas Corante Base �gua Coral">Tintas Corante Base �gua Coral</a></li>
						<li><a href="coral/decora-brancos.php" title="Tintas Coral Decora Brancos">Tintas Coral Decora Brancos</a></li>
						<li><a href="coral/decora-cores.php" title="Tintas Coral Decora Cores">Tintas Coral Decora Cores</a></li>
						<li><a href="coral/decora-luz-espaco.php" title="Tintas Coral Decora Luz Espa�o">Tintas Coral Decora Luz Espa�o</a></li>
						<li><a href="coral/esmalte-fosco-coralit.php" title="Tintas Coral Esmalte Fosco Coralit">Tintas Coral Esmalte Fosco Coralit</a></li>
						<li><a href="coral/esmalte-altobrilho-coralit.php" title="Tintas Esmalte Alto Brilho Coralit">Tintas Esmalte Alto Brilho Coralit</a></li>
						<li><a href="coral/fundo-preparador-paredes-base-agua.php" title="Tintas Fundo Preparador de Paredes Base �gua">Tintas Fundo Preparador de Paredes Base �gua</a></li>
						<li><a href="coral/grafite.php" title="Grafite Coral">Grafite Coral</a></li>
						<li><a href="coral/hammerite-esmalte-sintetico.php" title="Tintas Hammerite Esmalte Sint�tico Anti-ferrugem">Tintas Hammerite Esmalte Sint�tico Anti-ferrugem</a></li>
						<li><a href="coral/massa-acrilica.php" title="Massa Acr�lica Coral">Massa Acr�lica Coral</a></li>
						<li><a href="coral/massa-corrida.php" title="Massa Corrida Coral">Massa Corrida Coral</a></li>
						<li><a href="coral/massa-para-madeira.php" title="Massa para Madeira Coral">Massa para Madeira Coral</a></li>
						<li><a href="coral/pinta-piso.php" title="Tintas Coral Pinta Piso">Tintas Coral Pinta Piso</a></li>
						<li><a href="coral/rende-muito-coralatex-fosco.php" title="Tintas Coral Rende Muito Coralatex Fosco">Tintas Coral Rende Muito Coralatex Fosco</a></li>
						<li><a href="coral/selador-acrilico.php" title="Tintas Coral Selador Acr�lico">Tintas Coral Selador Acr�lico</a></li>
						<li><a href="coral/seladora-madeira.php" title="Tintas Coral Seladora Madeira">Tintas Coral Seladora Madeira</a></li>
						<li><a href="coral/sol-e-chuva.php" title="Tintas Coral Sol e Chuva">Tintas Coral Sol e Chuva</a></li>
						<li><a href="coral/sparlack-cetol.php" title="Coral Sparlack Cetol">Coral Sparlack Cetol</a></li>
						<li><a href="coral/solvente-para-hammerite.php" title="Solvente para Hammerite Coral">Solvente para Hammerite Coral</a></li>
						<li><a href="coral/fundo-reparador-coralit-zero.php" title="Fundo Reparador Coralit Zero">Fundo Reparador Coralit Zero</a></li>
					</ul>	
					<ul>
						<li><strong>Tintas Advance</strong></li>
						<li><a href="advance/advance-tintas.php" title="Advance Tintas">Advance Tintas</a></li>
						<li><a href="advance/122-primer.php" title="122 Primer">122 Primer</a></li>
						<li><a href="advance/519-adlux-primier-fosco.php" title="519 Adlux Primier Fosc">519 Adlux Primier Fosco</a></li>
						<li><a href="advance/530-adlux-df-brilhante-extra-rapido.php" title="530 adlux df brilhante extra r�pido">530 adlux df brilhante extra r�pido</a></li>
						<li><a href="advance/630-adlux-acabamento-brilhante.php" title="630 Adlux Acabamento Brilhante">630 Adlux Acabamento Brilhante</a></li>
						<li><a href="advance/601-adlux-df-semibrilho-extra-rapido.php" title="601 Adlux DF Semibrilho Extra R�pido">601 Adlux DF Semibrilho Extra R�pido</a></li>
						<li><a href="advance/70-tl-acabamento.php" title="70 TL Acabamento">70 TL Acabamento</a></li>
						<li><a href="advance/70-tl-primer.php" title="70 TL Primer">70 TL Primer</a></li>
						<li><a href="advance/772-DF-sem-brilho.php" title="772 DF Sem Brilho">772 DF Sem Brilho</a></li>
						<li><a href="advance/870-primer-fosco.php" title="870 Primer Fosco">870 Primer Fosco</a></li>
						<li><a href="advance/878-primer-acetinado.php" title="878 Primer Acetinado">878 Primer Acetinado</a></li>
						<li><a href="advance/96-acamento-brilhante.php" title="96 Acamento Brilhante">96 Acamento Brilhante</a></li>
						<li><a href="advance/adepoxi-80df.php" title="Adepoxi 80 DF">Adepoxi 80 DF</a></li>
						<li><a href="advance/adepoxi-86df.php" title="Adepoxi 86 DF">Adepoxi 86 DF</a></li>
						<li><a href="advance/adepoxi-86df.php" title="Adepoxi 86 DF">Adepoxi 86 DF</a></li>
						<li><a href="advance/ecoclinic-acrilico.php" title="Ecoclinic Acr�lico">Ecoclinic Acr�lico</a></li>
						<li><a href="advance/adpoly-7990-acabamento.php" title="AdPoly 7990 Acabamento">AdPoly 7990 Acabamento</a></li>
						<li><a href="advance/ecopoxi-828.php" title="Ecopoxi 828">Ecopoxi 828</a></li>
						<li><a href="advance/ecopoxi-828.php" title="Ecopoxi 828">Ecopoxi 828</a></li>
					</ul>	
					<ul>
						<li><strong>Tintas Lazzuril</strong></li>
						<li><a href="/tintas-lazzuril.php" title="Lazurril'">Lazzuril</a></li>	
					</ul>
					<ul>
						<li><strong>Tintas Suvinil</strong></li>
						<li><a href="tintas-suvinil.php" title="Suvinil">Suvinil</a></li>	
						<li><a href="acrilico-fosco.php" title="Suvinil Acr�lico Premium Fosco">Suvinil Acr�lico Premium Fosco</a></li>	
						<li><a href="acrilico-premium-limpa-facil.php" title="Suvinil Acrilico Premium Limpa F�cil">Suvinil Acrilico Premium Limpa F�cil</a></li>	
						<li><a href="acrilico-premium-semibrilho.php" title="Suvinil Acrilico Premium Semibrilho">Suvinil Acrilico Premium Semibrilho</a></li>	
						<li><a href="acrilico-premium-toque-de-seda.php" title="Suvinil Acrilico Premium Toque de Seda">Suvinil Acrilico Premium Toque de Seda</a></li>	
						<li><a href="acrilico-toque-de-seda.php" title="Suvinil Acr�lico Toque de Seda">Suvinil Acr�lico Toque de Seda</a></li>	
						<li><a href="contra-microfissuras.php" title="Suvinil Contra Microfissuras">Suvinil Contra Microfissuras</a></li>	
						<li><a href="esmalte-auto-brilho.php" title="Tinta para Pisos Suvinil">Tinta para Pisos Suvinil</a></li>	
						<li><a href="esmalte-fosco.php" title="Suvinil Emalter Fosco">Suvinil Emalter Fosco</a></li>	
						<li><a href="fundo-preparador.php" title="Fundo Preparador">Fundo Preparador</a></li>	
						<li><a href="latex-maxx.php" title="Fundo Preparador">Sunivil Latex Maxx</a></li>	
						<li><a href="massa-acrilica.php" title="Suvinil Massa Acr�lica">Suvinil Massa Acr�lica</a></li>	
						<li><a href="massa-corrida.php" title="Suvinil Massa Corrida">Suvinil Massa Corrida</a></li>	
						<li><a href="pisos-premium.php" title="Tinta para Pisos Suvinil">Tinta para Pisos Suvinil</a></li>	
						<li><a href="verniz-maritimo.php" title="Suvinil Verniz Maritimo">Suvinil Verniz Maritimo</a></li>	
					</ul>	
					<ul>
						<li><strong>Tintas Spray</strong></li>
						<li><a href="tintas-spray.php" title="Spray">Spray</a></li>								
					</ul>	
					<ul>
						<li><strong>Acessorios de Pintura</strong></li>
						<li><a href="acessorios-de-pintura.php" title="Acessorios de Pintura">Acessorios de Pintura </a></li>								
					</ul>
					<ul>
						<li><strong>Lojas e Endere�os de Lojas</strong></li>
						<li><a href="casa-da-pintura-lojas.php" title="Lojas de tintas | Endere�os A Casa da Pintura">Lojas de tintas | Endere�os A Casa da Pintura</a></li>								
					</ul>
					<ul>
						<li><strong>P�ginas de Contato</strong></li>
						<li><a href="contato.php" title="Contato">Contato</a></li>								
						<li><a href="trabalhe-conosco.php" title="Trabalhe Conosco">Trabalhe Conosco</a></li>								
					</ul>			
				</div>
			</div>
		</div>		
		<div id="Linha3">
			<? include "componentes/rodape.php"; ?>
		</div>	
	</div>
	<div id="mask"></div>
</body>
</html>