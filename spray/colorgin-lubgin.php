<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Colorgin Lubgin | Spray A Casa da Pintura</title>
	<meta name="Description" content="Colorgin Alta Temperatura � uma tinta em spray especialmente desenvolvida para pintura de partes externas de objetos" />
	<meta name="Keywords" content="tintas spray colorgin" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Colorgin Lubgin" alt="Colorgin Lubgin" src="../slices/spray-colorgin/img-colorgin-lubgin.jpg" />
						</div>
						<h2>Colorgin Lubgin</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p><b>Colorgin Lubgin </b>� um lubrificante em spray de alta  qualidade, que penetra, lubrifica, repele a umidade e evita a ferrugem.  Indispens�vel na casa, oficina, escrit�rio, f�brica, etc. Facilita a remo��o de  parafusos e porcas enferrujadas e � neutro, n�o ataca pinturas, pl�sticos e  borrachas.</p>
							<p><b>Indicada para:</b> fechaduras, dobradi�as, ferramentas,  portas e janelas, brinquedos, rolamentos, m�quinas, motores, chaves el�tricas e  utens�lios dom�sticos em geral. </p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<p><b>Preparo da Superf�cie/  Aplica��o:</b> </p>
								<ul>
									<li>Elimine  a poeira, gordura ou qualquer contaminante.</li>
									<li>Aplique  a uma dist�ncia de 25 cm  da superf�cie.</li>
								</ul>
								<p> Ao final do uso, limpe a v�lvula virando a lata para  baixo e pressione at� que saia apenas g�s.</p>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>