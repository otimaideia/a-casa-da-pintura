<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title> Suvinil Acr�lico Premium Limpa F�cil | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas</title>
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Suvinil Acr�lico Premium Limpa F�cil" alt="Suvinil Acr�lico Premium Limpa F�cil" src="../slices/tintas-suvinil/img-suvinil-acrilico-fosco.jpg" />
						</div>
						<h2>Acabamento Poli�ster MS</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>� uma tinta acr�lica de acabamento acetinado, especialmente formulada para as paredes onde a limpeza costuma ser mais freq�ente. </p>
							<p><b>Aplica��o</b></p>
							<p>Paredes</p>
							<p><b>Local de Aplica��o</b></p>
							<p>Exterior / Interior</p>
							<p><b>Acabamento</b></p>
							<p><span>Acetinado</span></p>
						</div>
						<div id="InformacoesAdicionais">
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>	
								<ul>
									<li>LIMPA FACIL BRANCO 18LT</li>
									<li>LIMPA FACIL BRANCO 3.6LT </li>
								</ul>								
								<ul>
									<li><b>Composi��o:</b></li>
									<li><span>Resina � base de dispers�o aquosa de copol�mero   estireno acr�lico, pigmentos isentos de metais pesados, cargas minerais   inertes, hidrocarbonetos alif�ticos, glic�is e tensoativos etoxilados e   carboxilados</span>.</li>
									<li><b><span><b>Observa��o:</b></span></b></li>
									<li><span>Sem Cheiro / Muito Menos Respingos</span> </li>
									<li><b>Importante:</b></li>
									<li>Para se obter o m�ximo da qualidade dos produtos Suvinil � necess�rio um bom <b>preparo da superf�cie</b> a ser aplicada.</li>
								</ul>	
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>