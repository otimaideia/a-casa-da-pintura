<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Coral Esmalte Fosco Coralit | A Casa da Pintura</title>
	<meta name="Description" content="Coral Esmalte Fosco Coralit: Usado geralmente em madeiras e metais, o esmalte cria uma pel�cula protetora que permite f�cil limpeza" />
	<meta name="Keywords" content="Tintas Coral coralit esmalte fosco Gesso A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="http://www.belatintas.com.br/esmalte-brilhante-tabaco-coralit-1-16-1-17.html" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Tinta Esmalte Fosco Coralit" alt="Tinta Esmalte Fosco Coralit" src="../slices/tintas-coral/img-esmalte-fosco-coralit.jpg" />
						</div>
						<h2>Esmalte Fosco Coralit</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Usado geralmente em madeiras e metais, o esmalte cria uma pel�cula protetora que permite f�cil limpeza. O esmalte sint�tico fosco se diferencia dos demais pela sua aus�ncia total de brilho. Use aguarr�s como solvente.</p>
						</div>
						<div id="InformacoesAdicionais">
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>								
								<ul>
									<li><b>Org�o Normatiza��o:</b> ABNT</li>
									<li><b>Produto Qu�mico:</b> Sim</li>
									<li><b>Produto Qu�mico Utiliza��o: </b> Decora��o, pintura em alvenaria, madeiras e metais</li>
									<li><b>Tipo Unidade:</b> Unidade</li>
									<li><b>Peso Produto: </b> 4,566 kg</li>
									<li><b>Altura Produto: </b> 0,19 m</li>
									<li><b>Largura Produto: </b> 0,345 m</li>
									<li><b>Comprimento Produto:</b> 0,34 m</li>
									<li><b>Possui Garantia?</b> Sim</li>
								</ul>
							</div> 
							<div id="TintasDisponiveis">
								<span id="TintasDisponiveisTitulo">Tintas Dispon�veis</span>
								<ul>
									<li>Coralit FC Branco 0.9LT</li>
									<li>Coralit FC Branco GL</li>
									<li>Coralit FC Preto 0.9LT</li>
									<li>Coralit FC Preto 225ML</li>
									<li>2982 - CORALIT ZERO B'AGUA AC BRANCO 0.9LT</li>
									<li>Coralit FC Preto GL</li>
									<li>Coralit FC VD Escolar GL</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>