<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title> Suvinil Latex Maxx | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas</title>
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Suvinil Latex Maxx" alt="Suvinil Latex Maxx" src="../slices/tintas-suvinil/img-latex-maxx.jpg" />
						</div>
						<h2>Suvinil Latex Maxx</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>F�cil aplica��o, �tima cobertura, secagem r�pida, 40% mais rendimento, 2 vezes mais resistente e baixo odor.  </p>
							<p><b>Aplica��o</b></p>
							<p>Paredes</p>
							<p><b>Local de Aplica��o</b></p>
							<p>Exterior / Interior</p>
							<p>Reboco, massa acr�lica, texturas, concreto, fibro-cimento, gesso e superf�cies internas de massa corrida.</p>
							<p><b>Acabamento</b></p>
							<p>Fosco Aveludado.</p>
						</div>
						<div id="InformacoesAdicionais">
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>	
								<ul>
									<li><b>Tintas Dispon�veis</b></li>
									<li>LATEX PVA MAXX AMARELO CANARIO 18LT</li>
									<li>LATEX PVA MAXX AREIA 3.6LT</li>
									<li>LATEX PVA MAXX BRANCO 3.6LT</li>
									<li>LATEX PVA MAXX BRANCO NEVE 0.9LT</li>
									<li>LATEX PVA MAXX BRANCO NEVE 18LT</li>
									<li>LATEX PVA MAXX CONCRETO 18LT</li>
									<li>LATEX PVA MAXX CONCRETO 3.6LT</li>
									<li>LATEX PVA MAXX GELO 18LT</li>
									<li>LATEX PVA MAXX GELO 3.6LT</li>
									<li>LATEX PVA MAXX LINHO 18LT</li>
									<li>LATEX PVA MAXX LINHO 3.6LT</li>
									<li>LATEX PVA MAXX LIRIO 18LT</li>
									<li>LATEX PVA MAXX LIRIO 3.6LT</li>
									<li>LATEX PVA MAXX MARFIM 18LT</li>
									<li>LATEX PVA MAXX MARFIM 3.6LT</li>
									<li>LATEX PVA MAXX PALHA 18LT</li>
									<li>LATEX PVA MAXX PALHA 3.6LT</li>
									<li>LATEX PVA MAXX PEROLA 18LT</li>
									<li>LATEX PVA MAXX PEROLA 3.6LT</li>
									<li>LATEX PVA MAXX TANGERINA 18LT</li>
									<li>LATEX PVA MAXX TANGERINA 3.6LT</li>
									<li>LATEX PVA MAXX UVA VERDE 18LT</li>
									<li>LATEX PVA MAXX UVA VERDE 3.6LT
									</li>
								</ul>
								<ul>
									<li><b>Composi��o:</b></li>
									<li>Resina a base de dispers�o aquosa de pol�meros   acr�licos e vin�licos,pigmentos isentos de metais pesados,cargas   inertes,glic�is e tenso-ativos etoxilados e carboxilados, bactericida e   fungicida a  base de isotiazolonas.</li>
									<li><b>Observa��o:</b></li>
									<li>Nova Embalagem de 9 L</li>
									<li><b>Importante:</b></li>
									<li>Para se obter o m�ximo da qualidade dos produtos Suvinil � necess�rio um bom <b>preparo da superf�cie</b> a ser aplicada..</li>
								</ul>								
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>