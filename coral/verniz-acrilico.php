<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Coral Verniz Acr&iacute;lico | A Casa da Pintura</title>
	<meta name="Description" content="Coral Verniz Acr&iacute;lico: O Verniz Acr�lico deve ser utilizado para dar maior prote��o e melhor acabamento �s paredes externas e internas" />
	<meta name="Keywords" content="Tintas Coral verniz acrilico A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Verniz Acr�lico" alt="Verniz Acr�lico" src="../slices/tintas-coral/img-verniz-acrilico.jpg" />
						</div>
						<h2>Verniz Acr�lico</h2>

						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>O Verniz Acr�lico deve ser utilizado para dar maior prote��o e melhor acabamento �s paredes externas e internas de concreto, pedra mineira, ard�sia e tijolo � vista. Possui resist�ncia � alcalinidade, � a��o da maresia e tamb�m ao sol e � chuva. � dilu�vel em �gua e, quando seco, fica incolor, proporcionando maior brilho, beleza e facilidade de limpeza � superf�ci.</p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes</span>
								<ul><li><b>Embalagem/Rendimento</b></li>
								</ul>
								<ul>
									<li>Lata <b>18 L</b>: 225 a 275 m� por dem�o</li>
									<li>Gal�o <b>3,6  L</b>: 45 a  55 m�  por dem�o</li>
								</ul>
								<ul>
									<li><b>Aplica��o</b></li>
									<li>Rolo de l�, pincel ou trincha. Limpe as ferramentas com �gua e sab�o. </li>
									<li><b>Dilui��o</b></li>
									<li>Diluir em at� 10% com �gua limpa. Em superf�cies n�o seladas, aplicar a  primeira dem�o dilu�da com 30% de �gua limpa. </li>
									<li><b>Acabamento</b>: Brilhante </li>
									<li><b>Secagem</b></li>
									<li>Ao toque: 2 horas</li>
									<li>Entre dem�os: 4 horas</li>
									<li>Final: 4 horas</li>
								</ul>
							</div> 
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>