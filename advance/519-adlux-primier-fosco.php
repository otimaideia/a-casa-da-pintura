<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>519 Adlux Primier Fosco | Advance Tintas</title>
	<meta name="Description" content="519 Adlux Primier Fosco: Primer alqu�dico anticorrosivo fosco de secagem r�pida. Atende a Norma Alcoa Alum�nio ET-11." />
	<meta name="Keywords" content="Tintas advance tintas 519 Adlux Primier Fosco A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="519 Adlux Primier Fosco " alt="519 Adlux Primier Fosco " src="../slices/tintas-advance/img-519-adlux-p-fosco.jpg" />
						</div>
						<h2>519 Adlux Primier Fosco</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Primer alqu�dico anticorrosivo fosco de secagem r�pida. Atende a Norma Alcoa Alum�nio ET-11.</p>
							<p><b>Locais para Aplica��o:</b>  Metais</p>
							<p><a target="_blank" title="Dados t�cnicos 519 Adlux Primier Fosco " href="pdf/519-adlux.pdf">Dados T�cnicos (PDF)</a></p>
							<span class="Titulo">Mais informa��es sobre a tinta</span>
							<p>Primer alqu�dico anticorrosivo fosco. Sua principal caracter�stica � a secagem r�pida com excelente ades�o e penetra��o ao substrato. Apresenta boa resist�ncia anticorrosiva e permite uma repintura r�pida do acabamento com excelente ancoragem. Atende a Norma Alcoa Alum�nio ET-11. Indicado para superf�cies met�licas tais como ferro e a�o. Devido a sua secagem r�pida e resist�ncia a intemp�ries, � ideal para equipamentos industriais de produ��o em s�rie, linha de produ��o de equipamentos agr�colas, bombas, transformadores etc. Para manuten��o industrial em revestimento de tanques, estruturas, caldeiras etc. Apresenta boa dureza, flexibilidade e ades�o.</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>