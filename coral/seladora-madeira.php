<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Coral Seladora Madeira | A Casa da Pintura</title>
	<meta name="Description" content="Coral Seladora Madeira: Proporciona excelente nivelamento, que prepara superf�cies de madeira nova para receber o verniz de acabamento" />
	<meta name="Keywords" content="Tintas Coral Selador Madeira A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="http://www.belatintas.com.br/esmalte-brilhante-tabaco-coralit-1-16-1-17.html" target="_blank" title="Fazer pedido">Fazer pedido</a>

						<div id="ImagemProduto">
							<img title="Tinta Seladora Madeira" alt="Tinta Seladora Madeira" src="../slices/tintas-coral/img-seladora-madeira.jpg" />
						</div>
						<h2>Seladora Madeira</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Proporciona excelente nivelamento, que prepara superf�cies de madeira nova para receber o verniz de acabamento. Seu uso evita problemas como forma��o de bolhas, falta de ader�ncia e perda de brilho do acabamento. Indicada para selar e uniformizar superf�cies internas de madeira, como m�veis, aglomerados, portas, janelas, etc</p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>																
								<ul>
									<li><b>Embalagem/Rendimento:</b></li>
									<li>Gal�o 3,6 L : 40 a 50 m� por dem�o</li>
									<li>Quarto 0,9 L: 7,5 a 8,75 m� por dem�o</li>
									<li><b>Aplica��o</b></li>
									<li>�reas grandes: trincha, rev�lver ou rolo de espuma. �reas pequenas:  pincel com cerdas macias ou boneca. Limpe as ferramentas com diluente  apropriado para nitrocelulose. </li>
									<li><b>Dilui��o</b></li>
									<li>Utilizar diluente apropriado para nitrocelulose. Diluir at� 100%. </li>
									<li><b>Secagem: </b>Ao toque: 10 min, entre dem�os: 1 hora, final: 2 horas</li>
								</ul>									
							</div>
							<div id="TintasDisponiveis">
								<span id="TintasDisponiveisTitulo">Tintas Dispon�veis</span>
								<ul>
									<li>Fund. Sintetico Nivel Branco 0.9LT - COD 752</li>
									<li>Fund. Sintetico Nivel Branco 3.6LT - COD 751</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>