<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Desenvolvidas | Anticorrosivas, Mar�tima | Casa da Pintura</title>
	<meta name="description" content="Desenvolvemos cores conforme necessidade! Ep�xi, a base de �gua, base solvente, poliuretano, Anticorrosivas, tudo na Casa da Pintura! "/>
	<meta name="keywords" content="anticorrosivas, mar�tima, advance, pintura maritima, industriais, para tanque, a casa da pintura, tintas desenvolvidas, tintas personalizadas" />
	<? include "componentes/includes.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "componentes/topo.php"; ?>
			</div>
		</div>	
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="MaquinaAdvance">
					<h2>M�quina Advence Color</h2>
					<img src="slices/img-maquina-advance-color.jpg" title="Maquina Advence Color" alt="Maquina Advence Color" />	
					<p>Desenvolvemos cores conforme necessidade do cliente:</p>
					<p>Com a m�quina Advance Color preparamos na hora todos os padr�es de cores industriais RAL e MUNSELL em toda linha:</p>
					<p>Ep�xi base �gua, base solvente, poliuretano, sint�tico base �gua e base solvente.</p>
					<p>Para todos os fins:</p>
					<ul>
						<li>Anticorrosivas</li>
						<li>Estruturas Met�licas</li>
						<li>Pintura Mar�tima</li>
						<li>Pisos Industriais</li>
						<li>Tanques</li>
					</ul>
				</div>
			</div>
		</div>		
		<div id="Linha3">
			<? include "componentes/rodape.php"; ?>
		</div>	
	</div>
	<div id="mask"></div>
</body>
</html>