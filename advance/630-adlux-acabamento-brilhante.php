<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>630 Adlux Acabamento Brilhante | Advance Tintas</title>
	<meta name="Description" content="630 Adlux Acabamento Brilhante: Acabamento alqu�dico de secagem normal. Recomendado para ambientes variados." />
	<meta name="Keywords" content="Tintas advance tintas adlux acabamento brilhante A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="630 Adlux Acabamento Brilhante" alt="519 Adlux Primier Fosco " src="../slices/tintas-advance/img-630-adlux.jpg" />
						</div>
						<h2>630 Adlux Acabamento Brilhante</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Acabamento alqu�dico de secagem normal. Recomendado para ambientes variados.</p>
							<p><b>Locais para Aplica��o:</b>  Metais</p>
							<p><a target="_blank" title="Dados t�cnicos 630 Adlux Acabamento Brilhante" href="pdf/630-adlux.pdf">Dados T�cnicos (PDF)</a></p>
							<span class="Titulo">Mais informa��es sobre a tinta</span>
							<p>Acabamento alqu�dico de secagem normal. Recomendado para ambientes variados por possuir um bom desempenho quando exposto ao intemper�smo. Indicado como revestimento est�tico e protetivo para estruturas de a�o e ferro, como acabamento para interiores e exteriores previamente pintados com primer anticorrosivo. Recomendada aplica��o em duas dem�os de 25 a 30 microns cada, equipamentos para ind�strias, pontes, m�quinas, tubula��es, bombas, parte externa de tanques expostos em ambiente de baixa e m�dia agressividade. N�o recomendado para servi�os de imers�o, ambientes com agressividade qu�mica, ataque de vapores ou derrames de solventes. N�o recomendado para exposi��o direta a �cidos ou bases. Apresenta boa dureza, flexibilidade e ades�o.</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>