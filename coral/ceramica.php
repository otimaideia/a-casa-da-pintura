<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Cer&acirc;mica Coral | A Casa da Pintura</title>
	<meta name="Description" content="Coral Ceramica: Embeleza Cer�mica � a tinta capaz de renovar e deixar as telhas e tijolos de sua casa bonitos como novos" />
	<meta name="Keywords" content="Tintas Coral ceramica A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Produto">
						<div id="Informacoes">
							<a id="FazerPedido" href="http://www.belatintas.com.br/esmalte-brilhante-ceramica-coralit-galao-galao.html" target="_blank" title="Fazer pedido">Fazer pedido</a>
							<div id="ImagemProduto">
								<img title="Cer�mica Coral" alt="Cer�mica Coral" src="../slices/tintas-coral/img-ceramica-coral.jpg" />
							</div>
							<h2>Cer�mica</h2>
							<div id="InformacoesProduto">
								<span class="Titulo">Descri��o do produto</span>
								<p>Embeleza Cer�mica � a tinta capaz de renovar e deixar as telhas e tijolos de sua casa bonitos como novos. � f�cil de aplicar e possui boa cobertura, oferecendo prote��o por muito mais tempo. Dispon�vel em sua cor tradicional, Embeleza Cer�mica tamb�m � indicada para objetos cer�micos n�o vitrificados (tipo porcelanato ou com brilho) e elementos vazados.</p>
							</div>
							<div id="InformacoesAdicionais"> 
								<div id="Detalhes">
									<span id="Detalhe">Detalhes:</span>
									<ul>
										<li><b>Refer�ncia:</b> PC</li>
										<li>Marca: A Casa da Pintura</li>
										<li>Unidade: PC</li>
									</ul>
									<ul>
										<li><b>Embalagem/Rendimento</b></li>
										<li>Gal�o 3,6 L: 40 a 50 m� por dem�o</li>
										<li>Quarto 0,9 L: 10 a 12,5 m� por dem�o</li>
									</ul>
									<ul>
										<li><b>Aplica��o</b></li>
										<li>�reas grandes: rolo de espuma ou rev�lver. �reas pequenas: pincel com cerdas macias. Limpe as ferramentas com Coralraz.</li>
									</ul>
									<ul>
										<li><b>Dilui��o</b></li>
										<li>Usar diluente Coralraz. Aplica��o pincel/rolo diluir no m�ximo 10%. Aplica��o rev�lver diluir no m�ximo 30%. </li>
									</ul>
									<ul>
										<li><b>Acabamento</b></li>
										<li>Brilhante</li>
									</ul>
									<ul>
										<li><b>Secagem</b></li>
										<li>Ao toque: 4 a 6  horas</li>
										<li>Entre dem�os: 8 horas</li>
										<li>Final: 18 a 24 horas</li>
									</ul>	
								</div>
								<div id="TintasDisponiveis">
									<span id="TintasDisponiveisTitulo">Tintas Dispon�veis:</span>
									<ul>
										<li>CERAMICA 0.9LT </li>
										<li>CERAMICA 3.6LT</li>
									</ul>
								</div>								
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>	
					<? include "../componentes/outros-produtos.php"; ?>						
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>