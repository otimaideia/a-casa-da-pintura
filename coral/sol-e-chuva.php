<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Coral Sol e Chuva | A Casa da Pintura</title>
	<meta name="Description" content="Coral Sol e Chuva: Essa � a tinta para quem quer proteger as paredes externas da casa, fa�a sol, fa�a chuva." />
	<meta name="Keywords" content="Tintas Coral sol chuva Gesso A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Produto">
						<div id="Informacoes">
							<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
							<div id="ImagemProduto">
								<img title="Tinta Coral Sol e Chuva" alt="Tinta Coral Sol e Chuva" src="../slices/tintas-coral/img-sol-e-chuva.jpg" />
							</div>
							<h2>Sol e Chuva</h2>
							<div id="InformacoesProduto">
								<span class="Titulo">Descri��o do produto</span>
								<p>Essa � a tinta para quem quer proteger as paredes externas da casa, fa�a sol, fa�a chuva. Coral Sol & Chuva protege suas paredes tamb�m contra maresia. � uma tinta acr�lica el�stica que forma uma pel�cula imperme�vel flex�vel, capaz de acompanhar a dilata��o e retra��o de paredes em alvenaria ou concreto, causadas pela mudan�a de temperatura. Nem a umidade consegue atingir a sua parede. Ela � f�cil de aplicar, possui excelente cobertura e aspecto emborrachado, que d� uma �tima resist�ncia e durabilidade.</p>
							</div>
							<div id="InformacoesAdicionais"> 
								<div id="Detalhes">
									<span id="Detalhe">Detalhes:</span>	
									<ul>
										<li><b>Embalagem/Rendimento</b></li> 
										<li>Lata 18 L: 125 a 175 m� por dem�o</li>
										<li>Gal�o 3,6 L: 34 a 44 m� por dem�o</li>
										<li>Quarto 0,8 L: 9,7 a 12,5 m� por dem�o</li>
										<li><b>Aplica��o</b></li>
										<li>Rolo de l� de p�lo baixo ou pincel de cerdas macias.<br>
											Limpe as ferramentas com �gua e sab�o. </li>
											<li><b>Dilui��o</b></li>
											<li>Diluir todas as dem�os em 10% com �gua pot�vel. </li>
											<li><b>Acabamento</b>: Fosco</li>
											<li><b>Secagem</b></li>
											<li>Ao toque: 2 horas, entre dem�os: 4 horas, final: 24 horas</li>
										</ul>		
									</div>	
									<div id="TintasDisponiveis">
										<span id="TintasDisponiveisTitulo">Tintas Dispon�veis</span>
										<ul>
											<li>Sol e Chuva Branco 18L (Emborrachada) - COD 5560 </li>
											<li>Sol e Chuva Branco GL (Emborrachada) - COD 3407 </li>
										</ul>
									</div>						
								</div>
							</div>
						</div>
						<? include "../componentes/solicitar-orcamento.php"; ?>
						<? include "../componentes/outros-produtos.php"; ?>
					</div>
				</div>
			</div>
			<div id="Linha3">
				<? include "../componentes/rodape-tintas.php"; ?>
			</div>
		</div>
		<div id="mask"></div>
	</body>
	</html>