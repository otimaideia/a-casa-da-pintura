<?php
date_default_timezone_set('America/Sao_Paulo');

include 'lumine/lumine-conf.php';
include 'lumine/Lumine.php';

$cfg = new Lumine_Configuration($lumineConfig);
spl_autoload_register(array('Lumine','import'));
register_shutdown_function(array($cfg->getConnection(),'close'));

?>