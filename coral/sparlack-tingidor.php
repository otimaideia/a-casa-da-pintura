<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Detalhes | A Casa da Pintura</title>
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Produto">
						<div id="Informacoes">
							<a id="FazerPedido" href="http://www.belatintas.com.br/verniz-brilhante-sparlack-maritimo-galao-galao.html" target="_blank" title="Fazer pedido">Fazer pedido</a>
							<div id="ImagemProduto">
								<img title="Sparlack Tingidor" alt="Sparlack Tingidoro" src="../slices/tintas-coral/img-tingidor.jpg" />
							</div>
							<h2>Sparlack Tingidor</h2>
							<div id="InformacoesProduto">
								<span class="Titulo">Descri��o do produto</span>	
								<p>Desenvolvido para tingimento de vernizes do tipo sint�ticos ou poliuretanos, seladoras � base de nitrocelulose e stains.</p>
								<p>Indicado para enobrecimento e personaliza��o de madeiras e m�veis externos e internos;</p>
								<p>Possui alta concentra��o de pigmentos;</p>
								<p>F�cil utiliza��o;</p>	
								<p>Permite a personaliza��o de cores</p>
								<p>1) Dilui��o: N�o diluir o produto. Pronto para uso.</p>	
								<p>2) Aplica��o: Por se tratar de um tingidor concentrado, adicione em pequenas quantidades ao verniz a ser tingido, agitando at� atingir a tonalidade desejada. Utilizar no m�ximo uma embalagem (100 ml ) por gal�o de 3,6 litros do verniz a ser tingido.</p>					
								<p>3) Secagem: Ao toque, em 4 horas. Ao manuseio, em 16 horas. Completa, em 24 horas (varia com as condi��es meteorol�gicas).</p>
								<p>4) Rendimento: 100 ml por gal�o de 3,6 litros do verniz a ser tingido.</p>
								<p>5) Cores: Cedro, Imbuia, Mogno e Ip�.</p>
								<p>6) Embalagens: 100ml</p>
							</div>
							<div id="InformacoesAdicionais"> 
								<div id="TintasDisponiveis">
									<span id="TintasDisponiveisTitulo">Tintas Dispon�veis:</span>
									<ul>
										<li>3225 SPARLACK EXTRA MARITIMO INCOLOR 1/4 0.9LT</li>
										<li>3226 SPARLACK EXTRA MARITIMO INCOLOR GL	3.6LT</li>
									</ul>
								</div>							
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>