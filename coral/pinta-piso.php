<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Coral Pinta Piso | A Casa da Pintura</title>
	<meta name="Description" content="Coral Pinta Piso: Pinta Piso possui em sua f�rmula uma tecnologia em resinas, que o torna 60% mais resistente que a f�rmula da tinta anterior" />
	<meta name="Keywords" content="Tintas Coral pinta piso A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="http://www.belatintas.com.br/tinta-p-piso-preto-coralpiso-18lt.html" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Tinta Pinta Piso" alt="Tinta Pinta Piso" src="../slices/tintas-coral/img-pinta-piso.jpg" />
						</div>
						<h2>Pinta Piso</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Pinta Piso possui em sua f�rmula uma tecnologia em resinas, que o torna 60% mais resistente que a f�rmula da tinta anterior. Pode ser aplicada em �reas onde h� grande circula��o, pois � resistente ao tr�fego.</p>
							<p>Com o novo Pinta Piso, locais como estacionamentos, garagens, pisos comerciais, quadras poliesportivas, varandas, cal�adas, e outras �reas de concreto r�stico, estar�o sempre protegidos da a��o do sol e da chuva e tamb�m dos desgastes causados por atritos.</p>
							<p>Al�m disso possui bom rendimento, �tima cobertura e f�cil de aplicar.</p>
						</div>
						<div id="InformacoesAdicionais">
							<div id="Detalhes">
								<span id="Detalhe">Detalhes</span>
								<ul>
									<li><b>Embalagem/Rendimento</b></li>
									<li>Lata <b>18 L</b>: 100 a 175 m� por dem�o</li>
									<li>Gal�o <b>3,6 L</b>: 20 a 35 m� por dem�o</li>
								</ul>
								<ul>
									<li><b>Aplica��o</b></li>
									<li>Rolo de l� ou pincel. Limpe as ferramentas com �gua e sab�o.</li>
									<li><b>Dilui��o</b></li>
									<li>Para superf�cies n�o pintadas diluir com 30% de �gua pot�vel. Para superf�cies j� pintadas, diluir com 20% de �gua pot�vel.</li>
									<li><b>Acabamento:</b> Fosco</li>
									<li><b>Secagem: </b>Ao toque: 30 min, entre dem�os: 4 horas, final: 4 horas, para o tr�fego de pessoas, aguardar a secagem de 48 horas e tr�fego de ve�culo, 72 horas</li>
								</ul>
							</div> 
							<div id="TintasDisponiveis">
								<span id="TintasDisponiveisTitulo">Tintas Dispon�veis</span>
								<ul>
									<li>720	CORALPISO AMARELO DEMARCACAO 18LT</li>		
									<li>719	CORALPISO AMARELO DEMARCACAO 3.6LT</li>	
									<li>722	CORALPISO AZUL 18LT</li>
									<li>721	CORALPISO AZUL 3.6LT</li>
									<li>724	CORALPISO BRANCO 18LT</li>
									<li>723	CORALPISO BRANCO 3.6LT</li>
									<li>728	CORALPISO CINZA ESCURO 18LT</li>
									<li>727	CORALPISO CINZA ESCURO 3.6LT</li>
									<li>726	CORALPISO CINZA MEDIO 18LT</li>
									<li>725	CORALPISO CINZA MEDIO 3.6LT</li>
									<li>730	CORALPISO CONCRETO 18LT</li>
									<li>729	CORALPISO CONCRETO 3.6LT</li>
									<li>732	CORALPISO PRETO 18LT</li>
									<li>731	CORALPISO PRETO 3.6LT</li>
									<li>734	CORALPISO VERDE 18LT</li>
									<li>733	CORALPISO VERDE 3.6LT</li>
									<li>736	CORALPISO VERMELHO 18LT</li>
									<li>735	CORALPISO VERMELHO 3.6LT</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>