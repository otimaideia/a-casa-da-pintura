<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Colorgin Silicone | Spray A Casa da Pintura</title>
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Colorgin Silicone" alt="Colorgin Silicone" src="../slices/spray-colorgin/img-colorgin-silicone.jpg" />
						</div>
						<h2>Colorgin Plasticos</h2>
						<div id="InformacoesProduto">
							<span>Descri��o do produto</span>
							<p><b>Colorgin Silicone </b>�  um produto de alta qualidade que lubrifica, impermeabiliza e repele a umidade.  Lubrifica partes de atrito, reduzindo barulhos e rangidos. Evita a ader�ncia de  tintas, colas e produtos graxosos. N�o � gorduroso, n�o mancha, nem ataca  pinturas.</p>	
							<p><b>Indicada para: </b>instrumentos de precis�o, armas, equipamentos  fotogr�ficos e �pticos, carburadores, conserva��o de partes pl�sticas do carro  e embelezamento dos pneus, ideal tamb�m como desmoldante para moldes de  ind�strias de pl�sticos injetados e cal�ados.</p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<p><b>Preparo da Superf�cie/  Aplica��o:</b></p>
								<ul>
									<li>Elimine  a poeira, gordura ou qualquer contaminante.</li>
									<li>Aplique  a uma dist�ncia de 25 cm  da superf�cie.</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>