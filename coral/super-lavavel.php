<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Tintas Coral Super Lavavel | A Casa da Pintura</title>
    <meta name="Description" content="Coral Super Lavavel: uma tinta acr�lica especialmente formulada para que voc� limpe facilmente manchas de caf�, chocolate, gordura, ketchup e mostarda" />
    <meta name="Keywords" content="Tintas Coral super lavavel A Casa da Pintura" />
    <meta name="Author" content="Wender S. Souza" />
    <meta name="Robots" content="index, follow" />
    <meta name="revisit-after" content="1 day" />
    <? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
    <div id="Pagina">
        <div id="Linha1">
            <div id="ConteudoLinha1">
                <? include "../componentes/topo.php"; ?>
            </div>
        </div>
        <div id="Linha2">
            <div id="ConteudoLinha2">
                <div id="ConteudoProdutos">
                    <div id="Informacoes">
                        <a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
                        <div id="ImagemProduto">
                         <img title="Tinta Super Lav�vel" alt="Tinta Super Lav�vel" src="../slices/tintas-coral/img-super-lavavel.jpg" />
                     </div>
                     <h2>Super Lav�vel</h2>
                     <div id="InformacoesProduto">
                        <span class="Titulo">Descri��o do produto</span>    
                        <p>Quem tem crian�a ou cachorro em casa sabe que n�o � f�cil manter paredes ou muros limpos. Foi por isso que a Coral desenvolveu a Super Lav�vel, uma tinta acr�lica especialmente formulada para que voc� limpe facilmente manchas de caf�, chocolate, gordura, ketchup, mostarda, batom, grafite, l�pis de cor e solado de sapato. Al�m disso, Coral Super Lav�vel tem zero odor*, � f�cil de aplicar e rende bem, dando excelente cobertura e acabamento acetinado. Indicada para ambientes externos e internos.</p>
                        <p>Super Lav�vel faz parte da nova linha de produtos Zero Odor da Coral. Conhe�a os outros produtos da linha: Decora Cores, Decora Brancos, Decora Neutros, Decora Acabamento Acetinado, Coralmur, Coralit Zero e Fundo Preparador Coralit Zero.</p>
                    </div>
                    <div id="InformacoesAdicionais">
                      <div id="Detalhes">
                         <span id="Detalhe">Detalhes</span>                         
                         <ul>
                            <li><b>Embalagem/Rendimento</b></li>
                            <li>Lata <b>18 L</b>: 200 a 300 m� por dem�o</li>
                            <li>Gal�o <b>3,6 L</b>: 40 a 60 m� por dem�o</li>
                            <li>Quarto* <b>0,8 L</b>: 9 a 14 m� por dem�o</li>
                        </ul>
                        <ul>
                            <li><b>Aplica��o</b></li>
                            <li>Rolo de l� de p�lo baixo ou pincel de cerdas macias. Limpe as ferramentas com �gua e sab�o.</li>
                            <li><b>Dilui��o</b></li>
                            <li>Superf�cies n�o seladas: diluir a 1� dem�o em at� 50% e as demais 10 a 30% com �gua pot�vel. Superf�cies j� seladas: diluir todas as dem�os de 10 a 30% com �gua pot�vel.</li>
                            <li><b>Acabamento:</b> Acetinado</li>
                            <li><b>Secagem: </b>Ao toque: 1 hora, entre dem�os: 4 horas, final: 4 horas</li>
                        </ul>
                    </div> 
                    <div id="TintasDisponiveis">
                        <span id="TintasDisponiveisTitulo">Tintas Dispon�veis:</span>
                        <ul>
                            <li>810 SUPER LAVAVEL BRANCO 18LT (CORALPLUS)</li>      
                            <li>809 SUPER LAVAVEL BRANCO 3.6LT (CORALPLUS)</li> 
                        </ul>
                    </div>
                </div>
            </div>
            <? include "../componentes/solicitar-orcamento.php"; ?>
            <? include "../componentes/outros-produtos.php"; ?>
        </div>
    </div>
</div>
<div id="Linha3">
 <? include "../componentes/rodape-tintas.php"; ?>
</div>
</div>
<div id="mask"></div>
</body>
</html>