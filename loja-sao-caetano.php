<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Loja S�o Caetano | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas </title>
<? include "componentes/includes.php"; ?>
</head>
<body>
<div id="Pagina">
	<div id="Linha1">
		<div id="ConteudoLinha1">
			<? include "componentes/topo.php"; ?>
		</div>
	</div>	
	<div id="Linha2">
		<div id="ConteudoLinha2">
			<div id="Lojas">
				<h2>Loja de Tintas S�o Caetano</h2>
				<p>Rua Alegre, 1.163 � Barcelona - S�o Caetano</p>
				<p>Telefone: (11) 3565-3335</p>
				<div id="MapaLoja">
					<iframe width="980" height="420" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=A+Casa+da+Pintura+Rua+Alegre,+1163+-+Barcelona,+S%C3%A3o+Caetano+do+Sul+-+SP,+09550-250&amp;aq=&amp;sll=-23.658375,-46.566008&amp;sspn=0.085221,0.169086&amp;ie=UTF8&amp;hq=A+Casa+da+Pintura&amp;hnear=Rua+Alegre,+1163+-+Santa+Paula,+S%C3%A3o+Paulo,+09550-250&amp;t=m&amp;cid=8562674292693891677&amp;ll=-23.615272,-46.546326&amp;spn=0.03303,0.084028&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
				</div>
			</div>
		</div>
	</div>		
	<div id="Linha3">
		<? include "componentes/rodape.php"; ?>
	</div>	
</div>
<div id="mask"></div>
</body>
</html>