<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /><title>Tintas Industriais | Advance, Ecoclinic, Adepoxi, Primer, Ecomar</title>
	<meta name="description" content="Tintas industriais em geral você encontra na Casa da Pintura! Alta temperatura, anticorrosiva, Advance, Ecoclinic, Adepoxi"/>
	<meta name="keywords" content="advance, ecoclinic, tintas, tintas industriais, anticorrosiva, tinta para ferro, tinta marítima, verniz, esmalte, atoxica, tinta para piso" />
	<? include "componentes/includes.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<h2>Tintas Industriais - <a href="/contato.php" title="Fa&ccedil;a seu or&ccedil;amento">Fa&ccedil;a seu or&ccedil;amento</a></h2>
					<ul>
						<li>
							<a title="Advance Tintas" href="advance-tintas.php"><img src="slices/img-advance-tintas.jpg" title="Advance Tintas" alt="Advance Tintas" /></a>
							<a title="Advance Tintas" href="advance-tintas.php">Advance Tintas</a>
						</li>
						<li>
							<a href="maquina-advance-color.php" title="Desenvolvemos Cores"><img src="slices/img-maquina-advance.jpg" title="Desenvolvemos Cores" alt="Desenvolvemos Cores" /></a>
							<a title="Desenvolvemos Cores" href="maquina-advance-color.php">Desenvolvemos Cores</a>
						</li>
					</ul>
					<h2 id="AdvanceTintas">Advance Tintas</h2>
					<h2>Base Água - Linha Ecocl&iacute;nic</h2>
					<ul>
						<li>
							<a href="advance/ecoclinic-acrilico.php" title="Tinta Advance Ecoclinic Acr&iacute;lico" alt="Tinta Advance Ecoclinic Acr&iacute;lico"><img src="slices/tintas-advance/img-ecoclinic-acrilico.jpg" alt="Tinta Advance Ecoclinic Acr&iacute;lico" title="Tinta Advance Ecoclinic Acr&iacute;lico"></a>
							<h3><a href="advance/esmalte-altobrilho-coralit.php" title="Tinta Advance Ecoclinic Acr&iacute;lico">Ecoclinic Acr&iacute;lico</a></h3>
						</li>
						<li>
							<a href="advance/ecomar.php" title="Tinta Advance Ecomar"><img src="slices/tintas-advance/img-ecoclinic-acrilico.jpg" alt="Tinta Advance Ecomar" title="Tinta Advance Ecomar"></a>
							<h3><a href="advance/ecomar.php" title="Tinta Advance Ecomar">Ecomar</a></h3>
						</li>		
						<li>
							<a href="advance/ecopoxi-828.php" title="Tinta Advance Ecopoxi 828 "><img src="slices/tintas-advance/img-ecopoxi.jpg" alt="Tinta Advance Ecopoxi 828 " title="Tinta Advance Ecopoxi 828 "></a>
							<h3><a href="advance/ecopoxi-828.php" title="Tinta Advance Ecopoxi 828 ">Ecopoxi 828</a></h3>
						</li>	
						<li>
							<a href="advance/ecopoxi-841-primer.php" title="Ecopoxi 841 Primer"><img src="slices/tintas-advance/img-ecopoxi-841-primer.jpg" alt="Ecopoxi 841 Primer" title="Ecopoxi 841 Primer"></a>
							<h3><a href="advance/ecopoxi-841-primer.php" title="Tinta Advance Ecopoxi 828 ">Ecopoxi 841 Primer</a></h3>
						</li>				
					</ul>
					<h2>Base Solvente - Linha Epoxi</h2>
					<ul>
						<li>
							<a href="advance/96-acabamento-brilhante.php" title="96 Acab. Brilhante" alt="96 Acab. Brilhante"><img src="slices/tintas-advance/img-epoxi-acabamento-brilhante.jpg" alt="96 Acab. Brilhante" title="96 Acab. Brilhante"></a>
							<h3><a href="advance/96-acabamento-brilhante.php" title="96 Acab. Brilhante">96 Acab. Brilhante</a></h3>
						</li>
						<li>
							<a href="advance/adepoxi-86df.php" title="Adepoxi 86 Df" alt="Adepoxi 86 Df"><img src="slices/tintas-advance/img-epoxi-acabamento-brilhante.jpg" alt="Adepoxi 86 Df" title="Adepoxi 86 Df"></a>
							<h3><a href="advance/adepoxi-86df.php" title="Adepoxi 86 Df">Adepoxi 86 Df</a></h3>
						</li>
						<li>
							<a href="advance/adepoxi-80df.php" title="Adepoxi 80 Df" alt="Adepoxi 80 Df"><img src="slices/tintas-advance/img-adepoxi-80.jpg" alt="Adepoxi 80 Df" title="Adepoxi 80 Df"></a>
							<h3><a href="advance/adepoxi-80df.php" title="Adepoxi 80 Df">Adepoxi 80 Df</a></h3>
						</li>
						<li>
							<a href="advance/70-tl-primer.php" title="70 TL Primer" alt="70 TL Primer"><img src="slices/tintas-advance/img-70-tl-primer.jpg" alt="70 TL Primer" title="70 TL Primer"></a>
							<h3><a href="advance/70-tl-primer.php" title="70 TL Primer">70 TL Primer</a></h3>
						</li>
						<li class="NoMargin">
							<a href="advance/122-primer.php" title="122 Primer" alt="122 Primer"><img src="slices/tintas-advance/img-122-primer.jpg" alt="122 Primer" title="122 Primer"></a>
							<h3><a href="advance/122-primer.php" title="122 Primer">122 Primer</a></h3>
						</li>
						<li>
							<a href="advance/870-primer-fosco.php" title="870 Primer Fosco" alt="870 Primer Fosco"><img src="slices/tintas-advance/img-870-primer-fosco.jpg" alt="870 Primer Fosco" title="122 Primer"></a>
							<h3><a href="advance/870-primer-fosco.php" title="122 Primer">870 Primer Fosco</a></h3>
						</li>
						<li>
							<a href="advance/878-primer-acetinado.php" title="878 Primer Acetinado" alt="878 Primer Acetinado"><img src="slices/tintas-advance/img-870-primer-fosco.jpg" alt="878 Primer Acetinado" title="878 Primer Acetinado"></a>
							<h3><a href="advance/878-primer-acetinado.php" title="878 Primer Acetinado">878 Primer Acetinado</a></h3>
						</li>
						<li>
							<a href="advance/70-tl-acabamento.php" title="70 TL Acabamento" alt="70 TL Acabamento"><img src="slices/tintas-advance/img-70-tl-primer.jpg" alt="70 TL Acabamento" title="70 TL Acabamento"></a>
							<h3><a href="advance/70-tl-acabamento.php" title="70 TL Acabamento">70 TL Acabamento</a></h3>
						</li>					
					</ul>
					<h2>Base Solvente - Alqu&iacute;dico</h2>
					<ul>
						<li>
							<a href="advance/503-adlux-df-brilhante-extra-rapido.php" title="503 Adlux DF" alt="503 Adlux DF"><img src="slices/tintas-advance/img-530-adlux-df-brilhante.jpg" alt="503 Adlux DF" title="503 Adlux DF"></a>
							<h3><a href="advance/503-adlux-df-brilhante-extra-rapido.php" title="503 Adlux DF">503 Adlux DF  </a></h3>
						</li>
						<li>
							<a href="advance/601-adlux-df-semibrilho-extra-rapido.php" title="601 Adlux DF" alt="601 Adlux DF"><img src="slices/tintas-advance/img-601-adlux.jpg" alt="601 Adlux DF" title="601 Adlux DF"></a>
							<h3><a href="advance/601-adlux-df-semibrilho-extra-rapido.php" title="601 Adlux DF">601 Adlux DF</a></h3>
						</li>
						<li>
							<a href="advance/519-adlux-primier-fosco.php" title="519 Adlux P Fosco" alt="519 Adlux P Fosco "><img src="slices/tintas-advance/img-519-adlux-p-fosco.jpg" alt="601 Adlux DF" title="519 Adlux P Fosco"></a>
							<h3><a href="advance/519-adlux-primier-fosco.php" title="519 Adlux P Fosco">519 Adlux P Fosco</a></h3>
						</li>
						<li>
							<a href="advance/630-adlux-acabamento-brilhante.php" title="630 Adlux Acabamento Brilhante" alt="630 Adlux Acabamento Brilhante"><img src="slices/tintas-advance/img-630-adlux.jpg" alt="630 Adlux Acabamento Brilhante" title="630 Adlux Acabamento Brilhante"></a>
							<h3><a href="advance/630-adlux-acabamento-brilhante.php" title="630 Adlux Acabamento Brilhante">630 Adlux Brilhante</a></h3>
						</li>
					</ul>
					<h2>Base Solvente - Poliuretano</h2>
					<ul>
						<li>
							<a href="advance/772-DF-sem-brilho.php" title="772 DF Sem Brilho" alt="772 DF Sem Brilho"><img src="slices/tintas-advance/img-772-df-sem-brilho.jpg" alt="772 DF Sem Brilho" title="772 DF Sem Brilho"></a>
							<h3><a href="advance/772-DF-sem-brilho.php" title="772 DF Sem Brilho">772 DF Sem Brilho</a></h3>
						</li>
						<li>
							<a href="advance/adpoly-7990-acabamento.php" title="7AdPoly 7990 Acabamento" alt="AdPoly 7990 Acabamento"><img src="slices/tintas-advance/img-adpoly-7990.jpg" alt="AdPoly 7990 Acabamento" title="AdPoly 7990 Acabamento"></a>
							<h3><a href="advance/adpoly-7990-acabamento.php" title="AdPoly 7990 Acabamento">AdPoly 7990 Acabamento</a></h3>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "componentes/rodape.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>