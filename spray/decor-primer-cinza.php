<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Decor Primer Cinza | Spray A Casa da Pintura</title>
	<meta name="Description" content="Colorgin Pl�sticos � uma tinta em spray desenvolvida para pintura de pl�sticos, PVC, acr�lico, cer�mica, telha, madeira, metal, vime e outros tipos de superf�cies lisas" />
	<meta name="Keywords" content="tintas spray colorgin uso geral" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Decor Spray Primer Cinza" alt="Decor Spray Primer Cinza" src="../slices/spray-colorgin/img-colorgin-decor-spray-primer.jpg" />
						</div>
						<h2>Decor Spray Primer Cinza</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p class="detalhes"><b>Decor Spray Primer Cinza </b>� um fundo preparador em spray para  superf�cies met�licas sem oxida��o.</p>
							<p class="detalhes"><b>Indicada  para:</b> metal sem oxida��o.</p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<p class="detalhes"><b>Rendimento Aproximado: </b>de 1,6 a 2,1 m� por embalagem por dem�o.</p>
								<p class="detalhes"><b>Preparo da Superf�cie/ Aplica��o:</b> </p>
								<ul>
									<li>Elimine a poeira, gordura ou qualquer contaminante;</li>
									<li>Aplique de 2 a 3 dem�os de Colorgin       Decor Spray Primer Cinza;</li>
									<li>Aguarde secagem e lixe com lixa d��gua 400 at� a superf�cie ficar lisa e aplique a       cor desejada de Colorgin Decor Spray;</li>
									<li>Ao final do uso, limpe a v�lvula virando a lata para baixo e pressione at� que saia apenas g�s.</li>
								</ul>
								<p class="detalhes"><b>Secagem:</b></p>
								<p class="detalhes"> Entre dem�os: de 2 a 3 minutos;</p>
								<p class="detalhes">Ao toque: 30 minutos;</p>
								<p class="detalhes">Total: 24 horas;</p>
								<p class="detalhes">*Para testes mec�nicos recomenda-se aguardar 72 horas</p>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>