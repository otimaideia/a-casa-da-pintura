<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Colorgin Plasticos  | Spray A Casa da Pintura</title>
	<meta name="Description" content="Colorgin Pl�sticos � uma tinta em spray desenvolvida para pintura de pl�sticos, PVC, acr�lico, cer�mica, telha, madeira, metal, vime e outros tipos de superf�cies lisas" />
	<meta name="Keywords" content="tintas spray colorgin uso geral" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Colorgin Plasticos" alt="Colorgin Plasticos" src="../slices/spray-colorgin/img-colorgin-plasticos.jpg" />
						</div>
						<h2>Colorgin Plasticos</h2>
						<div id="InformacoesProduto">
							<span>Descri��o do produto</span>
							<p><b>Colorgin Pl�sticos</b> � uma tinta em spray desenvolvida  para pintura de pl�sticos, PVC, acr�lico, cer�mica, telha, madeira, metal, vime  e outros tipos de superf�cies lisas. Sua alta tecnologia proporciona total e  permanente ader�ncia, dispensando o uso de primer ou fundo especial. Pode ser  usado em superf�cies externas e internas.</p>
							<p><b>Indicado para:</b> superf�cies de pl�sticos em geral,  PVC, acr�licos, cer�mica, telha, madeira, metal, vime e outros tipos de  superf�cies lisas. </p>	
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<p><b>Rendimento aproximado:</b></p>
								<p>De 1,1 m� a 1,3 m2 por embalagem, por dem�o.</p>
								<p><b>Secagem:</b>
								</p>
								<p> Entre dem�os: 2   a 3 minutos; </p>
								<p>Ao toque: 30 minutos; </p>
								<p>Manuseio: 3 horas; </p>
								<p>Total: 72 horas.</p>
								<p><b>Preparo da Superf�cie/  Aplica��o:</b> </p>
								<ul>
									<li>Elimine  a poeira, gordura ou qualquer contaminante.</li>
									<li>Aplique  de 2 a 3  dem�os de Colorgin Pl�sticos na cor desejada respeitando uma dist�ncia de 25 cm da superf�cie;</li>
									<li>Ao  final do uso, limpe a v�lvula virando a lata para baixo e pressione at� que  saia apenas g�s.</li>
								</ul>

							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>