
<link href="/css/reset.css" type="text/css" media="all" rel="stylesheet" />
<link href="/css/estrutura.css" type="text/css" media="all" rel="stylesheet" />
<link href="/css/tinycarousel.css" type="text/css" media="all" rel="stylesheet" />

<script type="text/javascript" src="//code.jquery.com/jquery-1.7.2.min.js"></script>

<!-- Slider Full Width -->
<script type="text/javascript" src="/js/cycle.js"></script>

<!-- Produtos Destaque -->
<script type="text/javascript" src="/js/jquery.tinycarousel.min.js"></script>

<!-- PLUGIN PARA CARREGAMENTO DE ESTADO CIDADE -->
<script type="text/javascript" src="http://cidades-estados-js.googlecode.com/files/cidades-estados-1.2-utf8.js"></script>

<!-- Geral -->
<script type="text/javascript" src="/js/script.js"></script>

<!-- Jquery Valida��o -->
<script type="text/javascript" src="/js/jquery-validate/dist/jquery.validate.min.js"></script>

<!-- Mascaras -->

<script type="text/javascript" src="/js/jquery.maskedinput.min.js"></script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18586544-1']);
  _gaq.push(['_setDomainName', 'acasadapintura.com.br']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>