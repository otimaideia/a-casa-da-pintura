<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>96 Acamento Brilhante | Advance Tintas</title>
	<meta name="Description" content="96 Acamento Brilhante Advance Tintas: Revestimento ep�xi bicomponente." />
	<meta name="Keywords" content="Tintas advance tintas ecomar A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="96 Acab. Brilhante" alt="96 Acab. Brilhante" src="../slices/tintas-advance/img-epoxi-acabamento-brilhante.jpg" />
						</div>
						<h2>96 Acab. Brilhante</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Revestimento ep�xi bicomponente.</p>
							<p><b>Locais para Aplica��o:</b>  Metais e Pisos</p>
							<p><a target="_blank" title="Dados t�cnicos Ecopoxi 828" href="pdf/acabamento-brilhante.pdf">Dados T�cnicos (PDF)</a></p>
							<span class="Titulo">Mais informa��es sobre a tinta</span>
							<p>Revestimento ep�xi bicomponente, para aplica��o em espessura de 30 a 50   micrometros, de grande resist�ncia a �lcalis, solu��es salinas, �gua   doce e salgada, derivados de petr�leo e tamb�m a abras�o. Atende a Norma   Petrobras N-1198. Tipo I -Poliamina/Tipo II - Poliamida.</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>