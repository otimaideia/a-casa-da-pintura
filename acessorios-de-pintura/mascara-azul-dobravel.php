<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>M&aacute;scara P1 Dobr&aacute;vel Com V&aacute;lvula </title>
	<meta name="Description" content="Acess�rios de Pintura, saiba como comprar a M�scara P1 Dobr�vel Com V�lvula  com Cabo pl�stico anat�mico e resist�nte e outros produtos para pintura na A Casa da Pintura" />
	<meta name="Keywords" content="M�scara P1 Azul Dobr�vel Com V�lvula tintas casa pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Mascara P1 dobr�vel com v�lvula" alt="Mascara P1 dobr�vel com v�lvula" src="../slices/pinceis-e-rolos/img-mascara-azul.jpg" />
						</div>
						<h2>Mascara P1 dobr�vel com v�lvula</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>C.A12500 respirador tipo semifacial filtrante, modelo dobr�vel, com solda ultras�nica em todo per�metro, confeccionada com manta sint�tica com tratamento eletrost�tico, com el�sticos para fixa��o e ajuste � cabe�a do usu�rio.</p>
							<p><b>C�digo: 1275</b></p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<ul>
									<li><b>Refer�ncia:</b> C.0106</li>
									<li><b>Marca:</b> A Casa da Pintura</li>
									<li><b>Unidade:</b> PC</li>
									<li><b>C�d. F�brica:</b> PFF1V</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>