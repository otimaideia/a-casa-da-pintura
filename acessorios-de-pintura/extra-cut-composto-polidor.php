<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Extra Cut 500ml Composto Polidor - 3M | Acess&oacute;rios de Pintura</title>
	<meta name="Description" content="Extra Cut 500ml Composto Polidor - 3M: desenvolvido para ser usado no processo de polimento de repinturas automotivas." />
	<meta name="Keywords" content="extra cut composto polidor 3m" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="EXTRA CUT 500ML Composto Polidor - 3M" alt="EXTRA CUT 500ML Composto Polidor - 3M" src="../slices/pinceis-e-rolos/img-extra-cut-3m.jpg" />
						</div>
						<h2>EXTRA CUT 500ML Composto Polidor - 3M</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>O <b>Composto Polidor Extra Cut</b>, foi desenvolvido para ser usado no processo de  polimento de repinturas automotivas. Tem alto poder de remo��o dos riscos de  lixamento, contamina��es e outros defeitos da camada de pintura, proporcionando  brilho intenso.<br>
								Indicado tamb�m para o polimento em repintura de vernizes de alta dureza.</p>
							</div>
							<div id="InformacoesAdicionais">
								<div id="Detalhes">
									<span id="Detalhe">Detalhes:</span>
									<ul>
										<li><b>Caracter�sticas:</b></li>
										<li>N�o cont�m cera ou silicone</li>
										<li>Produto de consist�ncia l�quida, desenvolvido  para uso profissional.</li>
										<li>Possui solvente � base d��gua</li>
										<li>N�o empasta nas boinas de polimento</li>
										<li>Seus res�duos s�o facilmente limpos com �gua</li>
									</ul>
								</div>
							</div>
						</div>
						<? include "../componentes/solicitar-orcamento.php"; ?>
						<? include "../componentes/outros-produtos.php"; ?>
					</div>
				</div>
			</div>
			<div id="Linha3">
				<? include "../componentes/rodape-tintas.php"; ?>
			</div>
		</div>
		<div id="mask"></div>
	</body>
	</html>