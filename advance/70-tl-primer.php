<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>70 TL Acabamento | Advance Tintas</title>
	<meta name="Description" content="70 TL Primer: Revestimento de componentes" />
	<meta name="Keywords" content="Tintas advance 70tl primer A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="70 TL Primer" alt="70 TL Primer" src="../slices/tintas-advance/img-70-tl-primer.jpg" />
						</div>
						<h2>70 TL Primer</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Revestimento anticorrosivo ep�xi de alto s�lidos de excelente resist�ncia qu�mica e ades�o.</p>
							<p><b>Locais para Aplica��o:</b>  Metais</p>
							<p><a target="_blank" title="Dados t�cnicos 70 TL Primer" href="pdf/70tl-primer.pdf">Dados T�cnicos (PDF)</a></p>
							<span class="Titulo">Mais informa��es sobre a tinta</span>
							<p>Revestimento anticorrosivo ep�xi de alto s�lidos de excelente   resist�ncia qu�mica e ades�o. Especialmente desenvolvido como primer em   sistemas de pintura interna de tanques. Tinta de fundo anticorrosiva   para tanques e equipamentos industriais, em ambientes de alta   agressividade em conjunto com ADEPOXI 70 TL ACABAMENTO.</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>