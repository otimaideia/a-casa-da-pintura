<?php
#### START AUTOCODE
/**
 * Classe generada para a tabela "tb_newsletter"
 * in 2014-02-03
 * @author Hugo Ferreira da Silva
 * @link http://www.hufersil.com.br/lumine
 * @package entidades
 *
 */

class TbNewsletter extends Lumine_Base {

    
    public $id;
    public $nmCliente;
    public $dsEmail;
    
    
    
    /**
     * Inicia os valores da classe
     * @author Hugo Ferreira da Silva
     * @return void
     */
    protected function _initialize()
    {
        $this->metadata()->setTablename('tb_newsletter');
        $this->metadata()->setPackage('entidades');
        
        # nome_do_membro, nome_da_coluna, tipo, comprimento, opcoes
        
        $this->metadata()->addField('id', 'id', 'int', 11, array('primary' => true, 'notnull' => true, 'autoincrement' => true));
        $this->metadata()->addField('nmCliente', 'nm_cliente', 'varchar', 45, array());
        $this->metadata()->addField('dsEmail', 'ds_email', 'varchar', 120, array());

        
    }

    #### END AUTOCODE


}
