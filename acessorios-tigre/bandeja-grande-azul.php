<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Bandeja Grande Azul Tigre | Acess&oacute;rios de Pintura A Casa da Pintura</title>
    <meta name="Description" content="Bandeja Grande Azul Tigre, confira mais detalhes deste acess�rio de pintura" />
    <meta name="Author" content="Wender S. Souza" />
    <meta name="Robots" content="index, follow" />
    <meta name="revisit-after" content="1 day" />
    <? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
    <div id="Pagina">
        <div id="Linha1">
            <div id="ConteudoLinha1">
                <? include "../componentes/topo.php"; ?>
            </div>
        </div>
        <div id="Linha2">
            <div id="ConteudoLinha2">
                <div id="ConteudoProdutos">
                    <div id="Informacoes">
                        <a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
                        <div id="ImagemProduto">
                            <img title="Bandeja Grande Azul" alt="Bandeja Grande Azul" src="../slices/acessorios-tigre/img-bandeja-azul.jpg" />
                        </div>
                        <h2>Bandeja Grande Azul</h2>
                        <div id="InformacoesProduto">
                            <span class="Titulo">Descri��o do produto</span>
                            <p>Acess�rio de Pintura da Marca Tigre.</p>
                            <p>Composi��o: Pl�stico </p>
                            <p>Cor: Preto</p>
                            <p>Formato: Bandeja</p>
                            <p><b>Refer�ncia: 2306</b></p>
                        </div>
                        <div id="InformacoesAdicionais">
                            <div id="Detalhes">
                                <span id="Detalhe">Detalhes:</span>
                                <p>N�o existem maiores detalhes para este produto</p>
                            </div>
                        </div>
                    </div>
                    <? include "../componentes/solicitar-orcamento.php"; ?>
                    <? include "../componentes/outros-produtos.php"; ?>
                </div>
            </div>
        </div>
        <div id="Linha3">
            <? include "../componentes/rodape-tintas.php"; ?>
        </div>
    </div>
    <div id="mask"></div>
</body>
</html>