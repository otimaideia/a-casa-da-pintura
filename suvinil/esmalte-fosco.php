<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title> Suvinil Esmalte Fosco | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas</title>
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Suvinil Esmalte Fosco" alt="Suvinil Esmalte Fosco" src="../slices/tintas-suvinil/img-esmalte_sintetico.jpg" />
						</div>
						<h2>Suvinil Esmalte Fosco</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Indicado para pintura de superf�cies internas de madeira, ferro,   alum�nio e galvanizados, alta resist�ncia ao atrito e �timo acabamento.   Tamb�m � especialmente indicado para pintura de quadros escolares   (lousas).  </p>
							<p><b>Aplica��o</b></p>
							<p>Madeiras e Metais</p>
							<p><b>Local de Aplica��o</b></p>
							<p>Interior</p>
							<p>Superf�cies internas de madeira e metais.</p>
							<p><b>Acabamento</b></p>
							<p>Fosco</p>
						</div>
						<div id="InformacoesAdicionais">
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<ul>
									<li><b>Tintas Dispon�veis</b></li>
									<li>ESMALTE FOSCO BRANCO 3.6LT</li>
									<li>ESMALTE FOSCO PRETO 3.6LT
									</li>
								</ul>
								<ul>
									<li><b>Composi��o:</b></li>
									<li>Resina alqu�dica � base de �leo vegetal   semi-secativo, pigmentos org�nicos e inorg�nicos, cargas minerais   inertes (acetinado e fosco), hidrocarbonetos alif�ticos, secantes   organo-met�licos. N�o cont�m benzeno.</li>
									<li><b>Importante:</b></li>
									<li>Para se obter o m�ximo da qualidade dos produtos Suvinil � necess�rio um bom <b>preparo da superf�cie</b> a ser aplicada..</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>