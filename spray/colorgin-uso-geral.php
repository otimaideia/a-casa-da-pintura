<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Colorgin Uso Geral  | Spray A Casa da Pintura</title>
	<meta name="Description" content="Colorgin Pl�sticos � uma tinta em spray desenvolvida para pintura de pl�sticos, PVC, acr�lico, cer�mica, telha, madeira, metal, vime e outros tipos de superf�cies lisas" />
	<meta name="Keywords" content="tintas spray colorgin uso geral" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Colorgin Uso Geral" alt="Colorgin Uso Geral" src="../slices/spray-colorgin/img-colorgin-uso-geral.jpg" />
						</div>
						<h2>Colorgin Uso Geral</h2>
						<div id="InformacoesProduto">
							<span>Descri��o do produto</span>
							<p><b>Colorgin Uso Geral  Premium </b>� uma tinta  acr�lica em spray indicada para reparos,  pinturas e decora��o de objetos em geral. Possui secagem r�pida, alta resist�ncia,  �timo rendimento e grande variedade de cores.</p>
							<p><b>Indicada para:</b> madeira, papel, gesso, cer�mica,  ferro e alum�nio.</p>

						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<p><b>Rendimento aproximado: </b>de 1,8m� a 2,3m� por embalagem, por  dem�o.</p>
								<p><b>Preparo da superf�cie/  Aplica��o</b></p>
								<ul>
									<li>Elimine  a poeira, gordura ou qualquer contaminante;</li>
									<li>Aplique o fundo certo para cada tipo de superf�cie e  depois aplique de 2 a  3 dem�os  de Colorgin Uso Geral na cor desejada; </li>
									<li>Aplique  o produto em camadas finas, a uma dist�ncia de 25 cm da superf�cie;</li>
								</ul>
								<p>Ao final do uso, limpe a v�lvula virando a lata para  baixo e pressione at� que saia apenas g�s. </p>
								<p><b>Sobre metal sem  oxida��o:</b> aplique COLORGIN PRIMER R�PIDO CINZA ou COLORGIN PRIMER R�PIDO �XIDO. Aguarde secagem e  lixe com lixa d �gua 400 at� a superf�cie ficar homog�nea e lisa;</p>
								<p><b>Sobre metal com oxida��o:</b> lixe bem at� remover a oxida��o. Aplique COLORGIN PRIMER R�PIDO �XIDO. Aguarde secagem e lixe com lixa d`�gua 400 at� a superf�cie ficar homog�nea e lisa;</p>
								<p><b>Sobre metal n�o ferroso (alum�nio galvanizado, cobre, lat�o, prata.):</b> aplique COLORGIN FUNDO PARA ALUM�NIO. Aguarde a secagem e lixe com lixa d`�gua 400 at� a superf�cie ficar homog�nea e lisa.</p>
								<p><b>Secagem:</b></p>
								<p>Entre dem�os: 5 a 10 minutos, ao toque: 30 minutos, Total: 24 horas e Para testes mec�nicos recomenda-se aguardar 72 horas</p>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>