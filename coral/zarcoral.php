<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Coral Zarcoral | A Casa da Pintura</title>
	<meta name="Description" content="Coral Zarcoral: Fundo sint�tico laranja fosco, inibidor de ferrugem em metais ferrosos" />
	<meta name="Keywords" content="Tintas Coral Zarcoral A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Zarcoral" alt="Zarcoral" src="../slices/tintas-coral/img-zarcoral.jpg" />
						</div>
						<h2>Zarcoral</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Fundo sint�tico laranja fosco, inibidor de ferrugem em metais ferrosos. Protege por muito mais tempo a beleza do acabamento final em superf�cies internas e externas de metais. F�cil de aplicar e de lixar, possui excelente rendimento, �tima ader�ncia e elevado poder anticorrosivo. Indicado como fundo para os seguintes produtos: Esmaltes Sint�ticos Coralit e Coralar.</p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes</span>
								
								<ul>
									<li><b>Embalagem/Rendimento</b></li>
									<li>Gal�o <b>3,6 L</b>: 30 a 40 m� por dem�o</li>
									<li>Quarto <b>0,9  L</b>: 7,5 a  10 m�  por dem�o</li>
									<li>Quarto <b>225  ml</b>: 1.8 a  2,5 m2  por dem�o </li>
								</ul>
								<ul><li><b>Aplica��o</b></li>
									<li>�reas grandes: rolo de espuma ou rev�lver. �reas pequenas: pincel com  cerdas macias ou boneca. Limpe as ferramentas com Coralraz. </li>
									<li><b>Dilui��o</b></li>
									<li>Aplica��o pincel/rolo diluir no m�ximo 10% com Coralraz. Aplica��o  rev�lver diluir no m�ximo 30% com Coralraz. </li>
									<li><b>Acabamento</b>: Fosco </li>
									<li><b>Secagem</b></li>
									<li>Ao toque: 4 a  6 horas</li>
									<li>Entre dem�os: 8 horas</li>
									<li>Final: 18 a  24 horas</li>
								</ul>
							</div> 
							<div id="TintasDisponiveis">
								<span id="TintasDisponiveisTitulo">Tintas Dispon�veis</span>
								<ul>
									<li><b>Tintas Dispon�veis</b></li>
									<li>826	ZARCORAL ZARCAO 0.225ML 1/16</li>
									<li>827	ZARCORAL ZARCAO 0.9LT</li>
									<li>829	ZARCORAL ZARCAO 3.6LT</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>