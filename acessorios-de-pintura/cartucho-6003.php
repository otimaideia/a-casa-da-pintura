<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>CARTUCHO 6003 3m | Acess&oacute;rios de Pintura</title>
	<meta name="Description" content="Este cartucho para Vapor Org�nico/G�s �cido � designado para uso com Respirador Semi Facial e Facial Inteiro S�rie 6000 e Respirador Semi Facial e Facial Inteiro S�rie 7000 que tenha encaixe tipo Baioneta para cartuchos" />
	<meta name="Keywords" content="cartucho 6003 casa pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Cartucho 6003 3M (pack c/02)" alt="Cartucho 6003 3M (pack c/02)" src="../slices/pinceis-e-rolos/img-cartucho-6003.jpg" />
						</div>
						<h2>Cartucho 6003 3M (pack c/02)</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Este cartucho para Vapor Org�nico/G�s �cido � designado para uso com Respirador Semi Facial e Facial Inteiro S�rie 6000 e Respirador Semi Facial e Facial Inteiro S�rie 7000 que tenha encaixe tipo Baioneta para cartuchos.</p>
							<p>CA 4115 </p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>