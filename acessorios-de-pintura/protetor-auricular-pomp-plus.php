<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>PROTETOR AURICULAR POMP PLUS 3M</title>
	<meta name="Description" content="Acess�rios de Pintura, saiba como comprar PROTETOR AURICULAR POMP PLUS 3M e outros produtos para pintura na A Casa da Pintura" />
	<meta name="Keywords" content="PROTETOR AURICULAR 3M casa pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Protetor Auricular Pomp Plus" alt="Protetor Auricular espuma com cor" src="../slices/pinceis-e-rolos/img-protetor-auricular.jpg" />
						</div>
						<h2>Protetor Auricular espuma Pomp Plus</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Protetor auditivo do tipo inser��o pr�-moldado, confeccionado em silicone, no formato c�nico com tr�s flanges conc�ntricos, de di�metros vari�veis, contendo um orif�cio em seu interior, que torna o equipamento macio e facilmente adapt�vel ao canal auditivo. O protetor est� dispon�vel em tamanho �nico, com cord�o de silicone ou polipropileno e caixa pl�stica.</p>
							<p>CA 5674</p>								
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>