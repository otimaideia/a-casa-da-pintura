<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /><title>Contato | Tintas e Acess&oacute;rios em Geral | A Casa da Pintura</title>
	<meta name="description" content="Entre em contato, tire suas dúvidas! As melhores tintas você encontra aqui na Casa da Pintura! "/>
	<meta name="keywords" content="onde fica, contato, telefone, a casa da pintura, email, orcamento, fale conosco, tirar duvidas, a casa da pintura, tintas, acessorios para pintura" />
	<? include "componentes/includes.php"; ?>
</head>
<body id="PaginaContato">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "componentes/topo.php"; ?>
			</div>
		</div>	
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoContato">
					<div id="ContatoColuna1">
						<h2>Contato</h2>
						<form id="FormularioContato" action="contato-obrigado.php" method="post">
							<ol>
								<li>
									<select name="assunto" id="assunto" class="Large">
										<option value="">Selecione o assunto</option>
										<option value="Dúvidas/Sugestões">D&uacute;vidas/Sugest&otilde;es</option>
										<option value="Orçamentos" >Or&ccedil;amentos</option>
										<option value="Outros" >Outros</option>
									</select>
								</li>
								<li>
									<input type="text" placeholder="Nome" id="nome" class="Large required" name="nome" />	
								</li>
								<li>
									<input type="text" placeholder="E-mail" id="email" class="Large required" name="email">
								</li>
								<li><input type="text" placeholder="Telefone" id="telefone" class="Large required" name="telefone"></li>
								<li>
									<select id="estado" name="estado" class="Large requerido"></select> 
									<input type="hidden" id="estado-escolhido1" name="estado_escolhido" />
								</li>
								<li>
									<select id="cidade" name="cidade" class="Large requerido">
										<option value="">Selecione uma cidade</option>
									</select>
									<input type="hidden" id="cidade-escolhida1" name="cidade_escolhida"/>
								</li>
								<li>
									<textarea placeholder="Mensagem" name="mensagem" id="mensagem" class="Large required" rows="2" cols="48"></textarea>
								</li>
								<li>
									<input type="checkbox" id="ativar" value="1" name="aceito"/><label style="font-size: 12px; color: #707070;" >Aceito receber informa&ccedil;&#245;es e promo&ccedil;&#245;es da Casa da Pintura</label>
								</li>
								<li id="Enviar">
									<input type="submit" name="enviar" value="Enviar" id="Enviar" />
								</li>
							</ol>
						</form>
					</div>
					<div id="ContatoColuna2">
						<img src="slices/img-a-casa-da-pintura.jpg" title="A Casa da Pintura" alt="A Casa da Pintura" />
					<!-- 
					<h2>Localiza&ccedil;&atilde;o - Centro de Distribui&ccedil;&atilde;o</h2>
					<iframe width="499" height="337" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=A+casa+da+Pintura+CENTRO+DE+DISTRIBUI%C3%87%C3%83O++Av.+Piraporinha,+1111+-+Bairro+Planalto+SBC&amp;aq=&amp;sll=-23.690315,-46.567268&amp;sspn=0.01065,0.021136&amp;ie=UTF8&amp;hq=A+casa+da+Pintura+CENTRO+DE+DISTRIBUI%C3%87%C3%83O++Av.+Piraporinha,+1111+-+Bairro+Planalto+SBC&amp;hnear=&amp;t=m&amp;cid=11663078834149639147&amp;ll=-23.675813,-46.569586&amp;spn=0.02649,0.042915&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
				-->

			</div>	
			<div id="BotaoTrabalheConosco">
				<a href="trabalhe-conosco.php" title="Trabalhe Conosco">Trabalhe Conosco</a>
			</div>			
			<div id="LojasContato">
				<h2>A Casa Da Pintura - Lojas</h2>
				<ul>
					<li><a href="loja-diadema.php" title="Loja de Tintas Diadema">Loja de Tintas Diadema</a></li>
					<li><a href="loja-sao-caetano.php" title="Loja de Tintas S&atilde;o Caetano">Loja de Tintas S&atilde;o Caetano</a></li>
					<li><a href="loja-frei-gaspar.php" title="Loja de Tintas Frei Gaspar">Loja de Tintas Frei Gaspar</a></li>
					<li><a href="loja-maua.php" title="Loja de Tintas Mau&aacute;">Loja de Tintas Mau&aacute;</a></li>
					<li><a href="loja-santo-andre.php" title="Loja de Tintas Santo Andr&eacute;">Loja de Tintas Santo And&eacute;</a></li>
					<li><a href="loja-sao-bernardo-do-campo.php" title="Loja de Tintas S&atilde;o Bernardo do Campo">Loja de Tintas S&atilde;o Bernardo do Campo</a></li>
					<li><a href="loja-automotivo.php" title="Loja de Tintas Automotivo">Loja de Tintas Automotivo - Planalto</a></li>
					<li><a href="centro-de-distribuicao.php" title="Centro de Distribui&ccedil;&atilde;o">Centro de Distribui&ccedil;&atilde;o</a></li>
				</ul>
			</div>			
		</div>
	</div>
</div>		
<div id="Linha3">
	<? include "componentes/rodape.php"; ?>
</div>	
</div>
<div id="mask"></div>
</body>
</html>