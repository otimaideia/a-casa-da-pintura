<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Coral Selador Acr&iacute;lico | A Casa da Pintura</title>
	<meta name="Description" content="Coral Selador Acr�lico: Indicado para paredes novas, o Selador Acr�lico impermeabiliza e uniformiza as mais diversas superf�cies de alvenaria" />
	<meta name="Keywords" content="Tintas Coral Selador Acrilico A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />	
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Produto">
						<div id="Informacoes">
							<a id="FazerPedido" href="http://www.belatintas.com.br/selador-acrilico-coral-galao-galao.html" target="_blank" title="Fazer pedido">Fazer pedido</a>
							<div id="ImagemProduto">
								<img title="Tinta Coral Selador Acr�lico" alt="Tinta Coral Selador Acr�lico" src="../slices/tintas-coral/img-selador-acrilico.jpg" />
							</div>
							<h2>Selador Acr�lico </h2>

							<div id="InformacoesProduto">
								<span class="Titulo">Descri��o do produto</span>
								<p>Indicado para paredes novas, o Selador Acr�lico impermeabiliza e uniformiza as mais diversas superf�cies de alvenaria devido ao seu poder selante e �tima ader�ncia. � um fundo de cor branco fosco, dilu�vel em �gua e de r�pida secagem. Com grande poder de preenchimento e cobertura, pode ser aplicado em ambientes internos e externos e prepara a superf�cie para os demais cuidados que sua parede necessita.</p>
							</div>
							<div id="InformacoesAdicionais"> 
								<div id="Detalhes">
									<span id="Detalhe">Detalhes:</span>									
									<ul>
										<li><b>Embalagem/Rendimento</b></li> 
										<li>Lata 18 L: 75 a 100 m� por dem�o</li>
										<li>Gal�o 3,6 L: 15 a 20 m� por dem�o</li>
										<li><b>Aplica��o</b></li>
										<li>Rolo de l� ou pincel. Limpe as ferramentas com �gua e sab�o. </li>
										<li><b>Dilui��o</b></li>
										<li>Deve ser dilu�do de 5 a 15% com �gua limpa. </li>
										<li><b>Acabamento</b>: Fosco</li>
										<li><b>Secagem</b></li>
										<li>Ao toque: 30 min, entre dem�os: 4 horas, final: 5 horas</li>
									</ul>
								</div>
								<div id="TintasDisponiveis">
									<span id="TintasDisponiveisTitulo">Tintas Dispon�veis</span>
									<ul>
										<li>Selador Acrilico Branco Neve 18LT - COD 745</li>
										<li>Selador Acrilico Branco Neve 3.6LT - COD 746</li>
									</ul>
								</div>							
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>