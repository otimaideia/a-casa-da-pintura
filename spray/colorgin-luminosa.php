<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Colorgin Luminosa  | Spray A Casa da Pintura</title>

	<meta name="Description" content="Colorgin Luminosa � uma tinta acr�lica fosca em spray,�que se evidencia nos ambientes utilizados, proporcionando efeito luminoso com a incid�ncia da luz" />
	<meta name="Keywords" content="tintas spray colorgin" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Colorgin Luminosa" alt="Colorgin Luminosa" src="../slices/spray-colorgin/img-colorgin-luminosa.jpg" />
						</div>
						<h2>Colorgin Luminosa</h2>
						<div id="InformacoesProduto">
							<span>Descri��o do produto</span>
							<p><b>Colorgin Luminosa</b> � uma tinta acr�lica fosca em  spray, que se evidencia nos ambientes  utilizados, proporcionando efeito luminoso com a incid�ncia da luz. � indicada  somente para uso interno.</p>
							<p><b>Indicada para:</b> decora��o de vitrines, sal�es,  artesanatos e uso escolar. Pode ser usada em superf�cies de madeira, ferro,  gesso, papel, vidro, isopor e em folhagens.</p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<p><b>Rendimento aproximado:</b></p>
								<p>1,2m� a 1,4m� por embalagem, por dem�o.</p>
								<p><b>Prepara��o da  superf�cie/ Aplica��o:</b> </p>
								<ul>
									<li>Elimine  a poeira, gordura ou qualquer contaminante;</li>
									<li>Aplique  de 2 a 3  dem�os do produto na cor desejada a uma dist�ncia de 25 cm da superf�cie;</li>
								</ul>	
								<p> Ao final do uso, limpe a v�lvula virando a lata para  baixo e pressione at� que saia apenas g�s.</p>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>