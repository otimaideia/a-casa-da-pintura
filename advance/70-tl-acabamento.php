<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>70 TL Acabamento | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas</title>
	<meta name="Description" content="70 TL Acabamento: Acabamento ep�xi de alto s�lidos de excelente resist�ncia qu�mica e � abras�o." />
	<meta name="Keywords" content="Tintas advance 70tl acabamento A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="70 TL Acabamento" alt="770 TL Acabamento" src="../slices/tintas-advance/img-70-tl-primer.jpg" />
						</div>
						<h2>70 TL Acabamento</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Acabamento ep�xi de alto s�lidos de excelente resist�ncia qu�mica e � abras�o</p>
							<p><b>Locais para Aplica��o:</b>  Metais</p>
							<p><a target="_blank" title="Dados t�cnicos 70 TL Acabamento" href="pdf/70tl-acabamento.pdf">Dados T�cnicos (PDF)</a></p>
							<span class="Titulo">Mais informa��es sobre a tinta</span>
							<p>Acabamento ep�xi de alto s�lidos de excelente resist�ncia qu�mica e �   abras�o. Especialmente desenvolvido como acabamento em sistemas de   pintura interna de tanques em conjunto com ADEPOXI 70 TL PRIMER.   Indicado, sob recomenda��o t�cnica, para contato direto com produtos   aliment�cios, conforme Portaria n� 26/96 da Vigil�ncia Sanit�ria do   Minist�rio da Sa�de. Utilizado em um sistema com ADEPOXI 70 TL PRIMER em   espessura final de 350 a 400 micrometros. Produto idealizado para   utiliza��o em cervejarias, ind�strias farmac�uticas, refrigera��es,   aliment�cias, latic�nios, papel e celulose, etc. Quando utilizado na   prote��o de estruturas ou tanques de concreto, recomendamos a   substitui��o do primer por uma dem�o de ADEPOXI 38 ou 138 HS VERNIZ,   para uma melhor performance do sistema.</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>