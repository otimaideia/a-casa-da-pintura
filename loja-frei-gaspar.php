<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Loja Frei Gaspar | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas </title>
<? include "componentes/includes.php"; ?>
</head>
<body>
<div id="Pagina">
	<div id="Linha1">
		<div id="ConteudoLinha1">
			<? include "componentes/topo.php"; ?>
		</div>
	</div>	
	<div id="Linha2">
		<div id="ConteudoLinha2">
			<div id="Lojas">
				<h2>Loja de Tintas Frei Gaspar</h2>
				<p>Rua Frei Gaspar, 283 - Centro - SBC</p>
				<p>Telefone: (11) 4339-3232</p>
				<div id="MapaLoja">
					<iframe width="980" height="420" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=A+casa+da+pintura+frei+gaspar&amp;aq=&amp;sll=-23.790019,-46.477012&amp;sspn=0.681073,1.352692&amp;ie=UTF8&amp;hq=A+casa+da+pintura+frei+gaspar&amp;hnear=&amp;t=m&amp;cid=11801491348164010317&amp;ll=-23.542586,-46.51886&amp;spn=0.528771,1.344452&amp;z=10&amp;iwloc=A&amp;output=embed"></iframe>
				</div>
			</div>
		</div>
	</div>		
	<div id="Linha3">
		<? include "componentes/rodape.php"; ?>
	</div>	
</div>
<div id="mask"></div>
</body>
</html>