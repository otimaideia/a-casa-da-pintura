<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title> Suvinil Acr�lico Premium Semibrilho | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas</title>
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Suvinil Acr�lico Premium Semibrilho" alt="Suvinil Acr�lico Premium Semibrilho" src="../slices/tintas-suvinil/img-acrilico-premium-semibrilho.jpg" />
						</div>
						<h2>Suvinil Acr�lico Premium Semibrilho</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span><p>� uma tinta de acabamento semibrilho de f�cil aplica��o, secagem r�pida e   baixo odor. Devido a sua intensidade de brilho, proporciona alta   impermeabilidade quando aplicada em ambientes externos e em superf�cies   internas oferece grande facilidade de limpeza.  </p>
							<p><b>Local de Aplica��o</b></p>
							<p><span>Exterior / Interior  Reboco, massa acr�lica,   texturas, concreto, fibrocimento e superf�cies internas de massa   corrida, gesso e repintura sobre tintas PVA ou Acr�licas</span></p>
							<p><b>Acabamento</b></p>
							<p><span>Semi-Brilho</span></p>

						</div>
						<div id="InformacoesAdicionais">
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<ul>
									<li><b>Tintas Dispon�veis</b></li>
									<li>ACRILICO SEMI-BRILHO BRANCO 18LT</li>
									<li>ACRILICO SEMI-BRILHO BRANCO 3.6LT</li>
								</ul>
								<ul>
									<li><b>Composi��o:</b></li>
									<li><span>Resina � base de dispers�o aquosa de copol�mero   estireno acr�lico, pigmentos isentos de metais pesados, cargas minerais   inertes, hidrocarbonetos alif�ticos, glic�is e tensoativos etoxilados e   carboxilados.</span>.</li>
									<li><b>Observa��o:</b></li>
									<li>Sem Cheiro / Muito Menos Respingos</span> </li>
									<li><b>Importante:</b></li>
									<li>Para se obter o m�ximo da qualidade dos produtos Suvinil � necess�rio um bom <b>preparo da superf�cie</b> a ser aplicada.</li>
								</ul>	
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>