<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Lojas de tintas | Endere�os A Casa da Pintura</title>
	<meta name="Description" content="Lojas de Tintas, confira todos os endere�os da Casa da Pintura em Diadema, S�o Bernardo do Campo, Rua Frei Gaspar, Av. Alvaro Guimar�es e Centro de Distribui��o em Piraporinha." />
	<meta name="Keywords" content="Tintas acessorios de pintura casa pintura " />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "componentes/includes.php"; ?>
</head>
<body>
	<div id="dialog" class="window">
		<a href="#" title="Fechar" alt="Fechar" class="close"><img title="Fechar" alt="Fechar" src="slices/img-Close.png" /></a>
		<h2>Loja Diadema</h2>
		<p>Tel: (11) 3201-5100</p>
		<iframe width="980" height="420" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=Av.+Fabio+Eduardo+R.+Esquivel,+753+-+Centro+-+Diadema&amp;aq=&amp;sll=-22.546141,-48.635523&amp;sspn=9.337056,16.907959&amp;ie=UTF8&amp;hq=&amp;hnear=Avenida+F%C3%A1bio+Eduardo+Ramos+Esquivel,+753+-+Centro,+S%C3%A3o+Paulo,+09920-575&amp;t=m&amp;ll=-23.677975,-46.611729&amp;spn=0.016507,0.042014&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe>
	</div>
	<div id="dialog" class="window">
		<a href="#" title="Fechar" alt="Fechar" class="close"><img title="Fechar" alt="Fechar" src="slices/img-Close.png" /></a>
		<h2>Loja Diadema</h2>
		<p>Tel: (11) 3201-5100</p>
		<iframe width="980" height="420" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=Av.+Fabio+Eduardo+R.+Esquivel,+753+-+Centro+-+Diadema&amp;aq=&amp;sll=-22.546141,-48.635523&amp;sspn=9.337056,16.907959&amp;ie=UTF8&amp;hq=&amp;hnear=Avenida+F%C3%A1bio+Eduardo+Ramos+Esquivel,+753+-+Centro,+S%C3%A3o+Paulo,+09920-575&amp;t=m&amp;ll=-23.677975,-46.611729&amp;spn=0.016507,0.042014&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe>
	</div>	
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "componentes/topo.php"; ?>
			</div>
		</div>	
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="Lojas">
					<h2>Lojas</h2>
					<ul>
						<li>
							<img src="slices/lojas/img-loja-diadema.jpg" title="Loja de tinta Diadema" alt="Loja de tintas Diadema"/>
							<h3>Diadema</h3>
							<p>Av. Fabio Eduardo R. Esquivel, 753 - Centro - Diadema - Tel. 11 3201-5100</p>
							<a href="loja-diadema.php" title="Saiba mais sobre a Loja de tintas Diadema">Saiba mais</a>						
						</li>
						<li>
							<img src="slices/lojas/img-loja-santo-andre.jpg" title="Loja de tinta Santo Andr�" alt="Loja de tintas Santo Andr�"/>
							<h3>Santo Andr�</h3>
							<p>Av. Itamarati, 1157 - Pq. Ja�atuba - Santo Andr� - Telefone: 11 4436-6666</p>
							<a href="loja-santo-andre.php" title="Saiba mais sobre a Loja de tintas Diadema">Saiba mais</a>
						</li>
						<li>
							<img src="slices/lojas/img-loja-sao-caetano.jpg" title="Loja de tinta S�o Caetano" alt="Loja de tintas S�oS�o Caetano"/>
							<h3>S�o Caetano</h3>
							<p>Rua Alegre, 1.163 - Barcelona - S�o Caetano - Telefone: 11 3565-3335</p>
							<a href="loja-sao-caetano.php" title="Saiba mais sobre a Loja de tintas Diadema">Saiba mais</a>
						</li>
						<li>
							<img src="slices/lojas/img-loja-sao-bernardo-do-campo.jpg" title="Loja de tinta S�o Bernardo do Campo" alt="Loja de tintas S�o Bernardo do Campo"/>
							<h3>S�o Bernardo do Campo</h3>
							<p>Av. Pereira Barreto, 646 - Centro - SBC - Tel. 11 4121-9292</p>
							<a href="loja-sao-bernardo-do-campo.php" title="Saiba mais sobre a Loja de tintas Diadema">Saiba mais</a>
						</li>
						<li>
							<img src="slices/lojas/img-loja-frei-gaspar.jpg" title="Loja tinta na Frei Gaspar" alt="Loja de tinta na Frei Gaspar"/>
							<h3>Frei Gaspar</h3>
							<p>Rua Frei Gaspar, 283 - Centro - SBC - Tel. 11 4339-3232</p>
							<a href="loja-frei-gaspar.php" title="Saiba mais sobre a Loja de tintas Diadema">Saiba mais</a>
						</li>
						<li>
							<img src="slices/lojas/img-loja-automotivo.jpg" title="Loja de Tintas Automotivas" alt="Loja de Tintas Automotivas"/>
							<h3>Automotivo</h3>
							<p>Av. Alvaro Guimar�es, 128 - Tel. 11 2381-4666</p>
							<a href="loja-automotivo.php" title="Saiba mais sobre a Loja de tintas Diadema">Saiba mais</a>
						</li>
						<li class="NoMargin">
							<img src="slices/lojas/img-loja-maua.jpg" title="Loja de tinta Mau�" alt="Loja de tinta Mau�"/>
							<h3>Mau�</h3>
							<p>Av. Dr. Getulio Vargas, 100 - Vl. Guarani</p>
							<a href="loja-maua.php" title="Saiba mais sobre a Loja de tintas Diadema">Saiba mais</a>
						</li>
						<li class="NoMargin">
							<img src="slices/lojas/img-centro-distribuicao.jpg" title="Centro de Distribui��o" alt="Centro de Distribui��o" />
							<h3>Centro de Distribui��o</h3>
							<p>Av. Dr. Getulio Vargas, 100 - Vl. Guarani</p>
							<a href="centro-de-distribuicao.php" title="Saiba mais sobre a Loja de tintas Diadema">Saiba mais</a>
						</li>
					</ul>
				</div>
			</div>
		</div>		
		<div id="Linha3">
			<? include "componentes/rodape.php"; ?>
		</div>	
	</div>
	<div id="mask"></div>
</body>
</html>