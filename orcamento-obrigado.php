<?php include 'lumine/config.php'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=charset=iso-8859-1" />
	<title> Or&#231;amento | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas</title>
	<? include "componentes/includes.php"; ?>
</head>
<body>
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "componentes/topo.php"; ?>
			</div>
		</div>	
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ContatoObrigado">
					<?php

					if (isset($_POST)) 
					{
						$newContato = new TbOrcamento;
						$newContato->nmCliente = $_POST['nome'];
						$newContato->dsEmail = $_POST['email'];
						$newContato->cdTelefone = $_POST['telefone'];
						$newContato->nmEmpresa = $_POST['empresa'];
						$newContato->hrContato = $_POST['horario'];
						$newContato->dsMensagem = $_POST['mensagem'];
						$newContato->linklProduto = $_POST['produto'];						
						$newContato->icAceito = $_POST['aceito'];

						$newContato->insert();

						$emailsender = $_POST['email'];
						$destinatario = "vendas@acasadapintura.com.br";

						if (PHP_OS == "Linux")
						$quebra_linha = "\n"; //Se for Linux
					elseif (PHP_OS == "WINNT")
						$quebra_linha = "\r\n"; // Se for Windows
					else
						//die("Este script nao esta preparado para funcionar com o sistema operacional de seu servidor");

						$aceitou = (isset($_POST['aceito']))?'Sim':'Não';


					$quebra_linha = "\n";
					
					$headers = "MIME-Version: 1.1" . $quebra_linha;
					$headers .= "Content-type: text/html; charset=utf-8" . $quebra_linha;
					$headers .= "From: vendas@acasadapintura.com.br". $quebra_linha;
					$headers .= "Cc: marketing@acasadapintura.com.br" . $quebra_linha;
					$headers .= "Return-Path: " .$destinatario . $quebra_linha;
					
					$mensagem .= "Orçamento". "<br />";
					$mensagem .= "<strong> Nome: </strong>" . utf8_encode($_POST["nome"]) . "<br />";
					$mensagem .= "<strong> Email: </strong>" . $_POST["email"] . "<br />";
					$mensagem .= "<strong> Telefone: </strong>" . $_POST["telefone"] . "<br />";
					$mensagem .= "<strong> Empresa onde trabalha: </strong>" . utf8_encode($_POST["empresa"]) . "<br />";
					$mensagem .= "<strong> Horário para contato: </strong>" . $_POST["horario"] . "<br />";
					$mensagem .= "<strong> Link do produto: </strong>" . $_POST["produto"] . "<br />";
					$mensagem .= "<strong> Mensagem: </strong>" . utf8_encode($_POST["mensagem"]) . "<br />";
					$mensagem .= "<strong> Aceita receber informações e promoções? </strong>" . $aceitou . "<br />";

					$sucesso = mail($destinatario, "Orçamento - A Casa da Pintura", $mensagem, $headers, "-r" . $emailsender);

					// para cliente

					$Mensagem = "";
					$headers="";
					
					$headers .= "MIME-Version: 1.1" . $quebra_linha;
					$headers .= "Content-type: text/html; charset=utf-8" . $quebra_linha;
					$headers .= "From: vendas@acasadapintura.com.br". $quebra_linha;
					$headers .= "Return-Path: vendas@acasadapintura.com.br";
					$headers .= "Reply-To: vendas@acasadapintura.com.br" . $quebra_linha;
					
					$Mensagem = "Sua solicitação de orçamento foi enviada com sucesso, em breve entraremos em contato!";

					$sucesso = mail($emailsender, $_POST["assunto"] . "Confirmação Orçamento - A Casa da Pintura", $Mensagem, $headers);


					if ($sucesso)
					{
						echo('<h2>Sua solicita&ccedil;&atilde;o foi enviada com sucesso! </h2>');
						echo('<p> Em breve retornaremos.</p>');
						echo('<p class="link"> Ir para <a href="index.php" title="P&aacute;gina inicial A Casa da Pintura">home.</a><img src="slices/icon-home.jpg" title="Ir para a p&aacute;gina inicial A Casa da Pintura" /alt="Ir para a página inicial A Casa da Pintura"> </p>');
					} 
					else
					{
						echo('<h2>Falha ao enviar orçamento!</h2>');
						echo('<p> Por favor, tente novamente. </p>');
					}
				}

				?>
			</div>
		</div>
	</div>
	<div id="Linha3">
		<? include "componentes/rodape.php"; ?>
	</div>
</div>
<div id="mask"></div>
</body>
</html>