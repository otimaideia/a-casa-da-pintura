<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas imobiliarias industriais e tintas automotivas | Loja de Tintas A Casa da Pintura</title>
	<meta name="Description" content="Comprar Tintas imobiliarias, tintas industriais e tintas automotivas. Todos os tipos de tintas voc� encontra no site da Casa da Pintura, venda de spray, impermeabilizante e acess�rios para pintura" />
	<meta name="Keywords" content="Tintas imobiliarias industriais automotivas site Casa Pintura venda spray impermeabilizante acess�rios loja comprar" />
	<? include "componentes/includes.php"; ?>
</head>
<body id="PaginaHome">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "componentes/topo.php"; ?>
			</div>
		</div>	
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="Slider">
					<div id="Images">
						<ul>
							<li><img src="slices/img-Banner-Pintura.png" height="413" title="Banner 1" alt="Banner 1" width="100%" /></li>
							<li><img src="slices/img-Banner-Pintura-2.png" height="413" title="Banner 2" alt="Banner 2"  width="100%"/>	</li>
						</ul>
					</div>
					<div id="Opcoes">
						<ul>
							<li>
								<a class="Icone" href="tintas-residenciais.php" title="Tintas Residenciais"><img src="slices/icon-Tintas-Residenciais.png" title="Tintas Residenciais" alt="Tintas Residenciais" /></a>
								<h2><a href="tintas-residenciais.php" title="Tintas Residenciais">Tintas Residenciais</a></h2>
								<p><a href="tintas-residenciais.php" title="Veja op��es de tintas para utiliza��o em casas, apartamentos e qualquer tipo de im�vel.">Veja op��es de tintas para utiliza��o em casas, apartamentos e qualquer tipo de im�vel.</a></p>	
							</li>
							<li id="TintasIndustriais">
								<a class="Icone" href="tintas-industriais.php" title="Tintas Industriais"><img src="slices/icon-Tintas-Industriais.png" title="Tintas Industriais" alt="Tintas Industriais" /></a>
								<h2><a href="tintas-industriais.php" title="Tintas Industriais">Tintas Industriais</a></h2>
								<p><a href="tintas-industriais.php" title="Temos toda linha de Epoxi, Alcatr�o de Hulha, Polieuretano, Primers, Tintas para alta temperatura e Esmaltes Sint�ticos.">Temos toda linha de Epoxi, Alcatr�o de Hulha, Polieuretano, Primers, Tintas para alta temperatura e Esmaltes Sint�ticos.</a></p>	
							</li>
							<li style="margin-right: 0;" id="TintasAutomotivas">
								<a class="Icone" href="tintas-automotivas.php" title="Tintas Automotivas"><img src="slices/icon-Tintas-Automotivas.png" title="Tintas Automotivas" alt="Tintas Automotivas" /></a>
								<h2><a href="tintas-automotivas.php" title="Tintas Automotivas">Tintas Automotivas</a></h2>
								<p><a href="tintas-automotivas.php" title="Diversas op��es de tintas para seu carro, moto, caminh�o ou qualquer tipo de autom�vel.">Diversas op��es de tintas para seu carro, moto, caminh�o ou qualquer tipo de autom�vel.</a></p>	
							</li>
						</ul>
					</div>
				</div>
				<div id="QuemSomos">
					<div class="Center">
						<div id="Descricao">
							<h3>Quem somos</h3>
							<p>Uma das maiores Distribuidoras de Tintas Coral e toda linha de Tintas Akzo Nobel em S�o Paulo.</p>
							<p>Distribuidora especializada nas melhores marcas de acess�rios de pintura Tigre. Atendimento e produtos diferenciados para Construtoras, Empreiteiras, arquitetos e decoradores.</p>
							<p>Entregamos e despachamos para todo Brasil. Consulte-nos!</p>
							<a id="SaibaMais" href="quem-somos.php">Saiba mais</a>
						</div>
						<div id="Video">
							<iframe width="610" height="330" src="//www.youtube.com/embed/NGKI6Pytd7Y" frameborder="0"></iframe>
						</div>
					</div>
				</div>
				<div id="OutrosLinks">
					<div class="Center">
						<div id="SimuladorCores">
							<img src="slices/img-Simulador-de-Cores-Coral.png" title="Simulador de Cores" alt="Simulador de Cores" />
							<h3>Simulador de cores</h3>
							<p><a href="http://www.coral.com.br/Downloads/CliqueDecore/clique_decore.zip" title="Simule ambientes e encontre a cor ideal para sua casa!">Simule ambientes e encontre a cor ideal para sua casa!</a></p>
							<a class="SaibaMais" href="http://www.coral.com.br/Downloads/CliqueDecore/clique_decore.zip" title="Saiba mais">Saiba Mais</a>
						</div>
						<div id="DicasPintura" class="NoMargin">
							<img src="slices/img-Dicas-de-Pintura.png" title="Dicas de Pintura" alt="Dicas de Pintura" />
							<h3>Dicas de Pintura</h3>
							<p><a href="http://a-casadapintura.blogspot.com.br/" title="Visite nosso blog e conhe�a mais sobre t�cnicas de pintura!">Visite nosso blog e conhe�a mais sobre t�cnicas de pintura!</a></p>
							<a class="SaibaMais" href="http://a-casadapintura.blogspot.com.br/" target="_blank" title="Saiba mais">Saiba Mais</a>
						</div>
					</div>
				</div>
				<div id="ProdutosDestaque">
					<h3>Produtos em Destaque</h3>
					<div id="SliderProdutos">
						<a class="buttons prev disable" href="#" title="Anterior">left</a>
						<div class="viewport">
							<ul class="overview" style="width: 1560px; left: 0px;">
								<li>
									<h4><a href="coral/decora-cores.php" title="Tinta Coral Decora">Tinta Coral Decora</a></h4>
									<a href="coral/decora-cores.php" title="Tinta Coral Decora"><img src="slices/produtos-destaque/img-tinta-coral-decora-cores.png" title="Tinta Coral Decora" alt="Tinta Coral Decora" /></a>
								</li>									
								<li>
									<h4><a href="acessorios-de-pintura/rolo-de-la.php" target="_blank" title="Rolo de L�">Rolo de L�</a></h4>
									<a href="acessorios-de-pintura/rolo-de-la.php" title="Rolo de L�"><img src="slices/pinceis-e-rolos/img-rolo-de-la.jpg" title="Rolo de L�" alt="Rolo de L�" /></a>
								</li>
								<li>
									<h4><a href="coral/decora-luz-espaco.php" title="Tinta Coral Decora">Decora Luz Espa�o </a></h4>
									<a href="coral/decora-luz-espaco.php" title="Tinta Coral Decora Luz Espa�o"><img src="slices/produtos-destaque/img-tinta-coral-decora.png" title="Tinta Coral Decora Luz Espa�o" alt="Tinta Coral Decora Luz Espa�o" /></a>
								</li>
								<li>
									<h4><a href="#" title="Primer Poliuretano">Primer Poliuretano</a></h4>
									<a href="#" title="Primer Poliuretano"><img src="slices/produtos-destaque/img-primer-poliur-p710.jpg" title="Primer Poliuretano" alt="Primer Poliuretano" /></a>
								</li> 
								<li>
									<h4><a href="#" title="Super Lav�vel">Super Lav�vel</a></h4>
									<a href="#" title="Super Lav�vel"><img src="slices/produtos-destaque/img-super-lavavel.jpg" title="Super Lav�vel" alt="Super Lav�vel" /></a>
								</li>  
							</ul>
						</div>
						<a class="buttons next" href="#" title="Pr�ximo">right</a>
					</div>
				</div>
			</div>
		</div>		
		<div id="Linha3">
			<? include "componentes/rodape.php"; ?>
		</div>	
	</div>
	<div id="mask"></div>
</body>
</html>