				<h1><a href="/index.php" title="A Casa da Pintura">A Casa da Pintura</a></h1>
				<div id="EnglobaRight"><a id="Telefone" name="Telefone" href="#NumeroTelefone" title="Clique aqui para ver o Telefone" onClick="_gaq.push(['_trackEvent', 'Telefone', 'Visualiza��o', 'telefones Geral', 0, false]);"></a>
					<div id="NumeroTelefone" class="Telefone">
						<a href="#" title="Fechar" alt="Fechar" class="close"><img title="Fechar" alt="Fechar" src="slices/img-Close.gif" /></a>
						<h2>Selecionar telefone da linha desejada</h2>
						<ul>
							<li>
								<span title="Clique para visualizar telefone da linha residencial" id="residencial" onClick="_gaq.push(['_trackEvent', 'Telefone', 'Visualiza��o', 'linha residencial', 0, false]);"><img src="slices/icon-telefone-residencial.gif" title="Clique para visualizar o telefone da linha residencial" alt="Clique para visualizar o telefone da linha residencial" /><br/>(11) 2596-66...</span>								
							</li>
							<li>
								<span title="Clique para visualizar telefone da linha industrial" id="industrial" onClick="_gaq.push(['_trackEvent', 'Telefone', 'Visualiza��o', 'linha industrial', 0, false]);"><img src="slices/icon-telefone-industrial.gif" title="Clique para visualizar o telefone da linha industrial" alt="Clique para visualizar o telefone da linha industrial" /></span>
								
							</li>
							<li>
								<span title="Clique para visualizar telefone da linha automotiva" id="automotiva" onClick="_gaq.push(['_trackEvent', 'Telefone', 'Visualiza��o', 'linha automotiva', 0, false]);"><img src="slices/icon-telefone-automotiva.gif" title="Clique para visualizar o telefone da linha industrial" alt="Clique para visualizar o telefone da linha industrial" /></span>
							</li>
						</ul>
						<ul>
							<li><strong id="telefoneresidencial">(11) 2596-6666</strong></li>
							<li><strong id="telefoneindustrial">(11) 2596-6666</strong></li>
							<li><strong id="telefoneautomotiva">(11) 2596-6658</strong></li>
						</ul>

					</div>
				<a onClick="_gaq.push(['_trackEvent', 'Endere�os', 'Visualiza��o', 'nossas lojas', 0, false]);" id="BtnNossasLojas" href="#NossasLojas" name="NossasLojas" title="Selecione a Loja"></a>
				<div id="NossasLojas" class="window">
					<a href="#" title="Fechar" alt="Fechar" class="close"><img title="Fechar" alt="Fechar" src="slices/img-Close.gif" /></a>
					<h2>Nossas Lojas</h2>
					<ul>
						<li>
							<img src="slices/lojas/img-loja-diadema.jpg" title="Loja de tinta Diadema" alt="Loja de tintas Diadema"/>
							<h2><a href="loja-diadema.php" title="Loja de Tintas Diadema">Loja de Tintas Diadema</a></h2>
							<p><a href="loja-diadema.php" title="Av. Fabio Eduardo R. Esquivel, 753 - Centro - Diadema - Tel. 11 3201-5100">Av. Fabio Eduardo R. Esquivel, 753 - Centro - Diadema</a></p>
							<p><span id="LojaDiadema" title="Clique para ver o telefone da loja de tintas A Casa da Pintura em Diadema" onClick="_gaq.push(['_trackEvent', 'Telefone', 'Visualiza��o', 'diadema', 0, false]);">Tel.: (11) 3201-51... Ver telefone </span><strong id="TelefoneDiadema">Telefone: (11) 3201-5100</strong></p>
							<p><a class="VerMapa" href="loja-diadema.php" title="Ver mapa da Loja de tintas A Casa da Pintura em Diadema">Ver mapa</a></p>
						</li>
						<li>
							<img src="slices/lojas/img-loja-santo-andre.jpg" title="Loja de tinta Santo Andr�" alt="Loja de tintas Santo Andr�"/>
							<h2><a href="loja-santo-andre.php" title="Loja de Tintas Santo Andr�">Loja de Tintas Santo Andr�</a></h2>
							<p><a href="loja-santo-andre.php" title="Av. Itamarati, 1157 - Pq. Ja�atuba - Santo Andr� - Telefone: 11 4436-6666">Av. Itamarati, 1157 - Pq. Ja�atuba - Santo Andr�</a></p>
							<p><span id="LojaSantoAndre" title="Clique para ver o telefone da loja de tintas A Casa da Pintura em Santo Andr�" onClick="_gaq.push(['_trackEvent', 'Telefone', 'Visualiza��o', 'santo andre', 0, false]);">Tel.: (11) 4436-66... Ver telefone </span><strong id="TelefoneSantoAndre">Telefone: (11) 4436-6666</strong></p>
							<p><a class="VerMapa" href="loja-santo-andre.php" title="Ver mapa da Loja de tintas A Casa da Pintura em Santo Andr�">Ver mapa</a></p>
						</li>
						<li>
							<img src="slices/lojas/img-loja-sao-caetano.jpg" title="Loja de tinta S�o Caetano" alt="Loja de tintas S�o Caetano"/>
							<h2><a href="loja-sao-caetano.php" title="Loja de Tintas S�o Caetano">Loja de Tintas S�o Caetano</a></h2>
							<p><a href="loja-sao-caetano.php" title="Rua Alegre, 1.163 - Barcelona - S�o Caetano - Telefone: 11 3565-3335">Rua Alegre, 1.163 - Barcelona - S�o Caetano</a></p>
							<p><span id="LojaSaoCaetano" title="Clique para ver o telefone da loja de tintas A Casa da Pintura em S�o Caetano" onClick="_gaq.push(['_trackEvent', 'Telefone', 'Visualiza��o', 's�o caetano', 0, false]);">Tel.: (11) 3565-33... Ver telefone </span><strong id="TelefoneSaoCaetano">(11) 3565-3335</strong></p>
							<p><a class="VerMapa" href="loja-sao-caetano.php" title="Ver mapa da Loja de tintas A Casa da Pintura em S�o Caetano">Ver mapa</a></p>
						</li>
						<li>
							<img src="slices/lojas/img-loja-sao-bernardo-do-campo.jpg" title="Loja de tinta S�o Bernardo do Campo" alt="Loja de tintas S�o Bernardo do Campo"/>
							<h2><a href="loja-sao-bernardo-do-campo.php" title="Loja de Tintas S�o Bernardo do Campo">Loja de Tintas S�o Bernardo do Campo</a></h2>
							<p><a href="loja-sao-bernardo-do-campo.php" title="Av. Pereira Barreto, 646 - Centro - SBC - Tel. 11 4121-9292">Av. Pereira Barreto, 646 - Centro - SBC</a></p>
							<p><span id="LojaSaoBernardo" title="Clique para ver o telefone da loja de tintas A Casa da Pintura em S�o Bernardo do Campo" onClick="_gaq.push(['_trackEvent', 'Telefone', 'Visualiza��o', 's�o bernardo', 0, false]);">Tel.: (11) 4121-92... Ver telefone </span><strong id="TelefoneSaoBernardo">(11) 4121-9292</strong></p>
							<p><a class="VerMapa" href="loja-sao-bernardo-do-campo.php" title="Ver mapa da Loja de tintas A Casa da Pintura em S�o Bernardo do Campo">Ver mapa</a></p>
						</li>
						<li>
							<img src="slices/lojas/img-loja-frei-gaspar.jpg" title="Loja tinta na Frei Gaspar" alt="Loja de tinta na Frei Gaspar"/>
							<h2><a href="loja-frei-gaspar.php" title="Loja de Tintas Frei Gaspar">Loja de Tintas Frei Gaspar</a></h2>
							<p><a href="loja-frei-gaspar.php" title="Rua Frei Gaspar, 283 - Centro - SBC - Tel. 11 4339-3232">Rua Frei Gaspar, 283 - Centro - SBC</a></p>
							<p><span id="LojaFreiGaspar" title="Clique para ver o telefone da loja de tintas A Casa da Pintura em Frei Gaspar" onClick="_gaq.push(['_trackEvent', 'Telefone', 'Visualiza��o', 'frei gaspar', 0, false]);">Tel.: (11) 4339-32... Ver telefone </span><strong id="TelefoneFreiGaspar">(11) 4339-3232</strong></p>
							<p><a class="VerMapa" href="loja-frei-gaspar.php" title="Ver mapa da Loja de tintas A Casa da Pintura em Frei Gaspar">Ver mapa</a></p>							
						</li>
						<li>
							<img src="slices/lojas/img-loja-automotivo.jpg" title="Loja de Tintas Automotivas" alt="Loja de Tintas Automotivas"/>
							<h2><a href="loja-automotivo.php" title="Loja de Tintas Automotivo">Loja de Tintas Automotivo - Planalto</a></h2>
							<p><a href="loja-automotivo.php" title="Av. Alvaro Guimar�es, 128 - Tel. 11 2381-4666">Av. Alvaro Guimar�es, 128</a></p>
							<p><span id="LojaAutomotivo" title="Clique para ver o telefone da loja de tintas Automotivas A Casa da Pintura" onClick="_gaq.push(['_trackEvent', 'Telefone', 'Visualiza��o', 'centro de tintas automotivas', 0, false]);">Tel.: (11) 2596-66... Ver telefone </span><strong id="TelefoneAutomotivo">(11) 2596-6658</strong></p>
							<p><a class="VerMapa" href="loja-automotivo.php" title="Ver mapa da Loja de tintas Automotivas A Casa da Pintura">Ver mapa</a></p>						
						</li>
						<li>
							<img src="slices/lojas/img-loja-maua.jpg" title="Loja de tinta Mau�" alt="Loja de tinta Mau�"/>
							<h2><a href="loja-maua.php" title="Loja de Tintas Mau�">Loja de Tintas Mau�</a></h2>
							<p><a href="loja-maua.php" title="Av. Dr. Getulio Vargas, 100 -� Vl. Guarani">Av. Dr. Getulio Vargas, 100 - Vl. Guarani</a></p>
							<p><span id="LojaMaua" title="Clique para ver o telefone da loja de tintas A Casa da Pintura em Mau�" onClick="_gaq.push(['_trackEvent', 'Telefone', 'Visualiza��o', 'mau�', 0, false]);">Tel.: (11) 4543-30... Ver telefone </span><strong id="TelefoneMaua">(11) 4543-3000</strong></p>
							<p><a class="VerMapa" href="loja-maua.php" title="Ver mapa da Loja de tintas A Casa da Pintura em Mau�">Ver mapa</a></p>						
						</li>
						<li>
							<img src="slices/lojas/img-centro-distribuicao.jpg" title="Centro de Distribui��o" alt="Centro de Distribui��o"/>
							<h2><a href="centro-de-distribuicao.php" title="Centro de Distribui��o">Centro de Distribui��o</a></h2>
							<p><a href="centro-de-distribuicao.php" title="Av. Piraporinha, 1111 - Bairro Planalto SBC">Av. Piraporinha, 1111 - Bairro Planalto SBC</a></p>
							<p><span id="LojaCentroDistribuicao" title="Clique para ver o telefone do Centro de Distribui��o A Casa da Pintura" onClick="_gaq.push(['_trackEvent', 'Telefone', 'Visualiza��o', 'centro de distribui��o', 0, false]);">Tel.: (11) 2596-66... Ver telefone </span><strong id="TelefoneCentroDistribuicao">(11) 2596-6666</strong></p>
							<p><a class="VerMapa" href="centro-de-distribuicao.php" title="Ver mapa do Centro de Distribui��o A Casa da Pintura">Ver mapa</a></p>						
						</li>
					</ul>					
				</div>
			</div>
				<ul id="MenuTopo">
					<li id="Home">
						<a href="/index.php" title="Home">Home</a>	
					</li>
					<li id="TintasResidenciais">
						<a href="/tintas.php" title="Tintas">Tintas</a>
						<ul>
							<li><a href="/tintas-residenciais.php" title="Tintas Residenciais">Tintas Residenciais</a></li>
							<li><a href="/tintas-imobiliarias.php" title="Tintas Imobili&aacute;rias">Tintas Imobili&aacute;rias</a></li>
							<li><a href="/tintas-industriais.php" title="Tintas Industriais">Tintas Industriais</a></li>
							<li><a href="/tintas-automotivas.php" title="Tintas Automotiva">Tintas Automotiva</a></li>
							<li><a href="/tintas-spray.php" title="Spray">Spray</a></li>
						</ul>	
					</li>
					<li id="Acessorios">
						<a href="#" title="Acess�rios">Acess�rios</a>	
						<ul>
							<li><a href="/acessorios-de-pintura.php" title="Lixas e Abrasivos">Lixas e Abrasivos</a></li>
							<li><a href="/acessorios-de-pintura.php" title="Colas e Adesivos">Colas e Adesivos</a></li>
							<li><a href="/acessorios-de-pintura.php" title="Pinc�is e Rolos">Pinc�is e Rolos</a></li>
							<li><a href="/acessorios-de-pintura.php" title="Escadas">Escadas</a></li>
						</ul>
					</li>
					<li>
						<a href="http://www.belatintas.com.br/" target="_blank" title="Loja Online" onClick="_gaq.push(['_trackEvent', 'Loja', 'Sa�da', 'loja online', 0, false])";>Loja Online</a>	
					</li>
					<li id="Contato">
						<a href="/contato.php" title="Contato">Contato</a>	
					</li>
				</ul>