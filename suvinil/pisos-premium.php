<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title> Suvinil Pisos Premium| A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas</title>
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Suvinil Pisos Premium" alt="Suvinil Pisos Premium" src="../slices/tintas-suvinil/img-pisos-premium.jpg" />
						</div>
						<h2>Suvinil Pisos Premium</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>� um produto de f�cil aplica��o, secagem r�pida, permitindo uma boa  cobertura e ader�ncia, bom alastramento, resistente � alcalinidade, maresia e  abras�o. Todos estes atributos tornam o produto uma �tima op��o econ�mica, para  renovar e embelezar pisos. </p>
							<p><b>Local de Aplica��o</b></p>
							<p>Exterior/Interior</p>
							<p>Tinta acrlica indicada para pintura e repintura de pisos cimentados e  pisos cermicos de acabamento fosco, tanto em reas externas ou internas.  indicado para reas de lazer, escadas, varandas, quadras poliesportivas e outras  superfcies de concreto rstico, liso ou ainda para repintura.<br>
								<br>
								Recomendao: O contato imediato com o piso aps aplicao do produto pode ocasionar  danos a pintura, portanto recomendamos aguardar 48 horas para utilizao do mesmo  para trfego de pessoas ou 72 horas para trfego de veculos.</p>
								<p><b>Acabamento</b></p>
								<p>Fosco</p>	
							</div>
							<div id="InformacoesAdicionais">
								<div id="Detalhes">
									<span id="Detalhe">Detalhes:</span>
									<ul>
										<li><b>Tintas Dispon�veis</b></li>
										<li>4811	SUVINIL PISO AMARELO 18LT</li>
										<li>4812	SUVINIL PISO AMARELO 3,6LT</li>
										<li>4813	SUVINIL PISO AZUL 18LT</li>
										<li>4814	SUVINIL PISO AZUL 3,6LT</li>
										<li>4815	SUVINIL PISO BRANCO 18LT</li>
										<li>4819	SUVINIL PISO CINZA 18LT</li>
										<li>4821	SUVINIL PISO CINZA ESCURO 18LT</li>
										<li>4823	SUVINIL PISO CONCRETO 18LT</li>
										<li>4825	SUVINIL PISO PRETO 18LT</li>
										<li>4827	SUVINIL PISO VERDE 18LT</li>
										<li>4828	SUVINIL PISO VERDE 3,6LT</li>
										<li>4829	SUVINIL PISO VERMELHO 18LT</li>
										<li>4830	SUVINIL PISO VERMELHO 3,6LT</li>
									</ul>
									<ul>
										<li><b>Composi��o:</b></li>
										<li>Resina � base de dispers�o aquosa de copol�mero estireno-acr�lico,  pigmentos isentos de metais pesados, cargas minerais inertes, glic�is,  tensoativos carboxilados, bactericidas e fungicidas (a base de isotiazolonas).</li>
										<li><b>Observa��o:</b></li>
										<li>Cimentado novo (r�stico ou liso) - Aguardar secagem e cura (28 dias no  m�nimo), aplicar uma dem�o de Suvinil Fundo Preparador para Paredes base �gua.  Cimentado queimado ? Ap�s secagem e cura, aplicar uma dem�o de Suvinil Fundo  Branco Ep�xi (conforme as instru��es da embalagem). Cimentado Fraco ? Aplicar  uma dem�o de Suvinil Fundo Preparador para Paredes Base �gua. Imperfei��es  Profundas: Corrigir com argamassa e aguardar secagem e cura (28 dias no  m�nimo). Aplicar uma dem�o de Suvinil Fundo Preparador para Paredes base d�gua.</li>
										<li><b>Importante</b></li>
										<li>Para se obter o m�ximo da qualidade dos produtos Suvinil � necess�rio um  bom <b>preparo da superf�cie</b> a ser aplicada.</li>
									</ul>						
								</div>
							</div>
						</div>
						<? include "../componentes/solicitar-orcamento.php"; ?>
						<? include "../componentes/outros-produtos.php"; ?>
					</div>
				</div>
			</div>
			<div id="Linha3">
				<? include "../componentes/rodape-tintas.php"; ?>
			</div>
		</div>
		<div id="mask"></div>
	</body>
	</html>