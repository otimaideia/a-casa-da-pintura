<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>772 DF Sem Brilho | Advance Tintas</title>
	<meta name="Description" content="772 DF Sem Brilho: Tinta de dupla fun��o poliuretano acr�lico bicomponente de altos s�lidos." />
	<meta name="Keywords" content="Tintas advance 772 df sem brilho A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="772 DF Sem Brilho " alt="772 DF Sem Brilho " src="../slices/tintas-advance/img-772-df-sem-brilho.jpg" />
						</div>
						<h2>772 DF Sem Brilho</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Tinta de dupla fun��o poliuretano acr�lico bicomponente de altos s�lidos...</p>
							<p><b>Locais para Aplica��o:</b>  Metais e Paredes</p>
							<p><a target="_blank" title="Dados t�cnicos 772 DF Sem Brilho" href="/2014/pdf/772-df-sem-brilho.pdf">Dados T�cnicos (PDF)</a></p>
							<span class="Titulo">Mais informa��es sobre a tinta</span>
							<p>Tinta de dupla fun��o poliuretano acr�lico bicomponente de altos s�lidos, baixo teor de compostos organicos volateis (Low VOC) a base de isocianato alif�tico, proporcionando um acabamento semibrilhante com excelente flexibilidade, resistencia quimica e dureza. Aplicado em uma �nica dem�o de 70 a 120 micrometros apresenta �tima resist�ncia ao intemperismo a vapores e respingos de �cidos e produtos qu�micos. Acabamento em sistemas de alto desempenho em revestimento anticorrosivos para equipamentos, pinturas externas de tanques, transformadores e estruturas met�licas em geral onde se necessitam de resist�ncia qu�mica, a abras�o e reten��o de cor. Aplicado diretamente sobre Adepoxi 78 Primer ou adepoxi 878, torna este sistema um excelente revestimento para prote��o anticorrosiva em ind�strias qu�micas, petroqu�micas, de papel e celulose, a�ucar e �lcool e outras de processamento qu�mico e �reas mar�timas. N�o recomendado para servi�os de imers�o. Amplamente utilizado em conjunto com Adepoxi 38 Verniz ou Adepoxi 138 HS Verniz, quando da aplica��o sobre concreto. Seu aspecto semibrilhante em alta espessura auxilia a mascarar os pequenos defeitos do substraro.</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>