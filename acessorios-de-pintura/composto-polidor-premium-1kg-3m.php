<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Composto Polidor Premium 1kg 3M | Acess&oacute;rios de Pintura</title>
	<meta name="Description" content="Composto Polidor Premium 1kg 3M: O Composto Polidor Premium 3M foi desenvolvido para ser usado no processo de polimento de repinturas automotivas." />
	<meta name="Keywords" content="cera protetora casa pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Composto Polidor Premium 1 kg 3M" alt="Composto Polidor Premium 1 kg 3M" src="../slices/pinceis-e-rolos/img-composto-polidor-g.jpg" />
						</div>
						<h2>Composto Polidor Premium 1 kg 3M</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>O <b>Composto Polidor Premium 3M</b> foi desenvolvido para ser usado no processo de polimento de repinturas automotivas. Tem excelente poder de remo��o dos riscos de lixamento, contamina��es e outros defeitos da camada de pintura, proporcionando brilho intenso. Indicado tamb�m para o polimentos de todos os tipos de tintas e vernizes.</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>