<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Adepoxi 86 DF | Advance Tintas</title>
	<meta name="Description" content="Adepoxi 86 DF Advance Tintas: Ep�xi modificado de alto s�lidos, secagem r�pida e tolerante a superf�cies de a�o carbono." />
	<meta name="Keywords" content="Tintas advance tintas adepoxi A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Adepoxi 86 Df" alt="Adepoxi 86 Df" src="../slices/tintas-advance/img-epoxi-acabamento-brilhante.jpg" />
						</div>
						<h2>Adepoxi 86 Df</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Ep�xi modificado de alto s�lidos, secagem r�pida e tolerante a superf�cies de a�o carbono.</p>
							<p><b>Locais para Aplica��o:</b>  Metais</p>
							<p><a target="_blank" title="Dados t�cnicos Adepoxi 86 Df" href="/2014/pdf/adepoxi-86.pdf">Dados T�cnicos (PDF)</a></p>
							<span class="Titulo">Mais informa��es sobre a tinta</span>
							<p>Adepoxi 86 DF - Aspecto Semi Brilho (S/B) � um ep�xi modificado de alto   s�lidos, secagem r�pida, tolerante a superf�cies de a�o carbono   preparada mecanicamente, utilizado como primer e acabamento em   superf�cies oxidadas, onde n�o � vi�vel o jateamento abrasivo. Apresenta   vantagens com rela��o a maioria dos produtos tradicionais de alta   espessura pelo baixo VOC emitido na atmosfera. Possibilita aplica��es em   uma �nica dem�o em altas espessuras com custo por m� menor que os   produtos tradicionais. Especialmente desenvolvido com pigmentos   anticorrosivos at�xico, cargas lamelares e resina ep�xi modificada para   permitir uma boa ancoragem sobre superf�cies preparadas atrav�s de   limpeza mec�nica. Atende a moderna tecnologia da pintura industrial,   minimizando a polui��o do meio ambiente. Dispensa a utiliza��o de primer.</p>
						</div>
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>