<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Detalhes | A Casa da Pintura</title>
    <? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
    <div id="Pagina">
        <div id="Linha1">
            <div id="ConteudoLinha1">
                <? include "../componentes/topo.php"; ?>
            </div>
        </div>
        <div id="Linha2">
            <div id="ConteudoLinha2">
                <div id="ConteudoProdutos">
                    <div id="Produto">
                        <div id="Informacoes">
                            <a id="FazerPedido" href="http://www.belatintas.com.br/verniz-brilhante-sparlack-maritimo-galao-galao.html" target="_blank" title="Fazer pedido">Fazer pedido</a>
                            <div id="ImagemProduto">
                                <img title="Veniz Sparlack Mar�timo Fosco" alt="Veniz Sparlack Mar�timo Fosco" src="../slices/tintas-coral/img-seladora.jpg" />
                            </div>
                            <h2>Sparlack Seladora</h2>
                            <div id="InformacoesProduto">
                                <span class="Titulo">Descri��o do produto</span>
                                <p>Sparlack Neutrex � um tingidor e impermeabilizante colorido que protege madeira e paredes de alvenaria das a��es da �gua. Indicado para ambientes internos e externos, como esquadrias, port�es de madeira e paredes de alvenari.</p>
                                <p>F�cil aplica��o</p>
                                <p>Melhora o rendimento dos vernizes Sparlack</p>
                                <p>Elimina a porosidade da madeira</p>      
                                <p>�timo poder de enchimento, selagem e ader�ncia</p>   
                                <p>1) Dilui��o: N�o diluir. Pronto para uso.</p>    
                                <p>2) Aplica��o: Pincel, trincha e rolo.</p>    
                                <p>3) Secagem: Aplicar de duas a tr�s dem�os (dependendo da tonalidade desejada), com intervalo de secagem de no m�nimo 2 horas.</p>    
                                <p>4) Rendimento: 32 m� / gal�o / dem�o � 08 m� / � de gal�o / dem�o.</p>
                                <p>5) Cores: Incolor</p>
                                <p>6) Acabamento: Semibrilhante</p>
                                <p>7) Embalagens: gal�o (3,6L) e quarto (0,9L) .</p>
                            </div>
                            <div id="InformacoesAdicionais"> 
                                <div id="TintasDisponiveis">
                                    <span id="TintasDisponiveisTitulo">Tintas Dispon�veis:</span>
                                    <ul>
                                        <li>3044 SPARLACK SELADORA CONCENTRADA 1/4 0.9LT</li>
                                        <li>3043 SPARLACK SELADORA CONCENTRADA GL 3.6LT
                                        </li>
                                    </ul>
                                </div>                          
                            </div>
                        </div>
                    </div>
                    <? include "../componentes/solicitar-orcamento.php"; ?>
                    <? include "../componentes/outros-produtos.php"; ?>
                </div>
            </div>
        </div>
        <div id="Linha3">
            <? include "../componentes/rodape-tintas.php"; ?>
        </div>
    </div>
    <div id="mask"></div>
</body>
</html>