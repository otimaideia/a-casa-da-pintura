<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Massa para Madeira Coral | A Casa da Pintura</title>
	<meta name="Description" content="Coral Massa para Madeira: A Massa para Madeira � ideal para corrigir imperfei��es" />
	<meta name="Keywords" content="massa madeira coral casa da pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Produto">
						<div id="Informacoes">
							<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
							<div id="ImagemProduto">
								<img title="Massa para Madeira Coral" alt="Massa para Madeira Coral" src="../slices/tintas-coral/img-massa-para-madeira.jpg" />
							</div>
							<h2>Massa para Madeira</h2>
							<div id="InformacoesProduto">
								<span class="Titulo">Descri��o do produto</span>
								<p>A Massa para Madeira � ideal para corrigir imperfei��es, podendo ser utilizada em interiores e exteriores. � de f�cil aplica��o, possui alto poder de enchimento e �tima lixabilidade. Tem baix�ssimo odor e secagem r�pida.</p>
							</div>
							<div id="InformacoesAdicionais"> 
								<div id="Detalhes">
									<span id="Detalhe">Detalhes</span>	
									<ul>
										<li><b>Embalagem/Rendimento</b></li>
										<li>Gal�o <b>3,6 L</b>: 10 a 15 m� por dem�o
										</li><li>Quarto <b>0,9 L</b>: 2,5 a 3,75 m� por dem�o</li>
									</ul>
									<ul>
										<li><b>Aplica��o</b></li>
										<li>Esp�tula ou desempenadeira de a�o.Limpe as ferramentas com �gua.</li>
									</ul>
									<ul>
										<li><b>Dilui��o</b></li>
										<li>Produto pronto para uso, n�o diluir.</li>
									</ul>

									<ul>
										<li><b>Acabamento</b></li>
										<li>Fosco</li>
									</ul>

									<ul>
										<li><b>Secagem</b></li>
										<li>Entre dem�os: 3 horas</li>
										<li>Final: 6 horas</li>
									</ul>					
								</div>	
								<div id="TintasDisponiveis">
									<span id="TintasDisponiveisTitulo">Tintas Dispon�veis</span>
									<ul>
										<li>Massa para Madeira 0.9LT </li>
										<li>Massa para Madeira 3.6LT </li>
									</ul>
								</div>						
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>