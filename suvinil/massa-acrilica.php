<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title> Suvinil Massa Acrilica | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas</title>
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Suvinil Massa Acrilica" alt="Suvinil Massa Acrilica" src="../slices/tintas-suvinil/img-massa-acrilica.jpg" />
						</div>
						<h2>Suvinil Massa Acrilica</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Para paredes internas.</p>
							<p>Nivela e corrige imperfei��es.</p>
							<p>Permite um acabamento mais liso e requintado �s tintas L�tex PVA ou Acr�lica. </p>
							<p><b>Aplica��o</b></p>
							<p>Paredes - Complementos</p>
							<p><b>Local de Aplica��o</b></p>
							<p>Interior.</p>
							<p><b>Corrigindo Problemas </b></p>
							<p>Bolhas, Trincas</p>
						</div>
						<div id="InformacoesAdicionais">
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<ul>
									<li><b>Tintas Dispon�veis</b></li>
									<li>MASSA CORRIDA 18LT</li>
									<li>MASSA CORRIDA 3.6LT
									</li>
								</ul>
								<ul>
									<li><b>Composi��o:</b></li>
									<li>Resina alqu�dica � base de �leo vegetal   semi-secativo, hidro-carbonetos alif�ticos, cargas sint�ticas (fosco),   pigmento inorg�nico e secantes organo met�-licos. N�o cont�m benzeno e   metais pesados.</li>
									<li><b>Importante:</b></li>
									<li>Para se obter o m�ximo da qualidade dos produtos Suvinil � necess�rio um bom <b>preparo da superf�cie</b> a ser aplicada..</li>
								</ul>

							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>