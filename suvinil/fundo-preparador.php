<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title> Fundo Preparador| A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas</title>
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Fundo Preparador" alt="Fundo Preparador" src="../slices/tintas-suvinil/img-fundo-reparador-suvinil.jpg" />
						</div>
						<h2>Fundo Preparador</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Suvinil Fundo Branco Fosco Para superf�cies internas e externas de madeira nova Melhora o rendimento e a qualidade dos esmaltes. Boa ader�ncia e alastramento, �timo enchimento e prote��o contra oxida��o e corros�o..</p>
							<p><b>Aplica��o</b></p>
							<p>Madeiras</p>
							<p><b>Local de Aplica��o</b></p>
							<p>Indicado para superf�cies  externas e internas de madeira nova</p>
							<p>Resina alqu�dica � base de  �leo vegetal semi-secativo, hidrocarbonetos alif�ticos e arom�ticos, cargas  minerais inertes, pigmentos inorg�nicos e secantes organomet�licos.&nbsp;</p>	
						</div>
						<div id="InformacoesAdicionais">
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<ul>
									<li><b>Tintas Dispon�veis</b></li>
									<li>4764	FUNDO PREPARADOR BASE DAGUA 18LT</li>
									<li>4765 FUNDO PREPARADOR BASE DAGUA 3.6LT
									</li>
								</ul>
								<ul>
									<li><b>Composi��o:</b></li>
									<li>Resina alqu�dica � base de  �leo vegetal semi-secativo, pigmentos org�nicos e inorg�nicos, cargas minerais  inertes (acetinado e fosco), hidrocarbonetos alif�ticos, secantes  organo-met�licos. N�o cont�m benzeno.</li>
									<li><b>Importante</b></li>
									<li>
										<p>Para se obter o m�ximo da  qualidade dos produtos Suvinil � necess�rio um bom <b>preparo  da superf�cie</b> a  ser aplicada.</p>
									</li>
								</ul>					
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>