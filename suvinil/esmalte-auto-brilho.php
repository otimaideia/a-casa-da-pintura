<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title> Esmalte Auto Brilho | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas</title>
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Esmalte Auto Brilho" alt="Esmalte Auto Brilho" src="../slices/tintas-suvinil/img-suvinil-acrilico-fosco.jpg" />
						</div>
						<h2>Esmalte Auto Brilho</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>F�cil aplica��o com caracter�sticas de alta resist�ncia �s intemp�ries. Possui �tima secagem, excelente acabamento e sua f�rmula siliconada permite uma menor ader�ncia de sujeira facilitando a limpeza.</p>
							<p><b>Aplica��o</b></p>
							<p>Madeiras e Metais </p>
							<p><b>Local de Aplica��o</b></p>
							<p>Exterior/Interior</p>
							<p>Superf�cies internas e  externas de madeira e metais.</p>
							<p><b>Acabamento</b></p>
							<p>Brilhante e Acetinado</p>	
						</div>
						<div id="InformacoesAdicionais">
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<ul>
									<li><b>Tintas Dispon�veis</b></li>
									<li>4676	ESMALTE AB ALUMINIO 3.6LT</li>
									<li>4679	ESMALTE AB AMARELO OURO 3.6LT</li>
									<li>4681	ESMALTE AB AREIA 3.6LT</li>
									<li>4683	ESMALTE AB AZUL DEL REY 3.6LT</li>
									<li>4685	ESMALTE AB AZUL FRANCA 3.6LT</li>
									<li>4695	ESMALTE AB BRANCO 3.6LT</li>
									<li>4697	ESMALTE AB CAMURCA 3.6LT</li>
									<li>4699	ESMALTE AB CELESTE 3.6LT</li>
									<li>4701	ESMALTE AB CINZA ESCURO 3.6LT</li>
									<li>4703	ESMALTE AB CINZA MEDIO 3.6LT</li>
									<li>4705	ESMALTE AB COLORADO 3.6LT</li>
									<li>4739	ESMALTE AB CREME NOBRE 3.6LT</li>
									<li>4707	ESMALTE AB GELO 3.6LT</li>
									<li>4709	ESMALTE AB LARANJA 3.6LT</li>
									<li>4711	ESMALTE AB MARFIM NOBRE 3.6LT</li>
									<li>4713	ESMALTE AB MARROM 3.6LT</li>
									<li>4717	ESMALTE AB PLATINA 3.6LT</li>
									<li>4721	ESMALTE AB PRETO 3.6LT</li>
									<li>4723	ESMALTE AB TABACO 3.6LT</li>
									<li>4727	ESMALTE AB VERDE COLONIAL 3.6LT</li>
									<li>4729	ESMALTE AB VERDE FOLHA 3.6LT</li>
									<li>4733	ESMALTE AB VERMELHO 3.6LT</li>
								</ul>
								<ul>
									<li><b>Composi��o:</b></li>
									<li>Resina alqu�dica � base de  �leo vegetal semi-secativo, pigmentos org�nicos e inorg�nicos, cargas minerais  inertes (acetinado e fosco), hidrocarbonetos alif�ticos, secantes  organo-met�licos. N�o cont�m benzeno.</li>
									<li><b>Importante</b></li>
									<li><p>Para se obter o m�ximo da  qualidade dos produtos Suvinil � necess�rio um bom <b>preparo  da superf�cie</b> a  ser aplicada.</p></li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>