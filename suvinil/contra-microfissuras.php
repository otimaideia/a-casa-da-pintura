<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title> Suvinil Contra Microfissuras| A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas</title>
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Suvinil Contra Microfissuras" alt="Suvinil Contra Microfissuras" src="../slices/tintas-suvinil/img-contra-microfissuras.jpg" />
						</div>
						<h2>Suvinil Contra Microfissuras</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>100% acr�lica el�stica que torna as paredes imperme�veis, protegendo-as  contra infiltra��es de �gua de fora para dentro, causadas por fissuras  (pequenas trincas) de at� 0,2   mm de abertura existente nas superf�cies,proporcionando  um acabamento flex�vel, el�stico. </p>
							<p><b>Aplica��o</b></p>
							<p>Paredes</p>
							<p><b>Local de Aplica��o</b></p>
							<p>Exterior</p>
							<p>� indicado especialmente  para �reas externas, pode ser aplicada sobre reboco, massa acr�lica, texturas  sem hidrorrepel�ncia, concreto e fibrocimento.</p>
							<p><b>Acabamento</b></p>
							<p>Fosco</p>
						</div>
						<div id="InformacoesAdicionais">
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>
								<ul>
									<li><b>Tintas Dispon�veis</b></li>
									<li>4757 CONTRA MICROFISSURAS BRANCO 18LT</li>
								</ul>
								<ul>
									<li><b>Composi��o:</b></li>
									<li>Resina 100% acr�lica elastom�rica  em dispers�o aquosa, aditivos heteroc�clicos, pigmentos isentos de metais  pesados, cargas minerais inertes, �lcoois, tensoativos etoxilados e carboxilado.</li>
									<li><b>Importante:</b></li>
									<li>Para se obter o m�ximo da qualidade dos produtos Suvinil � necess�rio um  bom <b>preparo da superf�cie</b> a ser aplicada.</li>
								</ul>						
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>