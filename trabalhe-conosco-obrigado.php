<?php include 'lumine/config.php'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=charset=iso-8859-1" />
	<title> Trabalhe Conosco | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas</title>
	<? include "componentes/includes.php"; ?>
</head>
<body>
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "componentes/topo.php"; ?>
			</div>
		</div>	
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ContatoObrigado">
					<?php

					if (isset($_POST)) 
					{

						$extensao     = pathinfo($_FILES['curriculo']['name'], PATHINFO_EXTENSION);
						$nomearquivo  = md5(time());  

						$_POST['arquivo'] = $nomearquivo.".".$extensao;  

						$tiposPermitidos = array('doc','docx','pdf','txt','DOC','DOCX','TXT','PDF');

						if (array_search($extensao, $tiposPermitidos) !== false) {

							if (move_uploaded_file($_FILES['curriculo']['tmp_name'], getcwd() . DIRECTORY_SEPARATOR . "curriculums" . DIRECTORY_SEPARATOR . $nomearquivo.".".$extensao) == true)
							{ 

								$newContato = new TbTrabalheConosco;
								$newContato->nmCandidato = $_POST['nome'];
								$newContato->dsEmail = $_POST['email'];
								$newContato->cdTelefone = $_POST['telefone'];
								$newContato->nmArquivo = $_POST['telefone'];

								$newContato->insert();

								$emailsender = $_POST['email'];
								$destinatario = "vendas@acasadapintura.com.br";

								if (PHP_OS == "Linux")
								$quebra_linha = "\n"; //Se for Linux
							elseif (PHP_OS == "WINNT")
								$quebra_linha = "\r\n"; // Se for Windows
							else
								//die("Este script nao esta preparado para funcionar com o sistema operacional de seu servidor");

								$quebra_linha = "\n";
							
							$headers = "MIME-Version: 1.1" . $quebra_linha;
							$headers .= "Content-type: text/html; charset=utf-8" . $quebra_linha;
							$headers .= "From: vendas@acasadapintura.com.br". $quebra_linha;
							$headers .= "Return-Path: " .$destinatario . $quebra_linha;
							$headers .= "Cc: marketing@acasadapintura.com.br" . $quebra_linha;
							
							$mensagem .= "<strong> Nome: </strong>" . $_POST["nome"] . "<br />";
							$mensagem .= "<strong> Email: </strong>" . $_POST["email"] . "<br />";
							$mensagem .= "<strong> Telefone </strong>" . $_POST["telefone"] . "<br />";
							$mensagem .= "<strong> Curriculo: </strong><a href='http://".$_SERVER['HTTP_HOST'] . DIRECTORY_SEPARATOR . "curriculums". DIRECTORY_SEPARATOR . $_POST['arquivo'] . "'>".$_POST['arquivo']."</a><br />";
							

							$sucesso = mail($destinatario, "Trabalhe Conosco - A Casa da Pintura", $mensagem, $headers, "-r" . $emailsender);

							// para cliente

							$Mensagem = "";
							$headers="";
							
							$headers .= "MIME-Version: 1.1" . $quebra_linha;
							$headers .= "Content-type: text/html; charset=utf-8" . $quebra_linha;
							$headers .= "From: vendas@acasadapintura.com.br". $quebra_linha;
							$headers .= "Return-Path: vendas@acasadapintura.com.br";
							$headers .= "Reply-To: vendas@acasadapintura.com.br" . $quebra_linha;

							$Mensagem = "<h3><strong>Seu Currículo foi enviado com sucesso!</strong></h3>	

							<strong>Você enviou um currículo para A Casa da Pintura. </strong><br>";

							$sucesso2 = mail($emailsender,"Trabalhe Conosco Obrigado - A Casa da Pintura", $Mensagem, $headers);


							if ($sucesso2)
							{
								echo('<h2>Obrigado por enviar seu curr&iacute;culo! </h2>');
								echo('<p> Em breve retornaremos.</p>');
								echo('<p class="link"> Ir para <a href="index.php" title="P&aacute;gina inicial A Casa da Pintura">home.</a><img src="slices/icon-home.jpg" title="Ir para a p&aacute;gina inicial A Casa da Pintura" /alt="Ir para a página inicial A Casa da Pintura"> </p>');
							} 
							else
							{

								echo('<h2>Falha ao enviar email!</h2>');
								echo('<p> Por favor, tente novamente. </p>');
							}
						}
					}
				}

				?>
			</div>
		</div>
	</div>
	<div id="Linha3">
		<? include "componentes/rodape.php"; ?>
	</div>
</div>
<div id="mask"></div>
</body>
</html>