<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Coral Coralar Esmalte | A Casa da Pintura</title>
	<meta name="Description" content="Coral Coralar Esmalte : Coralar � o esmalte sint�tico para quem deseja economia com qualidade" />
	<meta name="Keywords" content="Tintas Coral coralar esmalte A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Produto">
						<div id="Informacoes">
							<a id="FazerPedido" href="http://www.belatintas.com.br/esmalte-brilhante-tabaco-coralit-1-16-1-17.html" target="_blank" title="Fazer pedido">Fazer pedido</a>
							<div id="ImagemProduto">
								<img title="Tinta Coralar Esmalte" alt="Tinta Coralar Esmalte" src="../slices/tintas-coral/img-coralar-esmalte.jpg" />
							</div>
							<h2>Coralar Esmalte</h2>
							<div id="InformacoesProduto">
								<span class="Titulo">Descri��o do produto</span>
								<p>Coralar � o esmalte sint�tico para quem deseja economia com qualidade. Sua nova formula oferece mais brilho que a vers�o anterior, alem de bom rendimento e poder de cobertura. � f�cil de aplicar e oferece boa resist�ncia. Indicado para uso interno e externo em superf�cies de metais ferrosos, alum�nio, galvanizados e madeiras.</p>
							</div>
							<div id="InformacoesAdicionais"> 
								<div id="Detalhes">
									<span id="Detalhe">Detalhes:</span>	
									<ul>
										<li><b>Embalagem/Rendimento</b></li> 
										<li>Gal�o 3,6 L: 40 a 50 m� por dem�o</li>
										<li>Quarto 0,9 L: 10 a 12,5 m� por dem�o</li>
										<li><b>Aplica��o</b></li>
										<li>�reas grandes: rolo de  espuma ou rev�lver. �reas pequenas: pincel com cerdas macias. Limpe as  ferramentas com Coralraz. </li>
										<li><b>Dilui��o</b></li>
										<li>Usar diluente Coralraz.  Aplica��o pincel/rolo diluir no m�ximo 10%. Aplica��o rev�lver diluir no m�ximo  30%. </li>
										<li><b>Acabamento</b>: Brilhante / Acetinado /  Fosco</li>
										<li><b>Secagem:</b> Ao toque: 4 a 6 horas, entre dem�os: 8 horas, final: 14 a 18 horas</li>
									</ul>									
								</div>	
								<div id="TintasDisponiveis">
									<span id="TintasDisponiveisTitulo">Tintas Dispon�veis:</span>
									<ul>
										<li>600 - CORALAR ESM AB ALUMINIO 3.6LT</li>
										<li>602 - CORALAR ESM AB AMARELO 3.6LT</li>
										<li>604 - CORALAR ESM AB AREIA 3.6LT</li>
										<li>613 - CORALAR ESM AB AZ CELESTE 3.6LT</li>
										<li>606 - CORALAR ESM AB AZ DEL REY 3.6LT</li>
										<li>608 - CORALAR ESM AB AZ FRANCA 3.6LT</li>
										<li>611 - CORALAR ESM AB AZ MAR 3.6LT</li>
										<li>614 - CORALAR ESM AB BRANCO 3.6LT</li>
										<li>616 - CORALAR ESM AB BRANCO GELO 3.6LT</li>
										<li>618 - CORALAR ESM AB CAMURCA 3.6LT</li>
										<li>625 - CORALAR ESM AB COLORADO 3.6LT</li>
										<li>627 - CORALAR ESM AB CONHAQUE 3.6LT</li>
										<li>629 - CORALAR ESM AB CREME 3.6LT</li>
										<li>623 - CORALAR ESM AB CZ ESCURO 3.6LT</li>
										<li>620 - CORALAR ESM AB CZ MEDIO 3.6LT</li>
										<li>630 - CORALAR ESM AB MARFIM 3.6LT</li>
										<li>633 - CORALAR ESM AB MARROM 3.6LT</li>
										<li>634 - CORALAR ESM AB PLATINA 3.6LT</li>
										<li>636 - CORALAR ESM AB PRETO 3.6LT</li>
										<li>639 - CORALAR ESM AB TABACO 3.6LT</li>
										<li>640 - CORALAR ESM AB VD FOLHA 3.6LT</li>
										<li>644 - CORALAR ESM AB VERMELHO 3.6LT</li>
										<li>647 - CORALAR ESM AB VERMELHO GOIA 3.6LT</li>
									</ul>
								</div>						
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>								
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>