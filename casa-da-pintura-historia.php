<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Casa da Pintura</title>
	<? include "componentes/includes.php"; ?>
</head>
<body>
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "componentes/topo.php"; ?>
			</div>
		</div>	
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoQuemSomos">
					<div id="Coluna1">
						<div class="BlocoTexto">
							<h2>Hist�rico</h2>	
							<p>Empresa distribuidora de tintas, localizada na regi�o do grande ABC, no total s�o 5 lojas e um CD (Central de Distribui��o). Fundada em 15/08/1996, no munic�pio de Diadema, a �A Casa da Pintura� iniciou seu trabalho vendendo acess�rios para pintura, como: lixas, pinc�is e rolos para pintura. Conforme o tempo foi passando, foi introduzido, mais itens na linha de produtos, inclusive Abrasivos e Equipamentos de Prote��o Individual, sendo hoje, uma das maiores e mais completas lojas de tintas imobili�rias e industriais de S�o Paulo, com atendimento personalizado a Ind�strias, Construtoras, Arquitetos, Engenheiros e ao Consumidor final.</p>
						</div>
						<div class="BlocoTexto">
							<h2>Miss�o</h2>
							<p>Ser uma empresa lucrativa, vers�til, r�pida e eficaz, focada no cliente, reconhecida nacionalmente por sua log�stica, capaz de adaptar-se rapidamente as mudan�as de mercado sem perder a sua identidade. Ser uma empresa lucrativa, vers�til, r�pida e eficaz, focada no cliente, reconhecida nacionalmente por sua log�stica, capaz de adaptar-se rapidamente as mudan�as de mercado sem perder a sua identidade.</p>
						</div>
						<div class="BlocoTexto">
							<h2>Vis�o</h2>
							<p>Fortalecer a parceria com nossos clientes, disponibilizando solu��es pr�ticas e definitivas para seu dia a dia, valorizando a rela��o profissional e comercial, contar com colaboradores capacitados e empenhados no desempenho de suas fun��es.</p>
							<p>Para a Casa da Pintura, Responsabilidade Social � a capacidade de ouvir os interesses dos diferentes p�blicos e conseguir incorpor�-los ao seu planejamento que a faz uma empresa socialmente respons�vel.</p>
						</div>
						<div class="BlocoTexto">
							<h2>Valores</h2>
							<p>Confian�a</p>
							<p>�tica</p>
							<p>Honestidade</p>
							<p>Transpar�ncia</p>
							<p>Respeito</p>
							<p>Agilidade</p>
							<p>Flexibilidade</p>
							<p>Responsabilidade Social</p>
						</div>
						<h2>Pol�tica</h2>
						<p>Atender bem para atender sempre.</p>
					</div>
					<div id="Coluna2">
						<img src="slices/img-fachada-loja.jpg" title="Fachada loja A Casa da Pintura" alt="Fachada loja A Casa da Pintura" />
						<img src="slices/img-interno-loja-a-casa-da-pintura.jpg" title="A Casa da Pintura" alt="A Casa da Pintura" />
						<img src="slices/img-fachada-a-casa-da-pintura.jpg" title="Fechada da lojada A Casa da Pintura" alt="Fachada da loja A Casa da Pintura"/>
					</div>
				</div>
			</div>
		</div>		
		<div id="Linha3">
			<? include "componentes/rodape.php"; ?>
		</div>	
	</div>
	<div id="mask"></div>
</body>
</html>