<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Loja Mau� | A casa da pintura | Tintas Residenciais, Tintas Industriais e Tintas Automotivas </title>
<? include "componentes/includes.php"; ?>
</head>
<body>
<div id="Pagina">
	<div id="Linha1">
		<div id="ConteudoLinha1">
			<? include "componentes/topo.php"; ?>
		</div>
	</div>	
	<div id="Linha2">
		<div id="ConteudoLinha2">
			<div id="Lojas">
				<h2>Loja de Tintas Mau�</h2>
				<p>Av. Dr. Getulio Vargas, 100 � Vl. Guarani</p>
				<p>Telefone: (11) 2381-4666</p>
				<div id="MapaLoja">
					<iframe width="980" height="420" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=A+Casa+da+Pintura+Avenida+Doutor+Get%C3%BAlio+Vargas,+100+-+Vila+Guarani,+Mau%C3%A1+-+SP,+09310-180+%E2%80%8E&amp;aq=&amp;sll=-23.62526,-46.549029&amp;sspn=0.010655,0.021136&amp;ie=UTF8&amp;hq=A+Casa+da+Pintura+%E2%80%8E&amp;hnear=Avenida+Doutor+Get%C3%BAlio+Vargas,+100+-+Vila+Guarani,+S%C3%A3o+Paulo,+09310-180&amp;t=m&amp;cid=873638542135060966&amp;ll=-23.652858,-46.44805&amp;spn=0.033021,0.084028&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
				</div>
			</div>
		</div>
	</div>		
	<div id="Linha3">
		<? include "componentes/rodape.php"; ?>
	</div>	
</div>
<div id="mask"></div>
</body>
</html>