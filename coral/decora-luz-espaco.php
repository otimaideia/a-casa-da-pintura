<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Coral Decora Luz Espa&ccedil;o | A Casa da Pintura</title>
	<meta name="Description" content="Coral Decora Luz Espa&ccedil;o: Decora Luz & Espa�o, � uma tinta que ajuda a iluminar o seu ambiente, trazendo sensa��o de maior espa�o" />
	<meta name="Keywords" content="Tintas Coral Decora Luz Espa�o A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Tinta Decora Luz Espa�o" alt="Tinta Decora Luz Espa�o" src="../slices/tintas-coral/img-decora-luz-espaco.jpg" />
						</div>
						<h2>Decora Luz Espa�o</h2>

						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Decora Luz & Espa�o, � uma tinta que ajuda a iluminar o seu ambiente, trazendo sensa��o de maior espa�o, por contar com a tecnologia internacional LUMITEC, que apresenta part�culas que refletem o dobro de luminosidade, se comparada a uma tinta convencional.</p>
							<p>� um acr�lico Premium, de acabamento muito superior, e � indicado para ambientes externos e internos. � de f�cil aplica��o, apresenta excelente cobertura e um acabamento sofisticado, encontrado no acabamento fosco.</p>
						</div>
						<div id="InformacoesAdicionais">
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>		

								<ul>
									<li><b>Embalagem/Rendimento</b></li>
									<li>Lata <b>18 L</b>: 175 a 275 por dem�o</li>
									<li>Gal�o <b>3,6 L</b>: 35 a 55 m� por dem�o</li>								
								</ul>
								<ul>
									<li><b>Aplica��o</b></li>
									<li>Rolo de l� de p�lo baixo ou pincel de cerdas macias. Limpe as ferramentas com �gua e sab�o</li>
									<li><b>Dilui��o</b></li>
									<li>Superf�cies n�o seladas, diluir a 1� dem�o em at� 50%, as demais de 10% a 20% com �gua pot�vel.</li>
									<li>Superf�cies j� seladas diluir de 10% a 20% com �gua pot�vel.</li>
									<li><b>Acabamento:</b> Fosco</li>
									<li><b>Secagem</b> Ao toque: 30 minutos, entre dem�os: 4 horas, final: 4 horas</li>
								</ul>
							</div> 
							<div id="TintasDisponiveis">
								<span id="TintasDisponiveisTitulo">Tintas Dispon�veis:</span>
								<ul>
									<li>5680 DECORA FOSCO LUZ & ESPACO BRANCO ABSOLUTO 18LT</li>
									<li>5681 DECORA FOSCO LUZ & ESPACO BRANCO ABSOLUTO 3.6LT</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>