<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Coral Chega de Mofo | A Casa da Pintura</title>
	<meta name="Description" content="Coral Chega de Mofo: Mofo � uma das visitas mais desagrad�veis que as paredes da sua casa poderiam receber" />
	<meta name="Keywords" content="Tintas Coral Direto Gesso A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Chega de Mofo" alt="Chega de Mofo" src="../slices/tintas-coral/img-chega-de-mofo.jpg" />
						</div>
						<h2>Chega de Mofo</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Mofo � uma das visitas mais desagrad�veis que as paredes da sua casa poderiam receber. Mas, felizmente, voc� j� tem como se livrar desse inconveniente sem muito trabalho. � que, com Coral Chega de Mofo, voc� aplica a tinta diretamente sobre o mofo, sem limpar antes. As paredes ficam livres desses h�spedes desagrad�veis por muito mais tempo. Chega de mofo � uma tinta acr�lica com nova tecnologia de fungicida indicada para paredes internas e externas, possui secagem r�pida, �timo acabamento fosco, al�m de boa ader�ncia e resist�ncia.</p>	
						</div>				
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>	
								<ul>
									<li><b>Embalagem/Rendimento</b></li>
									<li>Lata <b>18 L </b>(Teto de Banheiro): 125 a  200 m�  por dem�o</li>
									<li>Gal�o <b>3,6 L</b>  (Teto de Banheiro): 25 a 40 m� por dem�o</li>
									<li>Quarto <b>0,9 L</b>: 6 a 11,5 m� por dem�os</li>
								</ul>
								<ul>
									<li><b>Aplica��o</b></li>
									<li>Rolo de l� de p�lo baixo ou pincel de cerdas macias. Limpe as  ferramentas com �gua e sab�o.</li>
								</ul>
								<ul>
									<li><b>Dilui��o</b></li>
									<li>Pronta para uso, n�o deve ser dilu�da. </li>
								</ul>
								<ul>
									<li><b>Acabamento</b></li>
									<li>Fosco </li>
								</ul>
								<ul>
									<li><b>Secagem</b></li>
									<li>Ao toque: 30 min</li>
									<li>Entre dem�os: 4 horas</li>
									<li>Final: 4 horas</li>
								</ul>								
							</div>
							<div id="TintasDisponiveis">
								<span id="TintasDisponiveisTitulo">Tintas Dispon�veis</span>
								<ul>
									<li>500ML</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>