<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Coral Coralmur Zero Odor | A Casa da Pintura</title>
	<meta name="Description" content="Coral Coralmur Zero Odor: Deixe suas paredes lindas, sem sentir o cheiro da mudan�a" />
	<meta name="Keywords" content="Tintas Coral Coralmur Zero Odor A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="http://www.belatintas.com.br/esmalte-brilhante-tabaco-coralit-1-16-1-17.html" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Tinta Coral 3 em 1" alt="Tinta Coral 3 em 1" src="../slices/tintas-coral/img_coral_3_em_1.jpg" />
						</div>
						<h2>Coral 3 em 1</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Deixe suas paredes lindas, sem sentir o cheiro da mudan�a. Nada de mudar seu ritmo de vida s� porque voc� resolveu trocar a cor das paredes.</p>
							<p>Coral 3 em 1 � o l�tex Super Premium da Coral que n�o deixa rastros de odor at� tr�s horas ap�s a aplica��o. Al�m disso, ele tem maior cobertura e ficou tr�s vezes mais resistente. Indicado para paredes internas e externas e para quem n�o tem tempo a perder.</p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>									
								<ul>
									<li>Zero Odor 3 horas ap�s a aplica��o.</li>
									<li>L�tex Super Premium.</li>
									<li>3x mais resistente.</li>
									<li>Maior cobertura.</li>
									<li>Acabamentos fosco aveludado e semibrilho.</li>
								</ul>
							</div> 
							<div id="TintasDisponiveis">
								<span id="TintasDisponiveisTitulo">Tintas Dispon�veis:</span>
								<ul>
									<li>433 - Coral 3 em 1 18LT LATEX PREMIUM 18LT</li>
									<li>434 - Coral 3 em 1 3.6LT LATEX PREMIUM 3.6LT</li>
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>