<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Tintas Decora Acetinado | A Casa da Pintura</title>
	<meta name="Description" content="Coral Coralar Esmalte Acetinado: Decora ajuda voc� a deixar a sua casa com a sua cara" />
	<meta name="Keywords" content="Tintas Coral Coralar Esmalte Acetinado A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Tinta Decora Acetinado" alt="Tinta Decora Acetinado" src="../slices/tintas-coral/img-decora-acetinado.jpg" />
						</div>
						<h2>Decora Acetinado</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Decora ajuda voc� a deixar a sua casa com a sua cara. Porque a cor da parede � um elemento de decora��o muito importante para criar o ambiente que tanto quer, com seu toque especial. Decora � um acr�lico de qualidade muito superior, com acabamento acetinado, que proporciona uma apar�ncia sedosa e aconchegante. Apresenta r�pida secagem, boa cobertura indicado para paredes internas e externas de alvenaria em geral, massa corrida, massa acr�lica, texturas e concreto.</p>
							<p>Decora Acabamento Acetinado faz parte da nova linha de produtos Zero Odor da Coral. Conhe�a os outros produtos da linha: Decora Cores, Decora Brancos, Decora Neutros, Super Lav�vel, Coralmur, Coralit Zero e Fundo Preparador Coralit Zero.</p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes:</span>	
								<ul>
									<li><b>Embalagem/Rendimento</b></li>
									<li>Lata 18 L: 200 a 300 m� por dem�o</li>
									<li>Gal�o 3,6 L: 40 a 60 m� por dem�o</li>
									<li>Quarto* 0,8 L: 9 a 14 m� por dem�o</li>
								</ul>
								<ul>
									<li><b>Aplica��o</b></li>
									<li>Rolo de l� de p�lo baixo ou pincel de cerdas macias. Limpe as ferramentas com �gua e sab�o.</li>
									<li><b>Dilui��o</b></li>
									<li>Superf�cies n�o seladas, diluir a 1� dem�o em at� 50% e as demais 10 a 30% com �gua pot�vel. Superf�cies j� seladas: diluir todas as dem�os de 10 a 30% com �gua pot�vel.</li>
									<li><b>Secagem:</b> Acetinado</li>
									<li><b>Secagem: </b>Ao toque: 1/2 hora, entre dem�os: 4 horas, final: 4 horas</li>
								</ul>
							</div> 
							<div id="TintasDisponiveis">
								<span id="TintasDisponiveisTitulo">Tintas Dispon�veis:</span>
								<ul>
									<li>Coralar Esmalte Acetinado Branco - COD 718</li>								
								</ul>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>			
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>