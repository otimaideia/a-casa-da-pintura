<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Ecopoxi 828 | Advance Tintas</title>
	<meta name="Description" content="Ecopoxi 841 Advance Tintas: Possibilita pinturas em &aacute;reas internas onde a evapora&ccedil;&atilde;o de solventes n&atilde;o &eacute; conveniente." />
	<meta name="Keywords" content="Tintas advance tintas ecopoxi A Casa da Pintura" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>

						<div id="ImagemProduto">
							<img title="Tinta Advance Ecopoxi 841 Primer" alt="TTinta Advance Ecopoxi 841 Primer" src="../slices/tintas-advance/img-ecopoxi-841-primer.jpg" />
						</div>
						<h2>Tinta Advance Ecopoxi 841 Primer</h2>
						<div id="InformacoesProduto">
							<span class="Titulo">Descri��o do produto</span>
							<p>Possibilita pinturas em �reas internas onde a evapora��o de solventes n�o � conveniente.</p>
							<p><b>Locais para Aplica��o:</b> Metais.</p>
							<p><a target="_blank" title="Dados t�cnicos Ecopoxi 841 Primer" href="/2014/pdf/ecopoxi-841.pdf">Dados T�cnicos (PDF)</a></p>
							<span class="Titulo">Mais informa��es sobre a tinta</span>
							<p>Ecopoxi 841 WBH Primer � um primer anticorrosivo � base de resina ep�xi,   hidrossol�vel, de dois ../componentes. Possibilita pinturas em �reas  internas onde a evapora��o de solventes n�o � conveniente ou � perigosa.  Atrav�s da pesquisa conjunta com nossos fornecedores de mat�ria-prima,   foi poss�vel desenvolver um primer base �gua com performance muito  pr�xima de um primer base solvente convencional. Seu desempenho foi   alcan�ado atrav�s da utiliza��o de pigmentos at�xicos e do uso de cargas  minerais inertes por�m com propriedades auxiliares na prote��o por   barreira.  Recomendado como primer anticorrosivo em superf�cies met�licas, onde n�o   � conveniente a presen�a de solventes no ar durante a aplica��o  e cura, bem como para empresas preocupadas na utiliza��o de produtos   ecologicamente corretos, que tem uma preocupa��o com a sa�de do  aplicador e de minimizar os efeitos ao meio ambiente. Por ser um Primer   este tamb�m auxilia para melhorar o aspecto final de v�rios produtos de  acabamento. Indicado para ind�strias farmac�uticas, qu�micas,   aliment�cias, bebidas, laborat�rios, salas de processamento de produtos   eletr�nicos,  controle museus, hospitais, na pintura de estruturas met�licas.</p>
						</div>										
						<div id="InformacoesAdicionais"></div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>