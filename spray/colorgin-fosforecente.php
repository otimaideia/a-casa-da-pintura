<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Colorgin Fosforecente | Spray A Casa da Pintura</title>
	<meta name="Description" content="Colorgin Fosforescente � uma tinta spray com pigmentos especiais, desenvolvida para brilhar no escuro" />
	<meta name="Keywords" content="tintas spray colorgin fosforecente" />
	<meta name="Author" content="Wender S. Souza" />
	<meta name="Robots" content="index, follow" />
	<meta name="revisit-after" content="1 day" />
	<? include "../componentes/includes-tintas.php"; ?>
</head>
<body id="PaginaTintasResidenciais">
	<div id="Pagina">
		<div id="Linha1">
			<div id="ConteudoLinha1">
				<? include "../componentes/topo.php"; ?>
			</div>
		</div>
		<div id="Linha2">
			<div id="ConteudoLinha2">
				<div id="ConteudoProdutos">
					<div id="Informacoes">
						<a id="FazerPedido" href="#" target="_blank" title="Fazer pedido">Fazer pedido</a>
						<div id="ImagemProduto">
							<img title="Colorgin Fosforecente" alt="Colorgin Fosforecente" src="../slices/spray-colorgin/img-colorgin-fosforecente.jpg" />
						</div>
						<h2>Colorgin Fosforecente</h2>
						<div id="InformacoesProduto">
							<span>Descri��o do produto</span>
							<p><b>Colorgin Fosforescente </b>� uma tinta spray com pigmentos  especiais, desenvolvida para brilhar no escuro. Pode ser usada em superf�cies  internas e externas, para decora��o de ambientes, sal�o de festas,  sinaliza��es, bicicletas, roupas, brinquedos, etc.</p>
							<p><b>Indicada para:</b> pe�as de gesso, madeira, metal,  tecidos, isopor e alvenaria.</p>
							<p>De 1,1 a  1,3 m2  por embalagem, por dem�o.</p>
							<p><b>Secagem:</b></p>
							<p> Entre dem�os: 2   a 3 minutos; </p>
							<p>Ao toque: 30 minutos; </p>
							<p> Manuseio: 1 hora;</p>
							<p>Total: 2 horas</p>
							<p><b>Preparo da Superf�cie /  Aplica��o:</b> </p>
							<p>Elimine  a poeira, gordura ou qualquer contaminante;</p>
							<p>Se  for uma superf�cie sem porosidade, lixe com lixa d��gua 320;</p>
							<p>Aplique  de 2 a 3  dem�os de COLORGIN FOSFORESCENTE h� uma dist�ncia de 25 cm da superf�cie;</p>
							<p>Ao  final do uso, limpe a v�lvula virando a lata para baixo e pressione at� que  saia apenas g�s.</p>
							<p><b>Sobre metais ferrosos e  n�o ferrosos: </b>aplique Colorgin Esmalte Antiferrugem 3 em 1 na cor branca e aguarde  secagem;</p>
							<p><b>Sobre madeira e gesso:</b> aplique Colorgin Fundo Branco  para Luminosa e aguarde secagem;</p>
							<p> <b>Sobre  isopor:</b> n�o � necess�rio a aplica��o do fundo</p>
						</div>
						<div id="InformacoesAdicionais"> 
							<div id="Detalhes">
								<span id="Detalhe">Detalhes: </span>
								<p><b>Rendimento Aproximado: </b>de 1,6 a 2,1 m� por embalagem por  dem�o.</p>
								<p><b>Preparo da Superf�cie/  Aplica��o:</b> </p>
								<p>Elimine  a poeira, gordura ou qualquer contaminante;</p>
								<p>Aplique o fundo certo para cada tipo de  superf�cie e depois aplique de 2   a 3 dem�os de Decor Spray na cor desejada; </p>
								<p>Ao final do uso, limpe a v�lvula virando a lata para  baixo e pressione at� que saia apenas g�s</p>
							</div>
						</div>
					</div>
					<? include "../componentes/solicitar-orcamento.php"; ?>
					<? include "../componentes/outros-produtos.php"; ?>
				</div>
			</div>
		</div>
		<div id="Linha3">
			<? include "../componentes/rodape-tintas.php"; ?>
		</div>
	</div>
	<div id="mask"></div>
</body>
</html>